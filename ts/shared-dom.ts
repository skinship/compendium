import * as icons from "./icons"

/**
 * easy creation of html elements with styles and attributes
 * @param tagName HTML tag name of the element, e.g. "div"
 * @param options @see CreateElementOptions
 * @returns element
 */
export function createElement<T extends keyof HTMLElementTagNameMap>(tagName: T, options: CreateElementOptions<T> = {}) {
    const { attributes = {}, style = {}, className = "", children = [] } = options
    const element = document.createElement(tagName)

    Object.assign(element, { ...attributes, className: className })
    Object.assign(element.style, style)
    element.append(...children)

    return element
}

/**
 * options for createElement
 */
export type CreateElementOptions<T extends keyof HTMLElementTagNameMap> = {
    /**
     * class attribute
     */
    className?: string
    /**
     * style attribute
     */
    style?: Partial<CSSStyleDeclaration>
    /**
     * child elements
     */
    children?: (Element | string)[]
    /**
     * all other attributes applicable to HTMLElementTagNameMap[T]
     */
     attributes?: Partial<HTMLElementTagNameMap[T]>

}

/**
 * ensures that an element exists in the document and returns this element
 * @param query any valid css selector, @see document.querySelector
 * @throws MissingElementError when element can not be found in document, aka is null or undefined
 * @returns selected element
 */
export function ensuredSelector<T extends Element>(query: string): T {
    const element = document.querySelector<T>(query)
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`)
    }
    return element
}

/**
 * Exception thrown when ensuredSelectorand waitForElement can not assure the existance of the input
 */
class MissingElementException extends Error {
    constructor(message: string) {
        super(message)
        this.name = "MissingElementException"
    }
}

/**
 * creates an icon, all icons are in icons.ts
 * @param name key of the icon in icons.ts
 * @param options @see CreateIconOptions
 * @returns icon element
 */
export function createIcon(name: IconType) {
    return createElementFromHTML<SVGElement>(icons[name])
}

/**
 * creates an element from string by using innerHTML
 * @param html any valid html as string
 * @returns element
 */
export function createElementFromHTML<T extends Element>(html: string): T {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild as T
}

/**
 * transforms icons.ts into a type
 */
export type IconType = keyof typeof icons

/**
 * convenience function to create text inputs
 * @param identifier 
 * @returns 
 */
export function createTextInput(identifier: string) {
    return createElement("input", {
        attributes: {
            type: "text",
            id: identifier,
        },
    })
}
