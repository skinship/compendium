/**
 * a skin variant item
 */
type skinCollectionItem = {
    name: string
    screenshots: string[]
}

export type skinInformation = {
    name: string
    variants: skinCollectionItem[]
    videos: {
        skinship: string[]
        author: string[]
    }
}

export type CarousselItemType = "yt-skinship" | "yt-author" | "img"
