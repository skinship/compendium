import { createElement, createIcon } from "../../shared-dom"
import { GalleryController } from "../GalleryController"
import { skinInformation } from "../types"
import { FullscreenGallery } from "./FullscreenGallery"

/**
 * The gallery itself, made up of:
 * - main preview
 * - navigation nodes and window navigation listeners
 * - caroussel
 * - fullscreen preview
 */
export class Gallery extends HTMLElement {
    /**
     * the caroussel containing all the small image previews
     */
    private caroussel: HTMLDivElement = createElement("div", {
        className: "caroussel",
    })
    /**
     * big main preview
     * initiated with placeholder item so .replaceWith can be used
     */
    private mainPreview: HTMLElement = createElement("img", {
        className: "main-gallery__main-preview",
    })
    /**
     * list of all variants for this gallery
     */
    private skinInformation: skinInformation
    /**
     * index of the currently selected variant
     */
    private activeVariantIndex: number = 0
    /**
     * button to navigate to the next element in the caroussel
     */
    private nextNavigationElement: HTMLElement = createElement("div", {
        className: "gallery__navigation gallery__navigation--next main-gallery__navigation main-gallery__navigation--next",
        attributes: {
            onclick: (e) => {
                e.stopPropagation()
                this.getController().nextImage()
            },
        },
        children: [createIcon("chevronRight")],
    })
    /**
     * button to navigate to the previous element in the caroussel
     */
    private prevNavigationElement: HTMLElement = createElement("div", {
        className: "gallery__navigation gallery__navigation--prev main-gallery__navigation main-gallery__navigation--prev",
        attributes: {
            onclick: (e) => {
                e.stopPropagation()
                this.getController().prevImage()
            },
        },
        children: [createIcon("chevronLeft")],
    })
    /**
     * Fullscreen gallery of this gallery.
     */
    // ! is used because its set within setDisplayedVariant which TS doesnt seem to properly detect
    private fullscreenGallery!: FullscreenGallery

    /**
     * GalleryController handling all switching logic.
     */
    // ! is used because its set within setDisplayedVariant which TS doesnt seem to properly detect
    private controller!: GalleryController 

    /**
     * constructor
     * @param collection list of variants to construct the gallery for
     */
    constructor(skinInformation: skinInformation) {
        super()
        this.skinInformation = skinInformation

        // main preview
        this.classList.add("gallery", "main-gallery")

        // main preview part 2
        this.append(
            createElement("div", {
                className: "main-gallery__main-preview-container",
                children: [this.mainPreview, this.nextNavigationElement, this.prevNavigationElement],
                attributes: {
                    onclick: () => {
                        this.fullscreenGallery.enterFullscreenGallery()
                    },
                },
            })
        )

        this.setDisplayedVariant()
        this.append(this.caroussel)
    }

    /**
     * For usage within next/prev elements to ensure it always gets the newest controller
     * @returns current controller
     */
    private getController() {
        return this.controller
    }

    /**
     * formats the gallerytitle for use troughout the component
     * skin name - variant name
     * variant name gets omitted if its equal to skinname
     * @returns formatted title for gallery
     */
    private getGalleryTitle() {
        let galleryTitle = this.skinInformation.name
        const variantName = this.skinInformation.variants[this.activeVariantIndex].name
        if (variantName != galleryTitle) {
            galleryTitle = galleryTitle + ` - ${variantName}`
        }
        return galleryTitle
    }

    /**
     * Adjusts the gallery to show the currently selected variant. This changes the main preview, the caroussel as well as internal values
     */
    private setDisplayedVariant() {
        this.controller = new GalleryController(this.skinInformation, this.activeVariantIndex)

        this.controller.addListener(()=>{
            this.syncState()
        })

        this.fullscreenGallery = new FullscreenGallery(this.controller, this.getGalleryTitle())
        document.body.append(this.fullscreenGallery)

        // clear caroussel
        this.caroussel.innerHTML = ""
        // add all elements
        this.caroussel.append(...this.controller.getAllItems())

        // reset main preview to first image (including nav nodes)
        this.syncState()
    }

    /**
     * Updates the gallery state to match the controller
     */
    public syncState() {
        // set visible navigation notes
        this.prevNavigationElement.classList.remove("hidden")
        this.nextNavigationElement.classList.remove("hidden")
        if (this.controller.activeIsFirst()) {
            this.prevNavigationElement.classList.add("hidden")
        }
        if (this.controller.activeIsLast()) {
            this.nextNavigationElement.classList.add("hidden")
        }

        // set image
        const newMain = this.controller.getActiveItem().getMainPreviewItem()
        this.mainPreview.replaceWith(newMain)
        this.mainPreview = newMain
    }
    
    /**
     * switches the displayed variants
     * @param index index of the variant in the variants list
     */
    public switchVariant(index: number) {
        // illegal variant indeces should not be possible, but better safe than sorry
        if (this.skinInformation.variants[index] == undefined) {
            return
        }
        this.activeVariantIndex = index
        this.setDisplayedVariant()
    }
}

customElements.define("image-gallery", Gallery)
