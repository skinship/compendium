import { createElement, createIcon } from "../../shared-dom"
import { GalleryController } from "../GalleryController"
import { MenuBar } from "./Menubar"

/**
 * gallery shown in fullscreen, layed on top of the rest of the page
 */
export class FullscreenGallery extends HTMLElement {
    /**
     * main preview of fullscreen view
     */
    private mainPreview: HTMLDivElement
    /**
     * button to navigate to the next element in the caroussel
     */
    private nextNavigationElement: HTMLDivElement = this.createNavigationNode("next")
    /**
     * button to navigate to the prev element in the caroussel
     */
    private prevNavigationElement: HTMLDivElement = this.createNavigationNode("prev")
    /**
     * constructor
     * @param parentGallery GalleryController
     * @param skinName name of the currently displayed skin
     */
    constructor(private readonly controller: GalleryController, skinName: string) {
        super()
        this.classList.add("fullscreen-gallery", "hidden")

        // to set correct state
        this.handleNavigationVisibility()
        this.mainPreview = this.controller.getActiveItem().getFullscreenElement()
        this.append(new MenuBar(this, skinName), this.mainPreview, this.prevNavigationElement, this.nextNavigationElement)

        window.addEventListener("keydown", (e) => {
            if (e.key == "Escape") {
                this.exitFullscreenGallery()
            }
        })
        // handle closing clicks
        this.addEventListener("click", (e) => {
            if(e.target != null){
                if (e.target == this || this.mainPreview.contains(e.target as Node)) {
                    this.exitFullscreenGallery()
                }
            }
        })

        controller.addListener(() => {
            this.syncState()
        })
    }

    private createNavigationNode(type: "next" | "prev") {
        return createElement("div", {
            className: `fullscreen-gallery__navigation fullscreen-gallery__navigation--${type} gallery__navigation gallery__navigation--${type}`,
            attributes: {
                onclick: () => {
                    if (type == "prev") {
                        this.controller.prevImage()
                    } else {
                        this.controller.nextImage()
                    }
                    this.syncState()
                },
            },
            children: [createIcon(type == "prev" ? "chevronLeft" : "chevronRight")],
        })
    }

    /**
     * Synchronises the state of the fullscreen gallery with the underlying basic gallery.
     */
    private syncState() {
        const newMain = this.controller.getActiveItem().getFullscreenElement()
        this.mainPreview.replaceWith(newMain)
        this.mainPreview = newMain
        this.handleNavigationVisibility()
    }

    /**
     * exit fullscreen gallery
     */
    public exitFullscreenGallery() {
        // prevent errors in case no element is in fullscreen
        if (document.fullscreenElement) {
            document.exitFullscreen()
        }
        this.classList.add("hidden")
        document.body.classList.remove("freeze")

        // replacing the element with a placeholder to ensure videos stop playing
        // doing this as its easier than dealing with the yt api
        const newMain = createElement("div")
        this.mainPreview.replaceWith(newMain)
        this.mainPreview = newMain
    }

    /**
     * enter fullscreen gallery
     */
    public enterFullscreenGallery() {
        this.syncState()
        this.classList.remove("hidden")
        this.handleNavigationVisibility()
        document.body.classList.add("freeze")
    }

    /**
     * changes the visibility of the navigation nodes
     */
    public handleNavigationVisibility() {
        this.nextNavigationElement.classList.remove("hidden")
        this.prevNavigationElement.classList.remove("hidden")
        if (this.controller.activeIsFirst()) {
            this.prevNavigationElement.classList.add("hidden")
        }
        if (this.controller.activeIsLast()) {
            this.nextNavigationElement.classList.add("hidden")
        }
    }
}

customElements.define("fullscreen-gallery", FullscreenGallery)
