import { createElement, createIcon } from "../../shared-dom"
import { GalleryController } from "../GalleryController"
import { imgurImageToThumbnail } from "../shared"
import { CarousselItemType } from "../types"

/**
 * This component represents one item of the caroussel.
 */
export class CarousselItem extends HTMLElement {
    /**
     * Element for the main preview.
     */
    private mainElement: HTMLImageElement | HTMLEmbedElement

    /**
     * Element for the fullscreen preview.
     */
    private fullscreenElement: HTMLDivElement

    /**
     * constructor
     * @param contentData image link | youtube id
     * @param controller gallery controller controlling t his element
     * @param type type of the content.
     */
    constructor(contentData: string, controller: GalleryController, type: CarousselItemType) {
        super()

        this.mainElement = createElement(type == "img" ? "img" : "embed", {
            className: "main-gallery__main-preview",
            attributes: {
                src: type == "img" ? imgurImageToThumbnail(contentData, "h") : `https://www.youtube.com/embed/${contentData}?rel=0`,
            },
        })

        this.fullscreenElement = createElement("div", {
            className: `fullscreen-gallery__main-preview-container fullscreen-gallery__main-preview-container--${type}`,
            children: [
                createElement(type == "img" ? "img" : "embed", {
                    className: "fullscreen-gallery__main-preview",
                    attributes: {
                        src: type == "img" ? contentData : `https://www.youtube.com/embed/${contentData}?rel=0`,
                    },
                }),
            ],
        })

        const baseClassName = "caroussel__item"
        this.classList.add(baseClassName)
        const videoClassName = `${baseClassName}--video`
        if (type == "yt-author") {
            this.classList.add(`${videoClassName}-author`, videoClassName)
        } else if (type == "yt-skinship") {
            this.classList.add(`${videoClassName}-skinship`, videoClassName)
        } else {
            this.classList.add(`${baseClassName}--image`)
        }

        if (type == "yt-author" || type == "yt-skinship") {
            const playIcon = createIcon("playSolid")

            playIcon.classList.add("play-button")
            if (type == "yt-skinship") {
                playIcon.classList.add("play-button--skinship")
            }

            this.append(playIcon)
        }

        this.addEventListener("click", () => {
            controller.setActiveItem(controller.getAllItems().indexOf(this))
        })
        this.append(
            createElement("img", {
                attributes: {
                    src: type == "img" ? imgurImageToThumbnail(contentData, "t") : `https://img.youtube.com/vi/${contentData}/mqdefault.jpg`,
                },
                className: "caroussel__image",
            })
        )
    }

    /**
     * Get the element that should be used for the main preview of the parent gallery.
     * @returns html element
     */
    public getMainPreviewItem() {
        return this.mainElement
    }

    /**
     * Get the element that should be used for the fullscreen main preview.
     * @returns
     */
    public getFullscreenElement() {
        return this.fullscreenElement
    }
}

customElements.define("caroussel-item", CarousselItem)
