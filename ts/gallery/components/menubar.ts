import { createElement, createIcon } from "../../shared-dom"
import { FullscreenGallery } from "./FullscreenGallery"

/**
 * menubar of the fullscreen gallery
 */
export class MenuBar extends HTMLElement {
    /**
     * constructor
     * @param fullscreenGallery parent fullscreen gallery of this menubar
     * @param skinName name of the displayed skin
     */
    constructor(fullscreenGallery: FullscreenGallery, skinName: string) {
        super()

        this.classList.add("menubar")

        this.append(
            createElement("span", {
                className: "menubar__title",
                attributes: {
                    innerText: skinName,
                },
            }),
            createElement("div", {
                className: "menubar__buttons-container",
                children: [
                    createElement("div", {
                        className: "menubar__button menubar__button--fullscreen",
                        children: [createIcon("fullscreen")],
                        attributes: {
                            onclick: () => fullscreenGallery.requestFullscreen(),
                        },
                    }),
                    createElement("div", {
                        className: "menubar__button menubar__button--close",
                        children: [createIcon("cross")],
                        attributes: {
                            onclick: () => fullscreenGallery.exitFullscreenGallery(),
                        },
                    }),
                ],
            })
        )
    }
}

customElements.define("menu-bar", MenuBar)
