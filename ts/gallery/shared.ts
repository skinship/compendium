/**
 * turns a given imgur link into the respective thumbnail link
 * if given url is not an imgur link it is simply returned
 * @param url url to edit
 * @param size thumbnail size, available: small/big square (square proprotions) | small/medium/large/huge thumbnail (keeps proportions)
 * @returns url
 */
 export function imgurImageToThumbnail(url: string, size: "s" | "b" | "t" | "m" | "l" | "h") {
    if (url.match(/https?:\/\/(i.)?imgur.com\/[a-zA-Z0-9]*.(png|jpg|jpeg|webp)/gm) != null) {
        return url.substring(0, url.lastIndexOf(".")) + size + url.substring(url.lastIndexOf(".", url.length))
    } else {
        return url
    }
}
