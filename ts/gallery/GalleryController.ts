import { CarousselItem } from "./components/CarousselItem"
import { skinInformation } from "./types"

/**
 * Class for handling all state logic for a gallery.
 * Anything that needs to e.g. change the item displayed should register itself as a listener.
 */
export class GalleryController {
    /**
     * List of all listeners of this gallery
     */
    private listeners: Function[] = []

    /**
     * All caroussel items.
     */
    private carousselItems: CarousselItem[] = []

    /**
     * The currently selected (active) item
     */
    private activeItem: CarousselItem

    /**
     * constructor
     * @param data full skin information
     * @param variantIndex index of the variant this controller should handle
     */
    constructor(data: skinInformation, variantIndex: number) {
        this.activeItem = this.carousselItems[0]

        data.variants[variantIndex].screenshots.forEach((url, i) => {
            this.carousselItems.push(new CarousselItem(url, this, "img"))
        })
        data.videos.skinship.forEach((videoID) => {
            this.carousselItems.push(new CarousselItem(videoID, this, "yt-skinship"))
        })
        data.videos.author.forEach((videoID) => {
            this.carousselItems.push(new CarousselItem(videoID, this, "yt-author"))
        })

        this.carousselItems[0].classList.add("active")
        this.activeItem = this.carousselItems[0]

        window.addEventListener("keydown", (e) => {
            if (e.key == "ArrowLeft") {
                this.prevImage()
            }
            if (e.key == "ArrowRight") {
                this.nextImage()
            }
        })
    }

    /**
     * Get the index of the currently active caroussel item.
     * @returns
     */
    private getActiveIndex() {
        return this.carousselItems.indexOf(this.activeItem)
    }

    /**
     * Switch to the next image.
     */
    public nextImage() {
        const index = this.getActiveIndex()
        // -1 to ensure this isnt already the last item
        if (index < this.carousselItems.length - 1) {
            this.setActiveItem(index + 1)
        }
    }

    /**
     * Switch to the previous image.
     */
    public prevImage() {
        const index = this.getActiveIndex()
        // not 0 to ensure this isnt already the first item
        if (index != 0) {
            this.setActiveItem(index - 1)
        }
    }

    /**
     * Check if the currently active item is the first one.
     * @returns true if first, otherwise false
     */
    public activeIsFirst(): boolean {
        if (this.getActiveIndex() == 0) {
            return true
        } else {
            return false
        }
    }

    /**
     * Check if the currently active item is the last one.
     * @returns true if last, otherwise false
     */
    public activeIsLast(): boolean {
        if (this.getActiveIndex() == this.carousselItems.length - 1) {
            return true
        } else {
            return false
        }
    }

    /**
     * Switch the active item to the given index
     * @param index
     */
    public setActiveItem(index: number) {
        // catch indexes over the list length
        if (index >= this.carousselItems.length) {
            return
        }
        this.activeItem = this.carousselItems[index]
        this.carousselItems.forEach((e) => e.classList.remove("active"))
        this.activeItem.classList.add("active")
        this.triggerListeners()
    }

    /**
     * Get the currently active caroussel item.
     * @returns active caroussel item
     */
    public getActiveItem() {
        return this.activeItem
    }

    /**
     * Get all caroussel items.
     * @returns list of all caroussel items.
     */
    public getAllItems() {
        return this.carousselItems
    }

    /**
     * Register a listener on this controller.
     * @param listener function to execute on change event
     */
    public addListener(listener: Function) {
        this.listeners.push(listener)
    }

    /**
     * Trigger all listeners.
     */
    private triggerListeners() {
        this.listeners.forEach((func) => func())
    }
}
