import { Skin } from "./types"

/**
 * This function fetches and parses the list of skins.
 * @returns skinData[] @see skinData
 */
 export async function getFullSkinIndex(url: string = "/search.json") {
    try {
        const response = await fetch(url)

        if (response.status != 200) {
            throw new Error(`Search fetch failed: ${response.status}`)
        }

        return JSON.parse(await response.text(), function (key, value) {
            if (["date_added", "date_released"].includes(key)) {
                return new Date(value)
            } else if (["game_modes", "author", "resolutions", "ratios", "tags", "categories", "skin_collection"].includes(key)) {
                if (value == null) {
                    return []
                }
            }
            return value
        }) as Skin[]
    } catch (e: any) {
        console.error(e.message)
    }
}
/**
 * This function fetches the prestige level map
 * @returns 
 */
 export async function getPrestigeMap() {
    try {
        const response = await fetch("/prestige.json")

        if (response.status != 200) {
            throw new Error(`Search fetch failed: ${response.status}`)
        }

        return JSON.parse(await response.text()) as Record<string, string>
    } catch (e: any) {
        console.error(e.message)
    }
}