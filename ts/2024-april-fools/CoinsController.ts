export class CoinsController {
    coins: number

    channel = new BroadcastChannel("coins_channel")

    constructor() {
        this.coins = parseInt(localStorage.getItem("coins") ?? "1000")

        setInterval(() => {
            this.increaseCoinsRandomly(10)
        }, 15000)
    }

    increaseCoinsRandomly(factor: number): number {
        // increases the coins randomly by 0-50 coins times the factor
        const newCoins = Math.floor(Math.random() * 50 * factor)
        this.coins = this.coins + newCoins
        this.saveCoins()
        return newCoins
    }

    spendCoins(amount: number) {
        this.coins = this.coins - amount
        this.saveCoins()
    }

    saveCoins() {
        localStorage.setItem("coins", this.coins.toString())
        this.channel.postMessage("saved")
    }

    getCoins() {
        return this.coins
    }
}
