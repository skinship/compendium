export function getSkinCollection() {
    const storage = localStorage.getItem("skin-collection")
    if (!storage) {
        return {}
    } else {
        const collection: Record<number, boolean> = JSON.parse(storage)
        return collection
    }
}

export function saveItemToSkinCollection(id: number) {
    const collection = getSkinCollection()
    if(!collection[id]){
        collection[id] = true
    }

    localStorage.setItem("skin-collection", JSON.stringify(collection))
}
