import { SkinItem } from "../../search/components/skin-item"
import { createElement } from "../../shared-dom"
import { Skin } from "../../types"
import { CoinsController } from "../CoinsController"
import { GachaController } from "../GachaController"
import { getSkinCollection } from "../skin-collection-helper"
import { GachaSelector } from "./GachaSelector"

export class SkinCollection extends HTMLElement {
    constructor(allSkins: Skin[], coinsController: CoinsController) {
        super()

        this.classList.add("skin-collection")

        this.append(
            createElement("button", {
                className: "button button--secondary",
                attributes: {
                    innerText: "<= back to gacha selector",
                    onclick: () => {
                        this.replaceWith(new GachaSelector(allSkins, coinsController))
                    },
                },
            })
        )

        // abusing the GachaController save logic
        const sortController = new GachaController(allSkins, "collection", "collection")

        const total5 = sortController.getSkins(5)
        const total4 = sortController.getSkins(4)
        const total3 = sortController.getSkins(3)
        let owned3 = 0
        let owned4 = 0
        let owned5 = 0
        const collection: Record<number, boolean> = getSkinCollection()

        console.log(collection)

        total5.forEach((skin) => {
            if (collection[skin.forum_thread_id]) {
                owned5++
            }
        })
        total4.forEach((skin) => {
            if (collection[skin.forum_thread_id]) {
                owned4++
            }
        })
        total3.forEach((skin) => {
            if (collection[skin.forum_thread_id]) {
                owned3++
            }
        })

        const label5 = createElement("span", {
            className: "skin-collection__section-label",
            attributes: {
                innerText: `[${Math.round((100 / total5.length) * owned5 * 10) / 10}%] (${owned5}/${total5.length}) 5* skins`,
            },
        })
        const label4 = createElement("span", {
            className: "skin-collection__section-label",
            attributes: {
                innerText: `[${Math.round((100 / total4.length) * owned4 * 10) / 10}%] (${owned4}/${total4.length}) 4* skins`,
            },
        })
        const label3 = createElement("span", {
            className: "skin-collection__section-label",
            attributes: {
                innerText: `[${Math.round((100 / total3.length) * owned3 * 10) / 10}%] (${owned3}/${total3.length}) 3* skins`,
            },
        })
        const grid5 = createElement("div", { className: "skin-grid" })
        const grid4 = createElement("div", { className: "skin-grid" })
        const grid3 = createElement("div", { className: "skin-grid" })
        this.append(
            createElement("span", {
                className: "skin-collection__header",
                attributes: {
                    innerText: "Skin Collection",
                },
            })
        )
        this.append(label5, grid5, label4, grid4, label3, grid3)

        sortController.getSkins(5).forEach((skin) => {
            const item = new SkinItem(skin)
            if (!collection[skin.forum_thread_id]) {
                item.classList.add("skin-item--not-owned")
            } else {
                item.classList.add(`skin-item--rarity-${sortController.getSkinRarity(skin)}`)
            }
            grid5.append(item)
        })
        sortController.getSkins(4).forEach((skin) => {
            const item = new SkinItem(skin)
            if (!collection[skin.forum_thread_id]) {
                item.classList.add("skin-item--not-owned")
            } else {
                item.classList.add(`skin-item--rarity-${sortController.getSkinRarity(skin)}`)
            }
            grid4.append(item)
        })
        sortController.getSkins(3).forEach((skin) => {
            const item = new SkinItem(skin)
            if (!collection[skin.forum_thread_id]) {
                item.classList.add("skin-item--not-owned")
            } else {
                item.classList.add(`skin-item--rarity-${sortController.getSkinRarity(skin)}`)
            }
            grid3.append(item)
        })
    }
}

customElements.define("skin-collection", SkinCollection)
