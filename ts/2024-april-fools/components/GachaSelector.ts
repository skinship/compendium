import { createElement } from "../../shared-dom"
import { Skin } from "../../types"
import { CoinsController } from "../CoinsController"
import { GachaController } from "../GachaController"
import { BannerMenu } from "./BannerMenu"
import { SkinCollection } from "./SkinCollection"

export class GachaSelector extends HTMLElement {
    constructor(allSkins: Skin[], coinsController: CoinsController) {
        super()

        // split skins into certain categories and "leftovers"

        const filteredSkins: Map<string, Skin[]> = new Map<string, Skin[]>()

        const groups = ["anime", "minimalistic", "eyecandy", "game"]

        allSkins.forEach((skin) => {
            let filtered = false

            groups.forEach((group) => {
                if (skin.categories.includes(group)) {
                    const newResult = filteredSkins.get(group) ?? []
                    newResult.push(skin)
                    filteredSkins.set(group, newResult)
                    filtered = true
                }
            })

            if (!filtered) {
                const newResult = filteredSkins.get("other") ?? []
                newResult.push(skin)
                filteredSkins.set("other", newResult)
            }
        })

        for (const entry of filteredSkins.entries()) {
            const skins = entry[1]
            const name = entry[0]

            const controller = new GachaController(skins, name, name)
            const selectorItem = createElement("div", {
                className: "banner-selector-item",
                children: [
                    createElement("span", { className: "banner-selector-item__name", attributes: { innerText: name } }),
                    createElement("span", { className: "banner-selector-item__total", attributes: { innerText: controller.getTotalPulls().toString() } }),
                    createElement("span", { className: "banner-selector-item__pity-4", attributes: { innerText: controller.getPity(4).toString() } }),
                    createElement("span", { className: "banner-selector-item__pity-5", attributes: { innerText: controller.getPity(5).toString() } }),
                    createElement("img", { className: "banner-selector-item__cover", attributes: { src: `/assets/img/gacha/banners/${name}.webp` } }),
                ],
            })

            selectorItem.addEventListener("click", () => {
                this.replaceWith(new BannerMenu(controller, allSkins, coinsController))
            })
            this.append(selectorItem)
        }

        this.prepend(
            createElement("div", {
                className: "main-menu__header",
                children: [
                    createElement("span", { className: "main-menu__header-label", attributes: { innerText: "Available Gacha Banners" } }),
                    createElement("span", { className: "main-menu__header-legend", attributes: { innerText: " total pulls | 4* pity | 5* pity" } }),
                ],
            })
        )

        this.classList.add("main-menu")

        this.append(
            createElement("button", {
                className: "button button--primary",
                attributes: {
                    innerText: "View Collection",
                    onclick: () => {
                        this.replaceWith(new SkinCollection(allSkins, coinsController))
                    },
                },
            })
        )
    }
}

customElements.define("gacha-selector", GachaSelector)
