import { ensuredSelector } from "../shared-dom"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

async function main() {
    initEventListeners()
}

function initEventListeners() {
    const noWarningCheckBox = ensuredSelector<HTMLInputElement>(".column-select__checkbox#show")
    document.querySelectorAll<HTMLInputElement>(".column-select__checkbox:not(#show)").forEach((e) => {
        e.addEventListener("change", () => {
            toggleColumn(e.id, e.checked)
            hideNoWarningRows(noWarningCheckBox.checked)
        })
    })
    noWarningCheckBox.addEventListener("change", () => {
        hideNoWarningRows(noWarningCheckBox.checked)
    })
}

function toggleColumn(column: string, show: boolean) {
    document.querySelectorAll(`.${column}`).forEach((e) => {
        show ? e.classList.remove("hidden") : e.classList.add("hidden")
    })
}

function hideNoWarningRows(show: boolean) {
    document.querySelectorAll("tbody tr").forEach((tr) => {
        if (show) {
            // show all rows
            tr.classList.remove("hidden")
        } else {
            // hide rows without warnings
            if (tr.querySelectorAll(".warning:not(.hidden)").length == 0) {
                tr.classList.add("hidden")
            } else {
                tr.classList.remove("hidden")
            }
        }
        // filter out page heads with no skins
        document.querySelectorAll(".page-index").forEach((header) => {
            if (Array.from(header.nextElementSibling!.children).every((child) => child.classList.contains("hidden"))) {
                header.classList.add("hidden")
            } else {
                header.classList.remove("hidden")
            }
        })
    })
}
