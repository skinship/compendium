import { getFullSkinIndex } from "../fetch"
import { initSearch } from "../search/index"
import { ensuredSelector } from "../shared-dom"

if (document.readyState === "loading") {
    // Loading hasn't finished yet
    document.addEventListener("DOMContentLoaded", main)
} else {
    // `DOMContentLoaded` has already fired
    main()
}

async function main() {
    const skinIndex = (await getFullSkinIndex())!
    const skinIds = ensuredSelector<HTMLElement>("#skin-ids").innerText.split("|").map(id => parseInt(id))

    initSearch({
        skinIndex: skinIndex.filter((skin) => skinIds.includes(skin.forum_thread_id)),
        pagination: false,
    })
}
