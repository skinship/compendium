export function randomiseArray(items: any[]) {
    for (let i = items.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1))
        ;[items[i], items[j]] = [items[j], items[i]]
    }
    return items.sort(() => Math.random() - 0.5)
}

export function wait(ms: number) {
    return new Promise((r) => setTimeout(r, ms))
}
