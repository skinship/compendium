import { createElement } from "../../shared-dom"
import { Skin } from "../../types"
import { renderSkinChunk } from "../pagination"
import { filter } from "../types"
import { generateUrlQueryParameter, updateUrlQueryPage } from "../url-query"

/**
 * This class defines the pagination for the skin listing (js search results)
 */
export class Pagination extends HTMLElement {
    /**
     * This property defines an array holding arrays of skinData[][]
     * Each array equals 1 page in the pagination
     */
    private chunkedSkinList: Skin[][]

    /**
     * Index of the currently shown/selected chunk
     */
    private currentChunkIndex: number

    /**
     * Array of pagination text node
     * This includes all pagination nodes containing a number, not the ones with an icon
     */
    private paginationTextNodes: HTMLElement[] = []

    /**
     * This number defines how many pagination pages there should be next to the current one on either side.
     */
    private paginationRange: number = 2

    /**
     * The node to navigate to the first page
     */
    private first: HTMLAnchorElement

    /**
     * The node to navigate one page back
     */
    private prev: HTMLAnchorElement

    /**
     * The node to navigate one page forwards
     */
    private next: HTMLAnchorElement

    /**
     * The node to navigate to the last page
     */
    private last: HTMLAnchorElement

    /**
     * This property defines the filter that was used for this search.
     * Used to generate pagination links
     */
    private filter: filter

    /**
     * constructor
     * @param chunkedSkinList An array of chunked skinData arrays. One array inside this array equals one page in the pagination
     * @param page the page the pagination should start on
     */
    constructor(chunkedSkinList: Skin[][], page: number = 1, filter: filter) {
        super()
        this.chunkedSkinList = chunkedSkinList
        this.filter = filter
        this.currentChunkIndex = page - 1
        updateUrlQueryPage(page)

        this.classList.add("pagination", "search-pagination")

        this.first = this.createIcon("first", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = 0
            this.handleNavigationClick()
        })
        this.prev = this.createIcon("prev", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = this.currentChunkIndex == 0 ? 0 : this.currentChunkIndex - 1
            this.handleNavigationClick()
        })
        this.append(this.first, this.prev)

        this.chunkedSkinList.forEach((skinChunk, i) => {
            this.paginationTextNodes.push(this.createPaginationTextNode(i, generateUrlQueryParameter(this.filter)))
        })

        this.next = this.createIcon("next", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = this.currentChunkIndex == this.chunkedSkinList.length - 1 ? this.chunkedSkinList.length - 1 : this.currentChunkIndex + 1
            this.handleNavigationClick()
        })
        this.last = this.createIcon("last", generateUrlQueryParameter(this.filter), () => {
            this.currentChunkIndex = chunkedSkinList.length - 1
            this.handleNavigationClick()
        })

        this.append(this.next, this.last)

        this.showPaginationNodes()
        this.updatePaginationVisualState()
        renderSkinChunk(this.chunkedSkinList[this.currentChunkIndex])
    }

    /**
     * This function updates which text nodes are visible
     */
    private showPaginationNodes() {
        let firstPage = this.currentChunkIndex - this.paginationRange < 0 ? 0 : this.currentChunkIndex - this.paginationRange
        let lastPage = this.currentChunkIndex + this.paginationRange

        // first
        if (firstPage == 0) {
            lastPage = this.paginationRange * 2
        }
        // second
        if (firstPage == 1) {
            lastPage = 1 + this.paginationRange * 2
        }
        // second last
        if (lastPage == this.chunkedSkinList.length) {
            firstPage = lastPage - this.paginationRange * 2 - 1
        }
        // last
        if (lastPage > this.chunkedSkinList.length) {
            lastPage = this.chunkedSkinList.length
            firstPage = lastPage - 1 - this.paginationRange * 2
        }
        if (firstPage < 0) {
            firstPage = 0
        }

        this.querySelectorAll("a:not(.pagination-item__icon-node)").forEach((e) => e.remove())
        this.paginationTextNodes
            .slice(firstPage, lastPage + 1)
            .reverse()
            .forEach((e) => this.insertBefore(e, this.children[2]))
    }

    /**
     * This function creates pagination nodes containing an icon
     * @param icon icon to be displayed
     * @returns pagination node
     */
    private createIcon(icon: "first" | "prev" | "next" | "last", queryParam: URLSearchParams, eventFunction: Function) {
        const icons = {
            first: `<svg class="icon " fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 19l-7-7 7-7m8 14l-7-7 7-7"></path></svg>`,
            prev: `<svg class="icon" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>`,
            next: `<svg class="icon" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>`,
            last: `<svg class="icon" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 5l7 7-7 7M5 5l7 7-7 7"></path></svg>`,
        }

        switch (icon) {
            case "prev":
                if (this.currentChunkIndex != (0 && 1)) {
                    queryParam.append("p", this.currentChunkIndex.toString())
                } else {
                    queryParam.delete("p")
                }
                break
            case "next":
                if (this.currentChunkIndex + 1 == this.chunkedSkinList.length) {
                    queryParam.set("p", this.chunkedSkinList.length.toString())
                } else {
                    queryParam.append("p", (this.currentChunkIndex + 2).toString())
                }
                break
            case "last":
                queryParam.append("p", this.chunkedSkinList.length.toString())
                break
        }

        return createElement("a", {
            className: `pagination-item pagination-item__icon-node`,
            children: [
                createElement("div", {
                    style: {
                        display: "flex",
                    },
                    attributes: {
                        innerHTML: icons[icon],
                    },
                }),
            ],
            attributes: {
                href: window.location.pathname.concat("?", queryParam.toString()),
                onclick: (e) => {
                    e.preventDefault()
                    eventFunction()
                },
            },
        })
    }

    /**
     * This function handles what happens when any navigation node is clicked
     */
    private handleNavigationClick() {
        if (this.chunkedSkinList[this.currentChunkIndex] != undefined) {
            renderSkinChunk(this.chunkedSkinList[this.currentChunkIndex])
            this.updatePaginationVisualState()
            this.showPaginationNodes()
            updateUrlQueryPage(this.currentChunkIndex + 1)

            // update prev node href
            const prevParam = new URLSearchParams(new URL(this.prev.href).search)
            if (prevParam.has("p")) {
                if (this.currentChunkIndex != 0) {
                    prevParam.set("p", this.currentChunkIndex.toString())
                } else {
                    prevParam.delete("p")
                }
            } else if (this.currentChunkIndex != 1) {
                // this adds the key if missing, unless its page 1, in which case we skip
                prevParam.append("p", (this.currentChunkIndex + 1).toString())
            }
            this.prev.href = window.location.pathname.concat("?", prevParam.toString())

            // update next node href
            const nextParam = new URLSearchParams(new URL(this.next.href).search)
            if (nextParam.has("p")) {
                if (this.currentChunkIndex + 1 == this.chunkedSkinList.length) {
                    nextParam.set("p", this.chunkedSkinList.length.toString())
                } else {
                    nextParam.set("p", (this.currentChunkIndex + 2).toString())
                }
            }
            this.next.href = window.location.pathname.concat("?", nextParam.toString())
        }
    }

    /**
     * This function creates pagination nodes containing a number
     * @param index number of the text node
     * @returns pagination node
     */
    private createPaginationTextNode(index: number, queryParam: URLSearchParams) {
        if (index != 0) {
            queryParam.append("p", (index + 1).toString())
        }

        return createElement("a", {
            className: `pagination-item ${index == 0 ? "current-page" : ""}`,
            attributes: {
                innerText: (index + 1).toString(),
                onclick: (e) => {
                    e.preventDefault()
                    this.currentChunkIndex = index
                    this.handleNavigationClick()
                },
                href: window.location.pathname.concat("?", queryParam.toString()),
            },
        })
    }

    /**
     * This function updates the visual states for all nodes of the pagination
     */
    private updatePaginationVisualState() {
        Array.from(this.children).forEach((e) => e.classList.remove("current-page"))
        if (this.currentChunkIndex == 0) {
            this.first.classList.add("current-page")
            this.prev.classList.add("current-page")
        }
        if (this.currentChunkIndex == this.chunkedSkinList.length - 1 || this.chunkedSkinList.length == 0) {
            this.next.classList.add("current-page")
            this.last.classList.add("current-page")
        }

        const element = this.paginationTextNodes[this.currentChunkIndex]
        
        if(element !==null && element !== undefined){
            element.classList.add("current-page")
        }
    }
}

customElements.define("search-pagination", Pagination)
