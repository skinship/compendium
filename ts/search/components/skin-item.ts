import { createElement, createIcon } from "../../shared-dom"
import { Skin } from "../../types"
import { prestigeMap } from "../config"
import { tagData } from "../types"

/**
 * This class defines an item of the skin listing
 */
export class SkinItem extends HTMLElement {
    /**
     * The skin this component is for
     * @see Skin
     */
    private skin

    /**
     * constructor
     * @param skin the skin this component is for
     */
    constructor(skin: Skin) {
        super()

        this.skin = skin
        this.classList.add("skin-item")

        this.append(
            createElement("a", {
                className: "skin-item__link",
                attributes: {
                    href: skin.url,
                },
            })
        )
        this.append(this.createCover())
        this.append(this.createInformationArea())
        this.append(this.createTagsArea())
    }

    /**
     * This function creates the HTMLDivElement containing cover and version count
     * @returns HTMLDivElement containing cover and count
     */
    private createCover() {
        const cover = createElement("div", {
            className: "skin-item__cover",
            children: [
                createElement("img", {
                    className: "cover",
                    attributes: {
                        src: `/assets/img/covers/${this.skin.forum_thread_id}.webp`,
                    },
                }),
            ],
        })

        const versionCount = this.skin.skin_collection.length

        if (versionCount > 1) {
            cover.append(
                createElement("div", {
                    className: "skin-item__cover-versions",
                    children: [
                        createIcon("documentSolid"),
                        createElement("span", {
                            className: "version-count",
                            attributes: {
                                innerText: versionCount.toString(),
                            },
                        }),
                    ],
                })
            )
        }

        return cover
    }

    /**
     * This function creates the HTMLDivElement containing all the information about the skin
     * @returns
     */
    private createInformationArea() {
        return createElement("div", {
            className: "skin-item__information",
            children: [
                createElement("div", {
                    className: "skin-item__information-row",
                    children: [
                        createElement("span", {
                            className: "skin-name",
                            attributes: {
                                innerText: this.skin.skin_name,
                            },
                        }),
                        createElement("div", {
                            className: "game-modes",
                            children: this.skin.game_modes.map((mode) => createIcon(mode == "catch" ? "ctb" : mode)),
                        }),
                    ],
                }),
                createElement("div", {
                    className: "skin-item__information-row",
                    children: [
                        createElement("span", {
                            className: "author",
                            attributes: {
                                innerText: this.skin.formatted_author,
                            },
                        }),
                        createElement("span", {
                            className: "information-seperator",
                        }),
                        ...this.skin.resolutions.map((e) => {
                            return createElement("span", {
                                className: "resolution",
                                attributes: {
                                    innerText: e.toUpperCase(),
                                },
                            })
                        }),
                    ],
                }),
            ],
        })
    }

    private createTagsArea() {
        const tags: HTMLElement[] = this.generateTagsData().map((tag) => createTag(tag))

        return createElement("div", {
            className: "skin-item__tags-list-container",
            children: [
                createElement("div", {
                    className: "tags-list",
                    children: tags,
                }),
            ],
        })

        function createTag(tag: tagData) {
            return createElement("a", {
                className: `tag${tag.prestige != "" ? " tag--prestige tag--prestige-" + tag.prestige : ""}`,
                attributes: {
                    innerText: tag.tag.replace(/_/g, " "),
                    href: `?${tag.searchKey}=${tag.tag}`,
                },
            })
        }
    }

    private generateTagsData(): tagData[] {
        const tagsData: tagData[] = []

        this.skin.resolutions.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "res",
                tag: e.toUpperCase(),
            })
        })
        this.skin.ratios.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "ratio",
                tag: e,
            })
        })
        this.skin.tags.sort().forEach((e: string) => {
            tagsData.push({
                prestige: prestigeMap[e],
                searchKey: "search",
                tag: e,
            })
        })
        this.skin.categories.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "tagin",
                tag: e,
            })
        })

        return tagsData
    }
}

customElements.define("skin-item", SkinItem)
