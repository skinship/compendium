import { GameModes, Ratios, Resolutions, Skin } from "../types"

/**
 * This defines the options for a filter
 * @param strInclude Optional, string that MUST be included by the skin
 * @param gameModeInclude Optional, game modes which MUST be included by the skin
 * @param tagInclude Optional, categories which MUST be included by the skin
 * @param tagExclude Optional, categories which MUST NOT be included by the skin
 * @param resolutions Optional, HD | SD tags which MUST be included by the skin
 * @param ratios Optional, aspect ratios which MUST be included by the skin
 * @param order Mandatory, sort order of the skin
 */
export type filter = {
    strInclude: string[]
    gameModeInclude: GameModes[]
    catInclude: string[]
    catExclude: string[]
    resolutions: Resolutions[]
    ratios: Ratios[]
    order: sortOrder
    dateAfter: Date
    dateBefore: Date
}

/**
 * This type defines all possible sort orders
 */
export type sortOrder = "a-z" | "z-a" | "old-new" | "new-old" | "random" | "comp-old-new" | "comp-new-old"

/**
 * Defines the information about a tag as is required for dispalying them in .tags-list
 */
export type tagData = {
    tag: string
    searchKey: string
    prestige: string
}

/**
 * Defines the various kinds of tags, mainly regular (""), various prestige tags and keymode
 */
export type tagType = "" | "c1" | "c2" | "c3" | "cs" | "sb" | "key" | "soty-1" | "soty-2" | "soty-3" | "soty-t10"

export type searchSettings = {
    skinIndex: Skin[]
    pagination: boolean
}
