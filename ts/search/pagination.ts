import { Skin } from "../types"
import { Pagination } from "./components/pagination"
import { SkinItem } from "./components/skin-item"
import { resultsPerPage } from "./config"
import { hideDefaultListing } from "./search"
import { filter, searchSettings } from "./types"

/**
 * This function triggers the pagination for searchResults
 * @param skinList The array of all skins @see skinData
 */
export function paginateSearchResults(skinList: Skin[], page: number, filter: filter, settings: searchSettings) {
    // show all skins on one page if pagination is disabled
    const chunkSize = settings.pagination ? resultsPerPage : skinList.length;
    const chunkedSkinList = chunkSkinList(skinList, chunkSize)
    hideDefaultListing()
    const pagination = document.querySelector<HTMLElement>(".search-pagination")
    if(pagination !== null){
        pagination.remove()
    }
    document.querySelector<HTMLElement>("#skin-grid-results")!.after(new Pagination(chunkedSkinList, page, filter))
}

/**
 * This function splits the list of skins into smaller chunks for pagination and returns these as an array
 * @param skinList The array of all skins to be chunked @see skinData
 * @param chunkSize The amount of skins per chunk
 * @returns skinData[][]
 */
function chunkSkinList(skinList: Skin[], chunkSize: number) {
    const chunkedList = []
    for (let i = 0; i < skinList.length; i += chunkSize) {
        chunkedList.push(skinList.slice(i, i + chunkSize))
    }
    return chunkedList
}

/**
 * This function renders a list skins into #skin-grid-results
 * @param skinList array of skins to be rendered, @see skinData
 */
export function renderSkinChunk(skinList: Skin[]) {
    clearResultGrid()

    if (skinList == undefined) {
        return
    }
    const resultGrid = document.querySelector<HTMLElement>("#skin-grid-results")!
    skinList.forEach((s) => resultGrid!.append(new SkinItem(s)))
}

/**
 * This function clears the resultsGrid of all its content
 */
export function clearResultGrid() {
    document.querySelector<HTMLElement>("#skin-grid-results")!.innerHTML = ""
}
