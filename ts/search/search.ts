import { randomiseArray } from "../shared"
import { ensuredSelector } from "../shared-dom"
import { GameModes, Ratios, Resolutions, Skin } from "../types"
import { searchDates } from "./config"
import { clearResultGrid, paginateSearchResults } from "./pagination"
import { filter, searchSettings, sortOrder } from "./types"
import { setUrlQueryParameters } from "./url-query"

/**
 * This function get the configuration of the user selected filters
 * @returns filter config @see filter
 */
export function getFilters() {
    const searchInput = ensuredSelector<HTMLInputElement>("#search-input")
    const tagIncludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--cat.include")
    const tagExcludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--cat.exclude")
    const resolutionIncludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--resolution.include")
    const ratioIncludeNodes = document.querySelectorAll<HTMLSpanElement>(".filter-tag--ratio.include")
    const gameModeIncludeNodes = document.querySelectorAll<HTMLDivElement>(".filter-game-mode.include")
    const dateAfterInput = ensuredSelector<HTMLInputElement>("#date-after")
    const dateBeforeInput = ensuredSelector<HTMLInputElement>("#date-before")
    const exactSearchRegex = /"[^"]*"/gm

    let filter: filter = {
        strInclude: [],
        gameModeInclude: Array.from(gameModeIncludeNodes).map((e) => e.dataset.tag as GameModes),
        catInclude: Array.from(tagIncludeNodes).map((e) => e.dataset.tag!),
        catExclude: Array.from(tagExcludeNodes).map((e) => e.dataset.tag!),
        resolutions: Array.from(resolutionIncludeNodes).map((e) => e.dataset.tag as Resolutions),
        ratios: Array.from(ratioIncludeNodes).map((e) => e.dataset.tag as Ratios),
        order: document.querySelector<HTMLSelectElement>(".filter-order-selector")!.value as sortOrder,
        dateAfter: dateAfterInput.value == "" ? searchDates.oldest : new Date(dateAfterInput.value),
        dateBefore: dateBeforeInput.value == "" ? searchDates.newest : new Date(dateBeforeInput.value),
    }

    if (searchInput.value != "") {
        let search = searchInput.value.toLowerCase()
        const match = search.match(exactSearchRegex)
        if (match !== null) {
            match.forEach((e) => {
                search = search.replace(e, "")
                // do not remove the wrapping "quotes". If they are removed they will not be added to the url query. the search ignores them
                addStringSearchElements([e])
            })
        }
        addStringSearchElements(search.trim().split(" "))
    }

    return filter

    function addStringSearchElements(elements: string[]) {
        elements.forEach((e) => {
            if (e != "") {
                // filter out empty elements
                filter.strInclude.push(e)
            }
        })
    }
}

/**
 * This function filters an array of skinData objects by the given filters.
 * @param skinIndex A list of skins to be filtered
 * @param filter The options to be filtered with @see filter
 * @returns skinData[] @see skinData
 */
export function filterSkins(skinIndex: Skin[], filter: filter) {
    // This returns a new array, so no need to worry about mutating the original skinlist (which gets reused)
    const filteredSkins = skinIndex.filter((skin) => {
        let skinCategories = skin.categories.map((e) => e.toLowerCase())
        let skinGameModes = skin.game_modes
        let skinResolutions = skin.resolutions
        let skinRatios = skin.ratios
        let skinMetaData = [...skin.author, ...skinCategories, ...skin.tags, skin.skin_name, ...skin.skin_collection.map((e) => e.name)]
            .join(" ")
            .toLowerCase()
            .replace(/[^a-zA-Z0-9 ]/g, "")

        return (
            // excludes | if any exclude tag is present on the skin => false; else => true
            !filter.catExclude.some((i) => skinCategories.includes(i)) &&
            // includes | checks that for every include array all elements are present on the skin
            filter.gameModeInclude.every((i) => skinGameModes.includes(i)) &&
            filter.catInclude.every((i) => skinCategories.includes(i)) &&
            filter.resolutions.every((i) => skinResolutions.includes(i)) &&
            (skinRatios.includes("all") ? true : filter.ratios.every((i) => skinRatios.includes(i))) &&
            filter.strInclude.every((i) => skinMetaData.includes(i.replace(/[^a-zA-Z0-9 ]/g, ""))) &&
            filter.dateAfter < skin.date_released &&
            filter.dateBefore > skin.date_released
        )
    })
    switch (filter.order) {
        case "a-z":
            return filteredSkins.sort((a: Skin, b: Skin) => {
                if (a.skin_name.toLowerCase() < b.skin_name.toLowerCase()) return -1
                if (a.skin_name.toLowerCase() > b.skin_name.toLowerCase()) return 1
                return 0
            })
        case "z-a":
            return filteredSkins.sort((a: Skin, b: Skin) => {
                if (a.skin_name.toLowerCase() < b.skin_name.toLowerCase()) return 1
                if (a.skin_name.toLowerCase() > b.skin_name.toLowerCase()) return -1
                return 0
            })
        case "old-new":
            return filteredSkins.sort((a: Skin, b: Skin) => {
                if (a.date_released > b.date_released) return 1
                if (a.date_released < b.date_released) return -1
                return 0
            })
        case "new-old":
            return filteredSkins.sort((a: Skin, b: Skin) => {
                if (a.date_released < b.date_released) return 1
                if (a.date_released > b.date_released) return -1
                return 0
            })
        case "comp-new-old":
            return filteredSkins.sort((a: Skin, b: Skin) => {
                if (a.date_added < b.date_added) return 1
                if (a.date_added > b.date_added) return -1
                return 0
            })
        case "comp-old-new":
            return filteredSkins.sort((a: Skin, b: Skin) => {
                if (a.date_added > b.date_added) return 1
                if (a.date_added < b.date_added) return -1
                return 0
            })
        case "random":
            return randomiseArray(filteredSkins)
        default:
            return filteredSkins
    }
}

/**
 * This function serves as the central calling point for performing the search.
 * @param skinIndex skinData[] skin array to be searched on
 * @param page the page the pagination should start on (1 higher than index because humans start counting at 1!)
 */
export function performSearch(settings: searchSettings, page: number = 1) {
    const resultNumberDisplayJs = document.querySelector<HTMLElement>(".filter-results-count.js")!

    const filter = getFilters()
    setUrlQueryParameters(filter)
    if (isFilterDefault(filter)) {
        showDefaultListing()
        return
    }
    const filteredSkinList = filterSkins(settings.skinIndex, filter)
    resultNumberDisplayJs.innerText = `${filteredSkinList.length.toString()} skins`
    paginateSearchResults(filteredSkinList, page, filter, settings)
}

/**
 * This function hides the default listing and unhides the things needed for the JS listing
 */
export function hideDefaultListing() {
    // remove default content
    ensuredSelector<HTMLElement>("#skin-grid").classList.add("hidden")
    document.querySelector<HTMLElement>(".pagination:not(.search-pagination)")?.classList.add("hidden")
    ensuredSelector<HTMLElement>(".filter-results-count.default").classList.add("hidden")
    // show JS content
    ensuredSelector<HTMLElement>(".filter-results-count.js").classList.remove("hidden")
    // grid handled by css
    // pagination gets readded all the time via js
}

/**
 * This function hides/removes the things added by the JS listing and shows the default listing again
 */
export function showDefaultListing() {
    // hide/remove js content
    clearResultGrid()
    ensuredSelector<HTMLElement>(".search-pagination").remove()
    ensuredSelector<HTMLElement>(".filter-results-count.js").classList.add("hidden")
    // show default content
    ensuredSelector<HTMLElement>("#skin-grid").classList.remove("hidden")
    ensuredSelector<HTMLElement>(".filter-results-count.default").classList.remove("hidden")
    document.querySelector(".pagination:not(.search-pagination)")?.classList.remove("hidden")
}

/**
 * This function checks if a given filter equals the default search settings
 * @param filter
 * @returns
 */
export function isFilterDefault(filter: filter) {
    if (
        filter.order == "new-old" &&
        filter.gameModeInclude.length != 0 &&
        filter.strInclude.length != 0 &&
        filter.catExclude.length != 0 &&
        filter.catInclude.length != 0 &&
        filter.resolutions.length != 0 &&
        filter.ratios.length != 0
    ) {
        return true
    }
    return false
}
