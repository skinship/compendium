import { initPrestigeMap } from "./config"
import { addEventListeners, readSearchState } from "./init"
import { performSearch } from "./search"
import { searchSettings } from "./types"
import { readUrlQuery, useReadQuery } from "./url-query"

export async function initSearch(settings: searchSettings) {
    readSearchState()
    await initPrestigeMap()
    if (window.location.search != "") {
        useReadQuery(...readUrlQuery(), settings)
    }
    addEventListeners(settings)

    performSearch(settings)
}
