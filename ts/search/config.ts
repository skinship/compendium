import { getPrestigeMap } from "../fetch"

/**
 * This variable defines how long it should take from last input to search.
 * Set to 1 second currently to prevent high amount of url history writing
 */
export const searchDebounceTime = 1000

/**
 * This variable defines how many search results should be shown per page, should always be the same as the jekyll config.
 */
export const resultsPerPage = 18

/**
 * This object defines the storage keys for localStorage.
 */
export const storageKeys = {
    /**
     * This key defines if the search should be fold out or not
     */
    searchFolded: "search-extended",
}

export const searchDates = {
    oldest: new Date("2008-01-01"),
    newest: new Date(Date.now()),
}

export let prestigeMap: Record<string, string> = {}

export async function initPrestigeMap() {
    prestigeMap = (await getPrestigeMap())!
}
