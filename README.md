# Compendium

This repository is the source for the [compendium website](https://compendium.skinship.xyz/), which is an extension of the [completed skins compendium](https://osu.ppy.sh/community/forums/topics/686664).

## Things to note for development

[Formats.md](https://gitlab.com/RockRoller/compendium/-/blob/master/formats.md) describes:

-   Format for user.html files
-   Format for skin.md files
-   List of all categories

Assets:

-   cover images go into /assets/img/covers/
-   all non brand icons are from [heroicons](https://heroicons.dev/)
-   new scss files should be added in \_sass and get imported by main.scss. There is only 1 stylesheet containing all css
-   each TS script should get its own folder inside /ts/
-   all functions, objects, classes, types, properties, etc. must have javadoc

## Batch updating skins:

-   set the config variable environment to "update" in order to get the details of a skin shown on the listing (doesnt work with the search, only the plain listing)

## Custom Jekyll Plugin

This site includes a custom jekyll plugin, mainly to speed up build times for specific things.

### Filters

All filters take both plain text inputs as well as variables as their input.

#### timestamp

{{ url | timestamp }}

This filter takes a string as an input and appends a question mark and the current timestamp to it. Used to bust the cache whenever the site rebuilds

#### concat_authors

{{ post.author | concat_authors }}

This filter takes an array of user IDs, gets their user names and concats them with the formatting that is wanted for skin-item.html

#### author_skins

{{ user_id | author_skins }}

This filter takes user_id as its ownly input and will return all skins made by that user_id as an array.

#### count_mode

{{ skins | count_mode: "mode" }}

This filter takes an array of posts and a game mode. It gives back a count of how many of those skins have the supplied game mode

#### get_user_name

{{ user_id | get_user_name }}

This filter takes user_id as its ownly input and will return the according username

#### get_author_file

{{ user_id | get_author_file }}

This filter takes user_id as its ownlt input and will return the according file from the authors collection (\_authors/)

#### filter_tags

{{ tags | filter_tags: "key" }}

This filter takes an array of tags and filters them for a wanted category. Valid Vategories are:
- `key-mode`: mania key mode tags
- `non-key-mode`: anything besides manua key mode tags
- `prestige`: contest and bulletin tags
- `other`: non prestige, non key mode tags

#### is_contest_role

{{ role_class | is_contest_role }}

Takes the classname of a role and checks if its a contest related role.

#### get_contest_id

{{ role_name | get_contest_id }}

Takes the name of a role and extracts the contest id from it
