require 'uri'
require 'json/ext'

# These maps can not be initiated via a function call, because access to @content is required
# Using a hook to populate these early would be possible, but more hassle than its worth

# user_id -> user_name
$MAP_SKINNERS_NAMES = {}
# user_id -> skins
$MAP_SKINNERS_SKINS = {}
# user_id -> _authors/ file
$MAP_SKINNERS_FILES = {}
# tag -> prestige level
$MAP_TAG_PRESTIGE = {}

$key_regex = /^[0-9]{1,2}k$/mi
$contest_winner_regex = /contest_\d{1,2}_winner/mi
$contest_second_regex = /contest_\d{1,2}_2nd_place/mi
$contest_third_regex = /contest_\d{1,2}_3rd_place/mi
$contest_submission_regex = /contest_?\d{0,2}_submission/mi
$soty_winner_regex = /skin_of_the_year_\d{4}_winner/mi
$soty_second_regex = /skin_of_the_year_\d{4}_2nd_place/mi
$soty_third_regex = /skin_of_the_year_\d{4}_3rd_place/mi
$soty_top10_regex = /skin_of_the_year_\d{4}_top_10/mi
$bulletin_regex = /skinners'_bulletin_showcase/mi

# This module contains various custom filters used by the site.
# Main focus in creating this was to increase build times, with increase in the ease of use as a side effect
# The Jekyll plugin documentation is sadly lackluster, its basically required to read Jekyll/Liquid source code
# Jekyll.logger.info is a useful utility for outputting to the console, however File.write also works
# Usual steps to do anything more advanced is to get the context and pull out the data that is required from there.
# Plugins always have access to context via @context.
# If one is ever not sure about the details of an object simply use .inspect, which is build into basically any Jekyll object
module Compendium
  # Takes an array of user_ids, gets their user_names and concats them as needed for skin-item.html
  # @param user_ids: array of user_id
  def concat_authors(user_ids)
    build_name_map(@context) if $MAP_SKINNERS_NAMES.empty?

    case user_ids.length
    when 0
      'by Unknown Author'
    when 1
      "by #{$MAP_SKINNERS_NAMES[user_ids[0]]}"
    when 2
      "by #{$MAP_SKINNERS_NAMES[user_ids[0]]} and #{$MAP_SKINNERS_NAMES[user_ids[1]]}"
    when 3
      "by #{$MAP_SKINNERS_NAMES[user_ids[0]]}, #{$MAP_SKINNERS_NAMES[user_ids[1]]} and #{$MAP_SKINNERS_NAMES[user_ids[2]]}"
    else
      'by Multiple Authors'
    end
  end

  # Takes a user_id and returns all skins made by that author
  # @param user_id: single user_id
  def author_skins(user_id)
    build_skins_map(@context) if $MAP_SKINNERS_SKINS.empty?

    skins = $MAP_SKINNERS_SKINS[user_id]
    skins.nil? ? [] : skins.reverse
  end

  # Takes an array of posts and returns how many of them have the given game mode skinned
  # @param posts: array of posts to check
  # @param mode: mode to check how many skins include it
  def count_mode(posts, mode)
    count = 0
    # catch users with no skins
    return count unless posts.respond_to?(:each)

    posts.each do |post|
      data = post.data
      # check if mode is included + catch skins with no game modes tagged
      count += 1 if data['game_modes'].respond_to?(:include?) && data['game_modes'].include?(mode)
    end
    count
  end

  # Takes a user_id and returns the user_name
  # @param user_id: singular user_id
  def get_user_name(user_id)
    build_name_map(context) if $MAP_SKINNERS_NAMES.empty?

    $MAP_SKINNERS_NAMES[user_id]
  end

  # Takes a user_id and returns the according file from the authors collection (_authors/)
  # @param user_id: a singular user_id
  def get_author_file(user_id)
    build_skinner_files_map(@context) if $MAP_SKINNERS_FILES.empty?

    $MAP_SKINNERS_FILES[user_id]
  end

  # Takes an array of tags and filters them for the wanted type
  # @param tags: array of tags to filter
  # @param type: defines what types to filter for
  def filter_tags(tags, type)
    filtered_tags = []

    case type
    # keymode (#k) tags
    when 'key-mode'
      tags.each do |tag|
        filtered_tags.push(tag) if $key_regex.match?(tag)
      end

    # specific prestige tags
    when 'contest-submission'
      tags.each do |tag|
        filtered_tags.push(tag) if $contest_submission_regex.match?(tag)
      end
    when 'contest-winner'
      tags.each do |tag|
        filtered_tags.push(tag) if $contest_winner_regex.match?(tag)
      end
    when 'contest-second'
      tags.each do |tag|
        filtered_tags.push(tag) if $contest_second_regex.match?(tag)
      end
    when 'contest-third'
      tags.each do |tag|
        filtered_tags.push(tag) if $contest_third_regex.match?(tag)
      end
    when 'skinners-bulletin'
      tags.each do |tag|
        filtered_tags.push(tag) if $bulletin_regex.match?(tag)
      end
    when 'soty-winner'
      tags.each do |tag|
        filtered_tags.push(tag) if $soty_winner_regex.match?(tag)
      end
    when 'soty-second'
      tags.each do |tag|
        filtered_tags.push(tag) if $soty_second_regex.match?(tag)
      end
    when 'soty-third'
      tags.each do |tag|
        filtered_tags.push(tag) if $soty_third_regex.match?(tag)
      end
    when 'soty-top10'
      tags.each do |tag|
        filtered_tags.push(tag) if $soty_top10_regex.match?(tag)
      end

    # non prestige, non key mode tags
    when 'other'
      tags.each do |tag|
        filtered_tags.push(tag) if !tag.include?('contest') && $bulletin_regex.match?(tag) && !$key_regex.match?(tag) && !tag.include?('skin_of_the_year')
      end
    end

    filtered_tags
  end

  # Takes an array of skins, checks if they use any non whitelisted image hosts and returns these hosts
  # @param array of skins
  def filter_hosts(skin_collection)
    bad_hosts = []
    whitelisted_hosts = @context.registers[:site].config['whitelisted_image_hosts']

    # catch skins without any variants
    [] if skin_collection.nil?

    skin_collection.each do |skin|
      screenshots = skin['screenshots']
      # catch skins without screenshots
      next if screenshots.nil?

      screenshots.each do |screenshot|
        uri = URI(screenshot)
        bad_hosts.push(uri.host) unless whitelisted_hosts.include?(uri.host)
      end
    end

    bad_hosts.uniq!
  end

  # takes the name of a role and extracts the contest id from it
  # @param role_name: name of the user badge
  def get_contest_id(role_name)
    role_name[/[0-9]+/m]
  end

  def get_prestige_level(tag)
    build_prestige_map(@context) if $MAP_TAG_PRESTIGE.empty?

    $MAP_TAG_PRESTIGE[tag]
  end

  # Builds the map of skins
  # user_id -> skins array
  # @param context: Liquid/Jekyll context to use for getting the posts collection
  def build_skins_map(context)
    # user_id => posts hash
    context.registers[:site].posts.docs.each do |post|
      data = post.data
      data['author'].each do |author|
        current_skins = $MAP_SKINNERS_SKINS[author]
        current_skins = [] if current_skins.nil?

        current_skins.push(post)
        $MAP_SKINNERS_SKINS[author] = current_skins
      end
    end
  end

  # Builds the map of names
  # user_id -> user_names
  # @param context: Liquid/Jekyll context to use for getting the authors collection
  def build_name_map(context)
    # user_id => user_name has
    context.registers[:site].collections['authors'].docs.each do |author_doc|
      data = author_doc.data
      $MAP_SKINNERS_NAMES[data['user_id']] = data['user_name']
    end
  end

  # Builds the map of author files
  # user_id -> _author/ file
  # @param context: Liquid/Jekyll context to use for getting the authors collection
  def build_skinner_files_map(context)
    # user_id => user_file has
    context.registers[:site].collections['authors'].docs.each do |author_doc|
      $MAP_SKINNERS_FILES[author_doc.data['user_id']] = author_doc
    end
  end

  def build_prestige_map(context)
    site = context.registers[:site]
    prestige_data = site.data['prestige']
    # Jekyll.logger.info prestige_data
    site.tags.each do |tag|
      stringName = tag[0]
      prestige_level = ''

      # if a tag can ever have multiple prestige levels this check will fail
      if $key_regex.match?(stringName)
        prestige_level = "key"
      elsif $contest_winner_regex.match?(stringName)
        prestige_level = "c1"
      elsif $contest_second_regex.match?(stringName)
        prestige_level = "c2"
      elsif $contest_third_regex.match?(stringName)
        prestige_level = "c3"
      elsif $contest_submission_regex.match?(stringName)
        prestige_level = "cs"
      elsif $soty_winner_regex.match?(stringName)
        prestige_level = "soty-1"
      elsif $soty_second_regex.match?(stringName)
        prestige_level = "soty-2"
      elsif $soty_third_regex.match?(stringName)
        prestige_level = "soty-3"
      elsif $soty_top10_regex.match?(stringName)
        prestige_level = "soty-t10"
      elsif $bulletin_regex.match?(stringName)
        prestige_level = "sb"
      end

      $MAP_TAG_PRESTIGE[stringName] = prestige_level
    end
  end
end

Liquid::Template.register_filter(Compendium)
