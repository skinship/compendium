---
layout: skin

skin_name: "Fullmetal Alchemist"
forum_thread_id: 696844
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7975654
resolutions:
    - hd
ratios:
    - "all"
tags:
    - "fullmetal_alchemist"
    - "fma"
    - "fmab"
    - "fullmetal_alchemist:_brotherhood"
categories:
    - anime
skin_collection:
    - name: "Fullmetal Alchemist"
      screenshots:
          - "http://i.imgur.com/crpnO5i.jpg"
          - "http://i.imgur.com/sj8Ulzc.jpg"
          - "http://i.imgur.com/n77ei2m.jpg"
          - "http://i.imgur.com/3q5Plye.jpg"
          - "http://i.imgur.com/Zx52ymR.jpg"
          - "http://i.imgur.com/oa01TL6.png"
          - "http://i.imgur.com/H5gd0tu.png"
          - "http://i.imgur.com/smZAPka.jpg"
          - "http://i.imgur.com/hujH7qe.png"
          - "http://i.imgur.com/HgD8VRd.jpg"
videos:
    skinship:
    author:
---
