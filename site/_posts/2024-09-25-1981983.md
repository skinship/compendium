---
layout: skin

skin_name: "Shiva"
forum_thread_id: 1981983
date_added: 2024-12-22
game_modes:
    - standard
author:
    - 15670087
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "winter"
    - "final_fantasy"
categories:
    - game
skin_collection:
    - name: "Shiva"
      screenshots:
          - "https://i.imgur.com/uBaBM1O.png"
          - "https://i.imgur.com/4tEZm7k.jpg"
          - "https://i.imgur.com/JXjjqRg.jpg"
          - "https://i.imgur.com/E0Pl4kU.jpg"
          - "https://i.imgur.com/B6m3jYM.jpg"
          - "https://i.imgur.com/exhWSow.jpg"
videos:
    skinship:
    author:
        - 4P2PD25H928
---
