---
layout: skin

skin_name: "Project Siesta"
forum_thread_id: 1390646
date_added: 2021-08-31
game_modes:
    - standard
author:
    - 16086912
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "the_detective_is_already_dead"
    - "tantei_wa_mou,_shindeiru."
    - "siesta"
categories:
    - anime
skin_collection:
    - name: "Project Siesta"
      screenshots:
          - "https://i.imgur.com/9W4fGOg.png"
          - "https://i.imgur.com/pKp1K8Y.png"
          - "https://i.imgur.com/Fvts6kq.png"
          - "https://i.imgur.com/dGzwaDt.png"
          - "https://i.imgur.com/GHIQioR.png"
          - "https://i.imgur.com/rK5dyI6.png"
          - "https://i.imgur.com/6gOB5rh.png"
          - "https://i.imgur.com/VeJ38SJ.png"
          - "https://i.imgur.com/NVmn4QI.png"
          - "https://i.imgur.com/LXa0w5z.png"
          - "https://i.imgur.com/hAjzKky.png"
          - "https://i.imgur.com/yR1IGtm.png"
videos:
    skinship:
    author:
---
