---
layout: skin

skin_name: "BPC DOTMATRIX"
forum_thread_id: 1653845
date_added: 2022-11-22
game_modes:
    - standard
    - taiko
author:
    - 4236855
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "BPC DOTMATRIX"
      screenshots:
          - "https://i.imgur.com/yTyrgVV.png"
          - "https://i.imgur.com/ERimQ8e.png"
          - "https://i.imgur.com/n3e6tZT.png"
          - "https://i.imgur.com/kAhgc4E.png"
          - "https://i.imgur.com/LireRkW.png"
videos:
    skinship:
    author:
---
