---
layout: skin

skin_name: "Honoka Kousaka"
forum_thread_id: 890456
date_added: 2021-07-18
game_modes:
    - standard
    - catch
author:
    - 7087699
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "honoka_kosaka"
    - "muse"
    - "love_live"
    - "printemps"
    - "μ's"
categories:
    - anime
skin_collection:
    - name: "Honoka Kousaka"
      screenshots:
          - "https://imgur.com/h44O1wG.png"
          - "https://imgur.com/AsdyclH.png"
          - "https://imgur.com/xiVkhO0.png"
          - "https://imgur.com/WzLvzfP.png"
          - "https://imgur.com/DgIq7jJ.png"
          - "https://imgur.com/cv1irsw.png"
          - "https://imgur.com/roLD8Kx.png"
          - "https://imgur.com/FgYvTCx.png"
          - "https://imgur.com/Rb8B7ya.png"
          - "https://imgur.com/5Zy2DaO.png"
          - "https://imgur.com/whAqkDY.png"
          - "https://imgur.com/T9bHZ5E.png"
          - "https://imgur.com/SeS89g7.png"
          - "https://imgur.com/6mRqa7L.png"
          - "https://imgur.com/80sRWxe.png"
          - "https://imgur.com/sQJ9Y1y.png"
          - "https://imgur.com/iNIQ9QR.png"
          - "https://imgur.com/B928ib5.png"
          - "https://imgur.com/phnMM2N.png"
videos:
    skinship:
    author:
---
