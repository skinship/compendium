---
layout: skin

skin_name: "Windosu!"
forum_thread_id: 1926832
date_added: 2024-07-11
game_modes:
    - standard
    - mania
    - catch
author:
    - 30298378
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "windows"
categories:
    - minimalistic
skin_collection:
    - name: "Citrusis"
      screenshots:
          - "https://i.imgur.com/N3Gao78.png"
          - "https://i.imgur.com/W1ZAm1G.png"
          - "https://i.imgur.com/8n3Mew0.png"
          - "https://i.imgur.com/uMvyhCJ.png"
          - "https://i.imgur.com/Qy3u8bo.png"
          - "https://i.imgur.com/tOHsfLf.png"
          - "https://i.imgur.com/80ikEgX.png"
videos:
    skinship:
    author:
---
