---
layout: skin

skin_name: "American Clown Fairy"
forum_thread_id: 538020
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6084778
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "clownpiece"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "American Clown Fairy"
      screenshots:
          - "http://i.imgur.com/4XVXzFV.jpg"
          - "http://i.imgur.com/KzMoKuT.jpg"
          - "http://i.imgur.com/Yr2wWwh.jpg"
          - "http://i.imgur.com/KS2KfSi.jpg"
          - "http://i.imgur.com/z2mXWSJ.jpg"
          - "http://i.imgur.com/AtziNxZ.jpg"
          - "http://i.imgur.com/5MGJlSF.jpg"
          - "http://i.imgur.com/92dlYdZ.jpg"
          - "http://i.imgur.com/hczvmkR.jpg"
          - "http://i.imgur.com/3fcwlmC.jpg"
          - "http://i.imgur.com/JuGHa4C.jpg"
          - "http://i.imgur.com/Pr3Aqqc.jpg"
          - "http://i.imgur.com/hcDASNg.jpg"
          - "http://i.imgur.com/3Rkw3pV.jpg"
videos:
    skinship:
    author:
---
