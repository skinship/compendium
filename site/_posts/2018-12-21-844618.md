---
layout: skin

skin_name: "Simplinimalism"
forum_thread_id: 844618
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 10959501
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Simplinimalism"
      screenshots:
          - "https://i.imgur.com/S1DJ2ij.jpg"
          - "https://i.imgur.com/tGz8Amh.jpg"
          - "https://i.imgur.com/gQa5zoP.png"
          - "https://i.imgur.com/PbLSGvw.png"
          - "https://i.imgur.com/rP5YBcK.png"
videos:
    skinship:
    author:
---
