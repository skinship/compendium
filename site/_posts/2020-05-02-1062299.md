---
layout: skin

skin_name: "Rocket League"
forum_thread_id: 1062299
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 7087699
    - 13929527
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "rocket_league"
categories:
    - eyecandy
    - game
skin_collection:
    - name: "Rocket League"
      screenshots:
          - "http://i.imgur.com/BM2d45W.png"
          - "http://i.imgur.com/ftDTXb4.png"
          - "http://i.imgur.com/DsfVafz.png"
          - "http://i.imgur.com/nWGC2wW.png"
          - "http://i.imgur.com/5U132W8.png"
          - "http://i.imgur.com/KNz7tIc.png"
          - "http://i.imgur.com/ISXz7qs.png"
          - "http://i.imgur.com/87hG7Nd.png"
          - "http://i.imgur.com/5B7xsQp.png"
          - "http://i.imgur.com/Db2olnS.png"
          - "http://i.imgur.com/q2p2ivK.png"
videos:
    skinship:
    author:
---
