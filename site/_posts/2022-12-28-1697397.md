---
layout: skin

skin_name: "Chip"
forum_thread_id: 1697397
date_added: 2023-01-25
game_modes:
    - standard
author:
    - 7306251
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Chip"
      screenshots:
          - "https://i.imgur.com/eabqZXl.png"
          - "https://i.imgur.com/EG154Wc.png"
          - "https://i.imgur.com/xw7hYmI.png"
          - "https://i.imgur.com/zgA1pAO.png"
          - "https://i.imgur.com/LWqUff3.png"
          - "https://i.imgur.com/m8UhtRy.png"
          - "https://i.imgur.com/dhL3ZHb.png"
          - "https://i.imgur.com/mcvTstv.png"
          - "https://i.imgur.com/lZF2hJK.png"
          - "https://i.imgur.com/po89mF7.png"
          - "https://i.imgur.com/AGwUmII.png"
          - "https://i.imgur.com/l2L4eJR.png"
videos:
    skinship:
    author:
---
