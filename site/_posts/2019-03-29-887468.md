---
layout: skin

skin_name: "Clear Skin Ultra 4.0"
forum_thread_id: 887468
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 5212789
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
    - "16:10"
    - "21:9"
tags:
    - "lazer"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "Clear Skin Ultra 4.0"
      screenshots:
          - "http://i.imgur.com/0nJb4QU.png"
          - "http://i.imgur.com/F5dh9S2.png"
          - "http://i.imgur.com/RQnZLzK.png"
          - "http://i.imgur.com/QvPJm0B.png"
          - "http://i.imgur.com/CsTs0WE.png"
          - "http://i.imgur.com/lr582y6.png"
          - "http://i.imgur.com/SUwRVkd.png"
          - "http://i.imgur.com/CXV2Olh.png"
videos:
    skinship:
    author:
---
