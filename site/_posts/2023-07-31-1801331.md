---
layout: skin

skin_name: "GAME OVER"
forum_thread_id: 1801331
date_added: 2023-12-23
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 6234482
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "retro"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "hsr"
    - "honkai:_star_rail"
    - "silver_wolf"
    - "arcade"
    - "skin_of_the_year_2023_top_10"
categories:
    - anime
    - eyecandy
    - game
skin_collection:
    - name: "GAME OVER"
      screenshots:
          - "https://i.imgur.com/6R35vVc.png"
          - "https://i.imgur.com/uG7w3Ww.png"
          - "https://i.imgur.com/F93IvBM.png"
          - "https://i.imgur.com/uf5RjXr.png"
          - "https://i.imgur.com/NkeKL0N.png"
          - "https://i.imgur.com/iokhYxd.png"
          - "https://i.imgur.com/rUg79aD.png"
          - "https://i.imgur.com/4L2kOsa.png"
          - "https://i.imgur.com/Bvzrw8s.png"
          - "https://i.imgur.com/ThzlOwt.png"
          - "https://i.imgur.com/xzP0sCM.png"
          - "https://i.imgur.com/8b6BEi3.png"
          - "https://i.imgur.com/TOdSwYR.png"
          - "https://i.imgur.com/NpYoyUW.png"
          - "https://i.imgur.com/025Qs1M.png"
          - "https://i.imgur.com/HDnRIc9.png"
          - "https://i.imgur.com/xagY3yN.png"
    - name: "Stylistic"
      screenshots:
          - "https://i.imgur.com/4L2kOsa.png"
    - name: "Performance"
      screenshots:
          - "https://i.imgur.com/Bvzrw8s.png"
videos:
    skinship:
    author:
---
