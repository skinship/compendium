---
layout: skin

skin_name: "Heterochromia"
forum_thread_id: 564469
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 5266889
resolutions:
ratios:
tags:
categories:
    - anime
skin_collection:
    - name: "Heterochromia"
      screenshots:
          - "http://i.imgur.com/4BLjIW1.jpg"
          - "http://i.imgur.com/yG28E86.jpg"
          - "http://i.imgur.com/I3liMdl.jpg"
          - "http://i.imgur.com/eXmoUsy.jpg"
          - "http://i.imgur.com/3AqrB7Z.jpg"
          - "http://i.imgur.com/I5myzcH.jpg"
          - "http://i.imgur.com/4nPlzr4.jpg"
          - "http://i.imgur.com/QFj0mgp.jpg"
videos:
    skinship:
    author:
---
