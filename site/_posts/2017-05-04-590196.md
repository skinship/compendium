---
layout: skin

skin_name: "Undertale Skin"
forum_thread_id: 590196
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 6877973
resolutions:
ratios:
tags:
    - "undertale"
categories:
    - game
skin_collection:
    - name: "Undertale Skin"
      screenshots:
          - "http://i.imgur.com/cqNrcLp.jpg"
          - "http://i.imgur.com/Dsk2Zi3.png"
          - "http://i.imgur.com/WFYzz40.jpg"
          - "http://i.imgur.com/aGenYxH.png"
          - "http://i.imgur.com/WH9fS5x.png"
          - "http://i.imgur.com/m40IAJD.png"
          - "http://i.imgur.com/QIM8MQg.png"
videos:
    skinship:
    author:
---
