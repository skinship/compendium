---
layout: skin

skin_name: "ironmouse.waifuJam();"
forum_thread_id: 1773210
date_added: 2023-11-06
game_modes:
    - standard
author:
    - 16274977
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "vtuber"
    - "ironmouse"
    - "vshojo"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "ironmouse.waifuJam();"
      screenshots:
          - "https://i.imgur.com/ZF2EosH.jpg"
          - "https://i.imgur.com/9OSXxWp.jpg"
          - "https://i.imgur.com/yMAaTpz.jpg"
          - "https://i.imgur.com/LqHzwGF.jpg"
          - "https://i.imgur.com/GHQlEFK.jpg"
          - "https://i.imgur.com/YMo59tX.jpg"
videos:
    skinship:
    author:
---
