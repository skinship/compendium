---
layout: skin

skin_name: "Fluttr"
forum_thread_id: 946426
date_added: 2021-06-28
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 4202284
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Fluttr"
      screenshots:
          - "https://i.imgur.com/Ktwjf0n.png"
          - "https://i.imgur.com/uLCq06m.png"
          - "https://i.imgur.com/xEGxXMb.png"
          - "https://i.imgur.com/va6AVPL.jpg"
          - "https://i.imgur.com/MCcEyTJ.jpg"
          - "https://i.imgur.com/pdIezc4.jpg"
          - "https://i.imgur.com/m1vT7bh.jpg"
          - "https://i.imgur.com/CO9BOie.png"
          - "https://i.imgur.com/cd91BxY.png"
          - "https://i.imgur.com/0cF6h1L.png"
          - "https://i.imgur.com/SGTmXaJ.png"
          - "https://i.imgur.com/FIFQCWe.png"
          - "https://i.imgur.com/SSjuDpx.png"
videos:
    skinship:
    author:
---
