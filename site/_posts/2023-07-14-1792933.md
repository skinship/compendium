---
layout: skin

skin_name: "Ghost Rule"
forum_thread_id: 1792933
date_added: 2023-07-20
game_modes:
    - standard
    - catch
author:
    - 7122165
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "hatsune_miku"
    - "vocaloid"
    - "beatmap"
    - "contest_4_submission"
    - "ghost_rule"
    - "skin_of_the_year_2023_top_10"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Ghost Rule"
      screenshots:
          - "https://i.imgur.com/dXU21sC.png"
          - "https://i.imgur.com/a3WSJga.png"
          - "https://i.imgur.com/wJKjSi1.png"
          - "https://i.imgur.com/hTSUlli.png"
          - "https://i.imgur.com/VlgsYGF.png"
          - "https://i.imgur.com/FP36GFT.png"
          - "https://i.imgur.com/aDeWlR5.png"
          - "https://i.imgur.com/iJg0OjZ.png"
          - "https://i.imgur.com/gIYdPoU.png"
          - "https://i.imgur.com/LjXx3jZ.png"
          - "https://i.imgur.com/J0cQoNX.png"
          - "https://i.imgur.com/Of6ORiA.png"
          - "https://i.imgur.com/ziWlzk8.png"
          - "https://i.imgur.com/01BSnpX.png"
videos:
    skinship:
        - GG51VmnZUwc
    author:
---
