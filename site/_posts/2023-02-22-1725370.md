---
layout: skin

skin_name: "Nanakusa Nazuna"
forum_thread_id: 1725370
date_added: 2024-01-09
game_modes:
    - standard
author:
    - 27667824
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "call_of_the_night"
    - "yofukashi_no_uta"
    - "nanakusa_nazuna"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Nanakusa Nazuna"
      screenshots:
          - "https://i.imgur.com/TxnSqJz.png"
          - "https://i.imgur.com/u8nViWe.png"
          - "https://i.imgur.com/a1iSaNy.png"
          - "https://i.imgur.com/w2lqez9.png"
          - "https://i.imgur.com/XQSnHOj.png"
          - "https://i.imgur.com/mHNUzKx.png"
          - "https://i.imgur.com/D8Fu3Hn.png"
          - "https://i.imgur.com/9EWkyao.png"
          - "https://i.imgur.com/xQlsvg9.png"
videos:
    skinship:
    author:
---
