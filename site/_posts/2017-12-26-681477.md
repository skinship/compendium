---
layout: skin

skin_name: "Armageddonia"
forum_thread_id: 681477
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 8712289
resolutions:
    - hd
    - sd
ratios:
tags:
    - "worms"
    - "worms_armageddon"
categories:
    - game
skin_collection:
    - name: "Armageddonia"
      screenshots:
          - "https://i.imgur.com/Qppng5J.png"
          - "https://i.imgur.com/2bmfkxs.png"
          - "https://i.imgur.com/AB6mhiA.png"
          - "https://i.imgur.com/wdSCHpE.png"
          - "https://i.imgur.com/CGcQlbs.png"
          - "https://i.imgur.com/T0U9BpI.png"
          - "https://i.imgur.com/pDwVx1u.png"
          - "https://i.imgur.com/0MPn5Y5.png"
          - "https://i.imgur.com/Ybm8AYF.png"
          - "https://i.imgur.com/wBiAxGj.png"
          - "https://i.imgur.com/7fm90mE.png"
          - "https://i.imgur.com/WrcqIv8.png"
videos:
    skinship:
    author:
---
