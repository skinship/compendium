---
layout: skin

skin_name: "[カンナカムイ] Kanna"
forum_thread_id: 1629550
date_added: 2023-02-07
game_modes:
    - standard
author:
    - 25025133
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "kanna_kamui"
    - "miss_kobayashi's_dragon_maid"
    - "kobayashi-san_chi_no_maid_dragon"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "[カンナカムイ] Kanna"
      screenshots:
          - "https://i.imgur.com/Q9TyDov.jpg"
          - "https://i.imgur.com/PALb3xq.png"
          - "https://i.imgur.com/xzqe9bt.png"
          - "https://i.imgur.com/C2121fK.png"
          - "https://i.imgur.com/inhsVzL.png"
          - "https://i.imgur.com/DLQGsTj.png"
          - "https://i.imgur.com/VpGkkk4.png"
          - "https://i.imgur.com/OHZKFmC.png"
videos:
    skinship:
    author:
---
