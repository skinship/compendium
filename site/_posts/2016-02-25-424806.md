---
layout: skin

skin_name: "Comfort"
forum_thread_id: 424806
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 3252321
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Comfort"
      screenshots:
          - "http://i.imgur.com/KyyXErC.jpg"
          - "http://i.imgur.com/Rsh93Ow.jpg"
          - "http://i.imgur.com/qch6LE4.jpg"
          - "http://i.imgur.com/fI4Q2TA.jpg"
          - "http://i.imgur.com/8xqI58Q.jpg"
          - "http://i.imgur.com/3z28jDW.jpg"
          - "http://i.imgur.com/ZfDZSmB.jpg"
          - "http://i.imgur.com/OktrDvz.jpg"
videos:
    skinship:
    author:
---
