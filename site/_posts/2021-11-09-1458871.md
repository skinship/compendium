---
layout: skin

skin_name: "JUIFEED"
forum_thread_id: 1458871
date_added: 2021-12-08
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 9452719
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - minimalistic
skin_collection:
    - name: "JUIFEED"
      screenshots:
          - "https://i.imgur.com/K35UAA5.png"
          - "https://i.imgur.com/mvY93Kj.png"
          - "https://i.imgur.com/ZO0mnjF.png"
          - "https://i.imgur.com/s2xABBy.png"
          - "https://i.imgur.com/bE9QYu5.png"
          - "https://i.imgur.com/qcWYVAk.png"
          - "https://i.imgur.com/Yg0cvpg.png"
          - "https://i.imgur.com/UZe4R08.png"
          - "https://i.imgur.com/bUVwWft.png"
          - "https://i.imgur.com/vbjAl3Z.png"
          - "https://i.imgur.com/oWOCQqe.png"
          - "https://i.imgur.com/8XijDv0.png"
          - "https://i.imgur.com/DYkLbZh.png"
          - "https://i.imgur.com/cJcU3ge.png"
videos:
    skinship:
    author:
---
