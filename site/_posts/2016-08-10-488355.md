---
layout: skin

skin_name: "Medicine Melancholy"
forum_thread_id: 488355
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 8178299
resolutions:
    - sd
ratios:
    - "16:9"
tags:
    - "medicine_melancholy"
    - "medicine_melanchory"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "Medicine Melancholy"
      screenshots:
          - "https://i.imgur.com/ZaQntg9.png"
          - "https://i.imgur.com/gwgbGOi.png"
          - "https://i.imgur.com/aWCmPk8.png"
          - "https://i.imgur.com/HS2gzlY.png"
          - "https://i.imgur.com/cB2l02Z.png"
          - "https://i.imgur.com/8MO4Gcj.png"
          - "https://i.imgur.com/ZQ9da3q.png"
          - "https://i.imgur.com/X1kmT4U.png"
          - "https://i.imgur.com/V6Nm8ia.png"
          - "https://i.imgur.com/ZLyR2K4.png"
videos:
    skinship:
    author:
---
