---
layout: skin

skin_name: "O2Jam U Ultimate"
forum_thread_id: 247348
date_added: 2021-06-24
game_modes:
    - mania
author:
    - 1454484
resolutions:
    - hd
ratios:
tags:
    - "o2jam"
    - "vsrg"
categories:
    - game
skin_collection:
    - name: "O2Jam U Ultimate"
      screenshots:
          - "http://i.imgur.com/EHpO39p.jpg"
          - "http://i.imgur.com/gwGvu2F.jpg"
          - "http://i.imgur.com/UoBD6BF.png"
          - "http://i.imgur.com/raGiA3h.png"
          - "http://i.imgur.com/fELYnAa.png"
videos:
    skinship:
    author:
---
