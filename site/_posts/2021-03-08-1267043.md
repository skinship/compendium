---
layout: skin

skin_name: "Space [Universal Apocalypse]"
forum_thread_id: 1267043
date_added: 2021-07-15
game_modes:
    - standard
author:
    - 13845312
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "space"
    - "galaxy"
    - "planets"
categories:
    - minimalistic
skin_collection:
    - name: "Space [Universal Apocalypse]"
      screenshots:
          - "https://i.imgur.com/9ItNCtn.jpeg"
          - "https://i.imgur.com/6JCRsCt.jpeg"
          - "https://i.imgur.com/cOYJKdR.jpeg"
          - "https://i.imgur.com/Cf4PzrW.jpeg"
          - "https://i.imgur.com/DWSAd90.jpeg"
          - "https://i.imgur.com/VKZwfdE.jpeg"
          - "https://i.imgur.com/t2zhGE3.jpeg"
          - "https://i.imgur.com/1i8gqlA.jpeg"
          - "https://i.imgur.com/2QCHY7L.jpeg"
          - "https://i.imgur.com/KRfT8Kc.jpeg"
videos:
    skinship:
        - U_RkN5Ph1k4
    author:
---
