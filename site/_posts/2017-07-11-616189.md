---
layout: skin

skin_name: "The Font is Sans"
forum_thread_id: 616189
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 8712289
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "The Font is Sans"
      screenshots:
          - "https://i.imgur.com/EkSerIx.png"
          - "https://i.imgur.com/fKh9LcD.png"
          - "https://i.imgur.com/WyjR8DA.png"
          - "https://i.imgur.com/MhWqgYP.png"
          - "https://i.imgur.com/wdRxMKt.png"
          - "https://i.imgur.com/FVVf5jI.png"
          - "https://i.imgur.com/mG6CndO.png"
          - "https://i.imgur.com/z80qZIS.png"
          - "https://i.imgur.com/QdlZeEX.png"
          - "https://i.imgur.com/3NMSQM3.png"
          - "https://i.imgur.com/hxMDyn5.png"
          - "https://i.imgur.com/3UizBKm.png"
          - "https://i.imgur.com/VK6Cm8A.png"
          - "https://i.imgur.com/mLGt84J.png"
          - "https://i.imgur.com/Fb9tgv7.png"
          - "https://i.imgur.com/wePzTRA.png"
          - "https://i.imgur.com/a3TjaJs.png"
          - "https://i.imgur.com/Qg21o1s.png"
          - "https://i.imgur.com/aLBLaLJ.png"
          - "https://i.imgur.com/0FvJAWT.png"
          - "https://i.imgur.com/8jpDcQX.png"
          - "https://i.imgur.com/xasRI4L.png"
videos:
    skinship:
    author:
---
