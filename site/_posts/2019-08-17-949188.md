---
layout: skin

skin_name: "Shinobu"
forum_thread_id: 949188
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6940341
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "monogatari"
    - "oshino_shinobu"
    - "kiss-shot_acerola-orion_heart-under-blade"
categories:
    - anime
skin_collection:
    - name: "Shinobu"
      screenshots:
          - "https://i.imgur.com/Bvl1vJC.jpg"
          - "https://i.imgur.com/nVXINbW.jpg"
          - "https://i.imgur.com/QqAY6OA.jpg"
          - "https://i.imgur.com/1Tqriao.jpg"
          - "https://i.imgur.com/8U8qixL.jpg"
          - "https://i.imgur.com/aqdb1x3.jpg"
          - "https://i.imgur.com/vujd4k5.jpg"
          - "https://i.imgur.com/hoxanxb.jpg"
          - "https://i.imgur.com/eQIagct.jpg"
          - "https://i.imgur.com/wWDHpa7.jpg"
          - "https://i.imgur.com/QTbcTLW.jpg"
videos:
    skinship:
    author:
---
