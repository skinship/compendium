---
layout: skin

skin_name: "東方Project - Kirisame Marisa"
forum_thread_id: 530165
date_added: 2021-06-28
game_modes:
    - standard
    - catch
author:
    - 3717733
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "kirisame_marisa"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "東方Project - Kirisame Marisa"
      screenshots:
          - "http://i.imgur.com/uvMi6fY.jpg"
          - "http://i.imgur.com/ZtYztjc.png"
          - "http://i.imgur.com/W24UBH9.jpg"
          - "http://i.imgur.com/lVaP82P.jpg"
          - "http://i.imgur.com/U2EBB17.jpg"
          - "http://i.imgur.com/RFGniy2.jpg"
          - "http://i.imgur.com/xspq9CE.png"
          - "http://i.imgur.com/qEPXv3w.jpg"
videos:
    skinship:
    author:
---
