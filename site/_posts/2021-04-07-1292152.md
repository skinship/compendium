---
layout: skin

skin_name: "冷戦(Reisen)"
forum_thread_id: 1292152
date_added: 2021-07-15
game_modes:
    - standard
author:
    - 20727092
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "reisen"
    - "udongein_inaba"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "冷戦(Reisen)"
      screenshots:
          - "https://i.imgur.com/kHojn99.jpeg"
          - "https://i.imgur.com/UIxoGYy.jpeg"
          - "https://i.imgur.com/IGBS3MP.jpeg"
          - "https://i.imgur.com/Gz9ZAoi.jpeg"
          - "https://i.imgur.com/i3dmpSb.jpeg"
          - "https://i.imgur.com/lsdlJgn.jpeg"
          - "https://i.imgur.com/XYNTV1T.jpeg"
          - "https://i.imgur.com/wYstBrS.jpeg"
          - "https://i.imgur.com/WuNZ3Ap.jpeg"
videos:
    skinship:
        - MSm_I99WInI
    author:
---
