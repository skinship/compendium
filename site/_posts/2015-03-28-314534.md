---
layout: skin

skin_name: "Persona 5"
forum_thread_id: 314534
date_added: 2021-07-18
game_modes:
    - standard
author:
    - 4334870
resolutions:
    - hd
    - sd
ratios:
tags:
    - "persona_5"
    - "persona"
categories:
    - anime
    - game
skin_collection:
    - name: "Persona 5"
      screenshots:
          - "http://i.imgur.com/xg7FHKb.jpg"
          - "http://i.imgur.com/9ZvNGTj.png"
          - "http://i.imgur.com/njJ6gQy.png"
          - "http://i.imgur.com/zlH3R6J.png"
          - "http://i.imgur.com/f8Lh9LK.png"
videos:
    skinship:
    author:
---
