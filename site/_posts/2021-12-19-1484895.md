---
layout: skin

skin_name: "HotChocolate"
forum_thread_id: 1484895
date_added: 2022-01-14
game_modes:
    - standard
    - taiko
author:
    - 9634575
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_2_submission"
    - "hot_chocolate"
categories:
    - minimalistic
skin_collection:
    - name: "HotChocolate"
      screenshots:
          - "https://i.imgur.com/Vvvhnfz.jpeg"
          - "https://i.imgur.com/k9gZjRs.jpeg"
          - "https://i.imgur.com/DlDYR7h.jpeg"
          - "https://i.imgur.com/pKJmglB.jpeg"
          - "https://i.imgur.com/j9wHGoM.jpeg"
          - "https://i.imgur.com/iQgHviJ.jpeg"
          - "https://i.imgur.com/q2uxBXS.jpeg"
          - "https://i.imgur.com/QQpXM3k.jpeg"
          - "https://i.imgur.com/B2XicYA.jpeg"
          - "https://i.imgur.com/cw4SBA5.jpeg"
          - "https://i.imgur.com/93FzJg1.jpeg"
          - "https://i.imgur.com/76Nqk9E.jpeg"
          - "https://i.imgur.com/citJCtb.jpeg"
videos:
    skinship:
        - 0trIPf60pOI
    author:
---
