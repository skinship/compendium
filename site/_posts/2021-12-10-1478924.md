---
layout: skin

skin_name: "Tsukihi Phoenix"
forum_thread_id: 1478924
date_added: 2022-02-01
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 6234482
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "monogatari"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "araragi_tsukihi"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Tsukihi Phoenix"
      screenshots:
          - "https://i.imgur.com/SttlMSX.jpeg"
          - "https://i.imgur.com/QiAnhYl.jpeg"
          - "https://i.imgur.com/nekcZqO.jpeg"
          - "https://i.imgur.com/ivHNMrh.jpeg"
          - "https://i.imgur.com/ktUgFPT.jpeg"
          - "https://i.imgur.com/QNhym5f.jpeg"
          - "https://i.imgur.com/6fzOY6z.jpeg"
          - "https://i.imgur.com/EGbkjpa.jpeg"
          - "https://i.imgur.com/fB8XYo9.jpeg"
          - "https://i.imgur.com/lg0JWsL.jpeg"
          - "https://i.imgur.com/PL1J0OZ.jpeg"
          - "https://i.imgur.com/Xa7Dun2.jpeg"
          - "https://i.imgur.com/XpStBu7.jpeg"
          - "https://i.imgur.com/2VVz9go.jpeg"
          - "https://i.imgur.com/tRDLr3m.jpeg"
          - "https://i.imgur.com/wemGXCy.jpeg"
          - "https://i.imgur.com/EIeH8Hv.jpeg"
videos:
    skinship:
    author:
---
