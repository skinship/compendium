---
layout: skin

skin_name: "Chino Kafuu"
forum_thread_id: 897591
date_added: 2021-07-18
game_modes:
    - standard
    - catch
author:
    - 7087699
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "is_the_order_a_rabbit?"
    - "gochuumon_wa_usagi_desu_ka?"
    - "gochiusa"
    - "chino_kafuu"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Chino Kafuu"
      screenshots:
          - "https://i.imgur.com/5xTYQXO.jpg"
          - "https://i.imgur.com/gjnWCk9.jpg"
          - "https://i.imgur.com/kuC1X8L.jpg"
          - "https://i.imgur.com/ONmdqHJ.jpg"
          - "https://i.imgur.com/iclvA4R.jpg"
          - "https://i.imgur.com/G5ENjgW.jpg"
          - "https://i.imgur.com/Yx1Vu3S.jpg"
          - "https://i.imgur.com/1QqtHTm.jpg"
          - "https://i.imgur.com/ydnuBRV.jpg"
          - "https://i.imgur.com/mh9I73I.jpg"
          - "https://i.imgur.com/RKyUUvS.jpg"
          - "https://i.imgur.com/4XWd90V.jpg"
          - "https://i.imgur.com/zYfhq85.jpg"
          - "https://i.imgur.com/PPbjU2l.jpg"
          - "https://i.imgur.com/3gC65YI.jpg"
          - "https://i.imgur.com/lGd2aBF.jpg"
          - "https://i.imgur.com/Xwxi9Po.jpg"
          - "https://i.imgur.com/YGasRFU.jpg"
          - "https://i.imgur.com/kt1xCoD.jpg"
videos:
    skinship:
    author:
---
