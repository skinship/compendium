---
layout: skin

skin_name: "The adventures of ryunosuke seibu (成歩堂龍ノ介の冒険)"
forum_thread_id: 1908862
date_added: 2024-06-20
game_modes:
    - standard
    - mania
author:
    - 24673124
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "contest_5_submission"
    - "the_great_ace_attorney"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "The adventures of ryunosuke seibu (成歩堂龍ノ介の冒険)"
      screenshots:
          - "https://i.imgur.com/8ErqNOL.jpg"
          - "https://i.imgur.com/lvveECk.png"
          - "https://i.imgur.com/1OJUTjQ.png"
          - "https://i.imgur.com/NymEfV6.jpg"
          - "https://i.imgur.com/Y7VJvSc.jpg"
          - "https://i.imgur.com/sFjOueD.png"
          - "https://i.imgur.com/QWjHO9g.png"
          - "https://i.imgur.com/Zxdygaf.png"
          - "https://i.imgur.com/OSkcuMr.png"
videos:
    skinship:
        - Q2HdBKKS48Q
    author:
---
