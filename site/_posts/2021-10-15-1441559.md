---
layout: skin

skin_name: "24Hour Cinderella"
forum_thread_id: 1441559
date_added: 2021-12-08
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 22267757
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "yakuza"
    - "4k"
    - "5k"
    - "7k"
categories:
    - eyecandy
    - game
skin_collection:
    - name: "24Hour Cinderella"
      screenshots:
          - "https://i.imgur.com/KcOwXrw.png"
          - "https://i.imgur.com/E5GDcHa.png"
          - "https://i.imgur.com/TiYsclD.png"
          - "https://i.imgur.com/eQJaR8D.png"
          - "https://i.imgur.com/WXNGLkD.png"
          - "https://i.imgur.com/6igThjY.png"
          - "https://i.imgur.com/zV5qMTI.png"
          - "https://i.imgur.com/bpqYBUN.png"
          - "https://i.imgur.com/Mn45GaY.png"
          - "https://i.imgur.com/eRBKD2K.png"
          - "https://i.imgur.com/nQYPU4e.png"
          - "https://i.imgur.com/7CHk4WS.png"
videos:
    skinship:
    author:
---
