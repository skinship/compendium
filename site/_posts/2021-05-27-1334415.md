---
layout: skin

skin_name: "kiyomii"
forum_thread_id: 1334415
date_added: 2021-08-03
game_modes:
    - standard
author:
    - 15290015
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "kiyomii"
      screenshots:
          - "https://i.imgur.com/vA7Sx0R.jpg"
          - "https://i.imgur.com/X2lRgFL.png"
          - "https://i.imgur.com/iYka3Cw.png"
          - "https://i.imgur.com/0tAcOpr.jpg"
          - "https://i.imgur.com/cgh7SIe.png"
          - "https://i.imgur.com/6cS5u3W.png"
          - "https://i.imgur.com/JmuX93f.png"
          - "https://i.imgur.com/R9ZsPii.png"
          - "https://i.imgur.com/A0FoEMN.png"
          - "https://i.imgur.com/25F58zF.png"
          - "https://i.imgur.com/nbnbJXi.jpg"
videos:
    skinship:
    author:
---
