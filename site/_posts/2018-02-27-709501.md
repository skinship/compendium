---
layout: skin

skin_name: "KissShot/Shinobu"
forum_thread_id: 709501
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 10289337
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "monogatari"
    - "oshino_shinobu"
categories:
    - anime
skin_collection:
    - name: "KissShot/Shinobu"
      screenshots:
          - "https://i.imgur.com/0s3MQfA.jpg"
          - "https://i.imgur.com/xIB6ezJ.jpg"
          - "https://i.imgur.com/pdSJJR7.jpg"
videos:
    skinship:
    author:
---
