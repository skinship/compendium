---
layout: skin

skin_name: "Incognita's Purple"
forum_thread_id: 1237594
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 17918275
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Incognita's Purple"
      screenshots:
          - "http://i.imgur.com/YbRGHbR.png"
          - "http://i.imgur.com/x0pWWWA.png"
          - "http://i.imgur.com/qKq7tSU.png"
          - "http://i.imgur.com/4yGz8GD.png"
          - "http://i.imgur.com/Sl6voli.png"
          - "http://i.imgur.com/N2U3BxB.png"
          - "http://i.imgur.com/LGrbmTs.png"
          - "http://i.imgur.com/PxGk42X.png"
          - "http://i.imgur.com/0rhsqkO.png"
videos:
    skinship:
    author:
---
