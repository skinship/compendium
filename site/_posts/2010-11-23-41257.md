---
layout: skin

skin_name: "Alice Margatroid"
forum_thread_id: 41257
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 46620
resolutions:
ratios:
tags:
    - "alice_margatroid"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "Alice Margatroid"
      screenshots:
          - "http://i.imgur.com/t1JLhb1.jpg"
          - "http://i.imgur.com/O6CeUlo.jpg"
          - "http://i.imgur.com/L7eYC7i.png"
          - "http://i.imgur.com/VourrSZ.png"
          - "http://i.imgur.com/dIkIKun.jpg"
videos:
    skinship:
    author:
---
