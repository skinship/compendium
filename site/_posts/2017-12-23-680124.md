---
layout: skin

skin_name: "Clear Futuristic Miku"
forum_thread_id: 680124
date_added: 2021-06-28
game_modes:
    - standard
    - catch
author:
    - 8388854
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "16:10"
tags:
    - "hatsune_miku"
    - "vocaloid"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Clear Futuristic Miku"
      screenshots:
          - "http://i.imgur.com/sboC1zU.png"
          - "http://i.imgur.com/uzgDI1Z.png"
          - "http://i.imgur.com/sBWym4b.png"
          - "http://i.imgur.com/3F9QV8V.png"
          - "http://i.imgur.com/3Lr4pLH.png"
          - "http://i.imgur.com/Z7MAuI2.png"
videos:
    skinship:
    author:
---
