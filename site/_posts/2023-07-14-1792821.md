---
layout: skin

skin_name: "Bring it on!!"
forum_thread_id: 1792821
date_added: 2023-07-20
game_modes:
    - standard
    - catch
author:
    - 19135423
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "kagamine_rin"
    - "vocaloid"
    - "animated"
    - "beatmap"
    - "contest_4_submission"
    - "bring_it_on"
    - "kagamine_len"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Bring it on!!"
      screenshots:
          - "https://i.imgur.com/rCH41uA.jpg"
          - "https://i.imgur.com/9bWv0ld.jpg"
          - "https://i.imgur.com/zakDFHx.jpg"
          - "https://i.imgur.com/te2BLXn.jpg"
          - "https://i.imgur.com/tvAtEtJ.jpg"
          - "https://i.imgur.com/NaaZfJk.jpg"
          - "https://i.imgur.com/KlpMtTl.jpg"
          - "https://i.imgur.com/keDW9ZR.jpg"
videos:
    skinship:
        - ngakmFTu6kk
    author:
---
