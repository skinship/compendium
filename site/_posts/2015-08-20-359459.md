---
layout: skin

skin_name: "♥ loveHeart"
forum_thread_id: 359459
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6509768
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - anime
skin_collection:
    - name: "♥ loveHeart"
      screenshots:
          - "http://i.imgur.com/BpYcYsQ.png"
          - "http://i.imgur.com/yTK5AoI.png"
          - "http://i.imgur.com/2Z2KUoK.png"
          - "http://i.imgur.com/ALPHYYO.png"
          - "http://i.imgur.com/GJN56rr.png"
          - "http://i.imgur.com/l888xqM.png"
          - "http://i.imgur.com/le8pT8j.png"
videos:
    skinship:
    author:
---
