---
layout: skin

skin_name: "Prinz Heinrich"
forum_thread_id: 1292847
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 6766278
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "prinz_heinrich"
    - "azur_lane"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
categories:
    - anime
skin_collection:
    - name: "Prinz Heinrich"
      screenshots:
          - "https://i.imgur.com/ZLFx7oe.jpg"
          - "https://i.imgur.com/uggB8Eg.jpg"
          - "https://i.imgur.com/FR4mMi7.jpg"
          - "https://i.imgur.com/c35dPbm.jpg"
          - "https://i.imgur.com/ZktOG4b.jpg"
          - "https://i.imgur.com/CTlaDx0.jpg"
          - "https://i.imgur.com/7DSbkkD.jpg"
          - "https://i.imgur.com/igw3H1Q.jpg"
          - "https://i.imgur.com/TUwP825.jpg"
          - "https://i.imgur.com/CXhq6ip.jpg"
          - "https://i.imgur.com/Fwapzsl.jpg"
          - "https://i.imgur.com/scDaBY8.jpg"
          - "https://i.imgur.com/k2ntN2L.jpg"
          - "https://i.imgur.com/EqPVZ9y.jpg"
          - "https://i.imgur.com/1UpRosr.jpg"
          - "https://i.imgur.com/EoMhi5k.jpg"
          - "https://i.imgur.com/FlVNMql.jpg"
videos:
    skinship:
        - PHAGNcVBh2Q
    author:
---
