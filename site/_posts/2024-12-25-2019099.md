---
layout: skin

skin_name: "FREEDOM DiVE REiMAGINED"
forum_thread_id: 2019099
date_added: 2024-12-28
game_modes:
    - standard
author:
    - 11805037
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "freedom_dive"
categories:
    - eyecandy
skin_collection:
    - name: "FREEDOM DiVE REiMAGINED"
      screenshots:
          - "https://i.imgur.com/Nz0UHQD.jpg"
          - "https://i.imgur.com/El3PpQ7.jpg"
          - "https://i.imgur.com/jXEm6TH.jpg"
          - "https://i.imgur.com/XekC31P.jpg"
          - "https://i.imgur.com/X4WZBs6.jpg"
          - "https://i.imgur.com/eU6E9Sb.jpg"
          - "https://i.imgur.com/Y888ezt.jpg"
          - "https://i.imgur.com/wNbIFuw.jpg"
          - "https://i.imgur.com/hVt1bLO.jpg"
          - "https://i.imgur.com/0xfvrj7.jpg"
          - "https://i.imgur.com/unwRq87.jpg"
          - "https://i.imgur.com/BdowZO0.jpg"
          - "https://i.imgur.com/biDHiqC.jpg"
          - "https://i.imgur.com/R6yOkAk.jpg"
videos:
    skinship:
    author:
        - 8Q0OOo4dLvI
---
