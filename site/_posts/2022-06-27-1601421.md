---
layout: skin

skin_name: "Menhera-kun"
forum_thread_id: 1601421
date_added: 2024-04-14
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 14085757
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Menhera-kun"
      screenshots:
          - "https://i.imgur.com/waW3bjC.jpg"
          - "https://i.imgur.com/b8JwFe1.jpg"
          - "https://i.imgur.com/tDgpMKR.jpg"
          - "https://i.imgur.com/JpPE0ba.jpg"
          - "https://i.imgur.com/fjf0V3v.jpg"
          - "https://i.imgur.com/78EwwSa.jpg"
          - "https://i.imgur.com/v1lGEKr.png"
          - "https://i.imgur.com/C3XBoMx.png"
          - "https://i.imgur.com/2yhBXDp.jpg"
          - "https://i.imgur.com/NMgNg95.jpg"
          - "https://i.imgur.com/QbHA0S4.jpg"
          - "https://i.imgur.com/gsFWx01.jpg"
          - "https://i.imgur.com/EDFKgsb.jpg"
          - "https://i.imgur.com/voKRueV.jpg"
          - "https://i.imgur.com/KCmRAoS.jpg"
videos:
    skinship:
    author:
        - wEspnMP9ydg
---
