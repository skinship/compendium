---
layout: skin

skin_name: "Spicy's VERDE"
forum_thread_id: 744250
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 11368176
resolutions:
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Spicy's VERDE"
      screenshots:
          - "https://imgur.com/TiDC6Cr.png"
          - "https://imgur.com/yBxhcqp.png"
          - "https://imgur.com/RGvuk3U.png"
          - "https://imgur.com/h2O6Wkg.png"
          - "https://imgur.com/iI4FGNs.png"
          - "https://imgur.com/UHDlY92.png"
          - "https://imgur.com/S953yr2.png"
          - "https://imgur.com/IXBFpMj.png"
          - "https://imgur.com/CahDqhr.png"
          - "https://imgur.com/B5zp4Gk.png"
          - "https://imgur.com/T6tnT8X.png"
          - "https://imgur.com/dM2OcTh.png"
          - "https://imgur.com/wiBBrbF.png"
          - "https://imgur.com/ljITouK.png"
videos:
    skinship:
    author:
---
