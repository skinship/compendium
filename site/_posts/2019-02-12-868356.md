---
layout: skin

skin_name: "Clinae"
forum_thread_id: 868356
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6385568
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "kochiya_sanae"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "Clinae"
      screenshots:
          - "http://i.imgur.com/n1fh6Fl.png"
          - "http://i.imgur.com/cLHpUFT.png"
          - "http://i.imgur.com/wAUoq8J.png"
          - "http://i.imgur.com/kHpYYlx.png"
          - "http://i.imgur.com/xaBLAW3.png"
          - "http://i.imgur.com/fjUWS0l.png"
          - "http://i.imgur.com/22HxUBz.png"
          - "http://i.imgur.com/mLiaM3H.png"
          - "http://i.imgur.com/eB6SQuX.png"
videos:
    skinship:
    author:
---
