---
layout: skin

skin_name: "Kaoruko Moeta"
forum_thread_id: 904391
date_added: 2021-07-18
game_modes:
    - standard
    - catch
author:
    - 7087699
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "comic_girls"
    - "kaoruko_moeta"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Kaoruko Moeta"
      screenshots:
          - "https://i.imgur.com/cq1hspD.jpg"
          - "https://i.imgur.com/kyghrcl.jpg"
          - "https://i.imgur.com/bMm9wH9.jpg"
          - "https://i.imgur.com/tR64yVY.jpg"
          - "https://i.imgur.com/KdhAvSs.jpg"
          - "https://i.imgur.com/ACipFi2.jpg"
          - "https://i.imgur.com/M0pFMSq.jpg"
          - "https://i.imgur.com/sEgS3b3.jpg"
          - "https://i.imgur.com/JtOtKF7.jpg"
          - "https://i.imgur.com/qUbtupJ.jpg"
          - "https://i.imgur.com/dV683Wk.jpg"
          - "https://i.imgur.com/gm5DrXX.jpg"
          - "https://i.imgur.com/59QTJe1.jpg"
          - "https://i.imgur.com/hBdM0tp.jpg"
          - "https://i.imgur.com/EMVwT7Q.jpg"
          - "https://i.imgur.com/sLgt6ZA.jpg"
          - "https://i.imgur.com/6wQfbJG.jpg"
          - "https://i.imgur.com/JXZ76zS.jpg"
videos:
    skinship:
    author:
---
