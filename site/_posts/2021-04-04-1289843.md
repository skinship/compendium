---
layout: skin

skin_name: "Starcruiser"
forum_thread_id: 1289843
date_added: 2021-07-15
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 2615199
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
categories:
    - other
skin_collection:
    - name: "Starcruiser"
      screenshots:
          - "https://i.imgur.com/SUkDoOq.jpeg"
          - "https://i.imgur.com/yMqvbor.jpeg"
          - "https://i.imgur.com/ZwBfR3x.jpeg"
          - "https://i.imgur.com/jrljPqp.jpeg"
          - "https://i.imgur.com/3bWQQxf.jpeg"
          - "https://i.imgur.com/9LlUUmK.jpeg"
          - "https://i.imgur.com/xOQgULu.jpeg"
          - "https://i.imgur.com/zsDJq6i.jpeg"
          - "https://i.imgur.com/rJ2c3Jp.jpeg"
          - "https://i.imgur.com/A5HoYAf.jpeg"
          - "https://i.imgur.com/h0maFFL.jpeg"
          - "https://i.imgur.com/bsxGOhy.jpeg"
          - "https://i.imgur.com/6RiGvSU.jpeg"
          - "https://i.imgur.com/VbYpooR.jpeg"
          - "https://i.imgur.com/MvUo6S1.jpeg"
          - "https://i.imgur.com/VHoXO9g.jpeg"
videos:
    skinship:
        - FCaV7C4Fb54
    author:
---
