---
layout: skin

skin_name: "Numi 悪魔ニムネ"
forum_thread_id: 1911115
date_added: 2024-04-18
game_modes:
    - standard
    - mania
    - catch
author:
    - 17561095
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "vtuber"
    - "4k"
    - "7k"
    - "akuma_nihmune"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Numi 悪魔ニムネ"
      screenshots:
          - "https://i.imgur.com/M9AsUbT.png"
          - "https://i.imgur.com/B0FaSGs.jpg"
          - "https://i.imgur.com/jy7DZXh.jpg"
          - "https://i.imgur.com/L9GZmAO.jpg"
          - "https://i.imgur.com/ipCatEJ.jpg"
          - "https://i.imgur.com/fiY3zrV.jpg"
          - "https://i.imgur.com/EUXcslc.jpg"
          - "https://i.imgur.com/8XS3c4f.jpg"
          - "https://i.imgur.com/ZqBcwkM.jpg"
          - "https://i.imgur.com/YBlQXla.jpg"
          - "https://i.imgur.com/s5b2yCg.jpg"
          - "https://i.imgur.com/bT91ptU.jpg"
          - "https://i.imgur.com/68YfHMd.jpg"
          - "https://i.imgur.com/qahh4Wu.jpg"
          - "https://i.imgur.com/YczPryY.jpg"
          - "https://i.imgur.com/4s9csMu.jpg"
videos:
    skinship:
    author:
        - Uvms5889AJo
---
