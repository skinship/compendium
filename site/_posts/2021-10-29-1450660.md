---
layout: skin

skin_name: "Turnip Boy Commits Tax Evasion"
forum_thread_id: 1450660
date_added: 2021-12-08
game_modes:
    - standard
author:
    - 4903197
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "turnip_boy_commits_tax_evasion"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "Turnip Boy Commits Tax Evasion"
      screenshots:
          - "https://i.imgur.com/I860lpW.png"
          - "https://i.imgur.com/DxkLZIo.jpeg"
          - "https://i.imgur.com/lSm0YGw.png"
          - "https://i.imgur.com/4mw13lt.png"
          - "https://i.imgur.com/wUZe5Ct.png"
          - "https://i.imgur.com/keEITeh.png"
          - "https://i.imgur.com/aqGeFAY.png"
videos:
    skinship:
    author:
---
