---
layout: skin

skin_name: "RWBY 2 Black"
forum_thread_id: 351477
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2129634
resolutions:
    - hd
    - sd
ratios:
tags:
    - "rwby"
categories:
    - anime
    - game
skin_collection:
    - name: "RWBY 2 Black"
      screenshots:
          - "http://i.imgur.com/SERtBqc.jpg"
          - "http://i.imgur.com/kzW9fBr.jpg"
          - "http://i.imgur.com/vRm7EQ5.jpg"
          - "http://i.imgur.com/jSGis3J.jpg"
          - "http://i.imgur.com/W3ibmi9.jpg"
          - "http://i.imgur.com/EopfkjM.jpg"
          - "http://i.imgur.com/GCp6To7.jpg"
          - "http://i.imgur.com/CCqa1sa.jpg"
          - "http://i.imgur.com/8xHbOOs.jpg"
          - "http://i.imgur.com/dVJZz2K.jpg"
          - "http://i.imgur.com/Ql26UeO.jpg"
videos:
    skinship:
    author:
---
