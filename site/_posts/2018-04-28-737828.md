---
layout: skin

skin_name: "Summer"
forum_thread_id: 737828
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 9903296
resolutions:
    - sd
ratios:
    - "16:9"
tags:
categories:
    - eyecandy
skin_collection:
    - name: "Summer"
      screenshots:
          - "https://i.imgur.com/iwPmQoB.jpg"
          - "https://i.imgur.com/iSvn3a3.jpg"
          - "https://i.imgur.com/SftiWgn.jpg"
          - "https://i.imgur.com/eRyWUT2.jpg"
          - "https://i.imgur.com/t4nBAa4.jpg"
          - "https://i.imgur.com/iJFSEn6.jpg"
          - "https://i.imgur.com/MF81UwZ.jpg"
videos:
    skinship:
    author:
---
