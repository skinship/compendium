---
layout: skin

skin_name: "new_Era"
forum_thread_id: 1941741
date_added: 2025-01-25
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 2168518
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "2k"
    - "3k"
    - "4k"
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "v1 - Blue Top Bar"
      screenshots:
          - "https://i.imgur.com/pBe3fqT.png"
          - "https://i.imgur.com/xpknzU5.png"
          - "https://i.imgur.com/eXUxllq.png"
          - "https://i.imgur.com/KryMKRA.png"
          - "https://i.imgur.com/qHVpFCq.png"
          - "https://i.imgur.com/aFBm43U.png"
          - "https://i.imgur.com/mObWsHi.png"
          - "https://i.imgur.com/uMEzzR2.png"
          - "https://i.imgur.com/ncMPD91.png"
          - "https://i.imgur.com/P5pwjT2.png"
          - "https://i.imgur.com/bvaeWJv.png"
          - "https://i.imgur.com/qemq7sl.png"
          - "https://i.imgur.com/3U5ckZ6.png"
          - "https://i.imgur.com/5Sz9VZ9.png"
          - "https://i.imgur.com/X0qwZE6.png"
    - name: "v1.1 - White Top Bar"
      screenshots:
          - "https://i.imgur.com/Doha5Eb.png"
          - "https://i.imgur.com/L1du2kt.png"
          - "https://i.imgur.com/4ZDQJD5.png"
          - "https://i.imgur.com/giEstCM.png"
          - "https://i.imgur.com/Y0UgQtW.png"
          - "https://i.imgur.com/Zj9Nibw.png"
          - "https://i.imgur.com/jcrDHlD.png"
          - "https://i.imgur.com/adpoLWM.png"
          - "https://i.imgur.com/qU3pFOJ.png"
          - "https://i.imgur.com/dKRYO4b.png"
          - "https://i.imgur.com/hrIV6fs.png"
          - "https://i.imgur.com/Q5SgYiX.png"
          - "https://i.imgur.com/QXBEfRS.png"
          - "https://i.imgur.com/yV1Dtub.png"
          - "https://i.imgur.com/m5Xgaxp.png"
videos:
    skinship:
    author:
---
