---
layout: skin

skin_name: "io15"
forum_thread_id: 1688412
date_added: 2023-01-05
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 8196177
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "skinners'_bulletin_showcase"
    - "vtuber"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
    - "airani_iofifteen"
categories:
    - anime
    - minimalistic
    - eyecandy
skin_collection:
    - name: "io15"
      screenshots:
          - "https://i.imgur.com/gO5OwUq.jpg"
          - "https://i.imgur.com/ud4igJt.jpg"
          - "https://i.imgur.com/gUjHSNB.jpg"
          - "https://i.imgur.com/ONmsVyj.jpg"
          - "https://i.imgur.com/dEww1qO.jpg"
          - "https://i.imgur.com/ES9AdD2.jpg"
          - "https://i.imgur.com/N3LCPew.jpg"
          - "https://i.imgur.com/eJiMmRP.jpg"
          - "https://i.imgur.com/vORZ0WB.jpg"
          - "https://i.imgur.com/eTuGyCd.jpg"
          - "https://i.imgur.com/CgKX1FU.jpg"
          - "https://i.imgur.com/o2dIfBu.jpg"
          - "https://i.imgur.com/u6OsU3a.jpg"
          - "https://i.imgur.com/u4HXfCz.jpg"
          - "https://i.imgur.com/e6K6eiv.jpg"
          - "https://i.imgur.com/ksxLWED.jpg"
          - "https://i.imgur.com/kZEpihB.jpg"
          - "https://i.imgur.com/Qeu9w3S.jpg"
          - "https://i.imgur.com/DUP1H4g.jpg"
          - "https://i.imgur.com/C7LhEx4.jpg"
videos:
    skinship:
        - OB2-it8cbG0
    author:
---
