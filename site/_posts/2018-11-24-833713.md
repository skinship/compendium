---
layout: skin

skin_name: "Sky Delta"
forum_thread_id: 833713
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 5143445
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
tags:
categories:
    - other
skin_collection:
    - name: "Sky Delta"
      screenshots:
          - "https://i.imgur.com/N2iRPbf.jpg"
          - "https://i.imgur.com/BBZFMSC.jpg"
          - "https://i.imgur.com/uAlQcnD.jpg"
          - "https://i.imgur.com/8Oe85e6.jpg"
          - "https://i.imgur.com/EkiysYH.jpg"
          - "https://i.imgur.com/bC1K2Pz.jpg"
          - "https://i.imgur.com/FrlOBx0.jpg"
          - "https://i.imgur.com/5jKSswD.jpg"
          - "https://i.imgur.com/5Q8neZI.jpg"
          - "https://i.imgur.com/diaehXT.jpg"
videos:
    skinship:
    author:
---
