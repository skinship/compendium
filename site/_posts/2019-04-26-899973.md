---
layout: skin

skin_name: "Gabe the Dog"
forum_thread_id: 899973
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2593280
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
tags:
    - "gabe_the_dog"
categories:
    - other
skin_collection:
    - name: "Gabe the Dog"
      screenshots:
          - "https://i.imgur.com/PEGgm1R.jpg"
          - "https://i.imgur.com/iUjLujB.jpg"
          - "https://i.imgur.com/BHCANT1.jpg"
          - "https://i.imgur.com/BCVy5lL.jpg"
          - "https://i.imgur.com/wWc243s.jpg"
          - "https://i.imgur.com/IsKOsTG.jpg"
          - "https://i.imgur.com/iIW8QcA.jpg"
          - "https://i.imgur.com/YgWfNxy.jpg"
          - "https://i.imgur.com/eLO9UuZ.jpg"
          - "https://i.imgur.com/IthM8I9.jpg"
videos:
    skinship:
    author:
---
