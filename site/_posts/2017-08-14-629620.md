---
layout: skin

skin_name: "Nishikino Maki"
forum_thread_id: 629620
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2636217
resolutions:
    - hd
    - sd
ratios:
tags:
    - "nishikino_maki"
    - "bibi"
    - "love_live"
categories:
    - anime
skin_collection:
    - name: "Nishikino Maki"
      screenshots:
          - "https://i.imgur.com/4FGd3pK.jpg"
          - "https://i.imgur.com/7liP9W1.jpg"
          - "https://i.imgur.com/Sy16dxK.jpg"
          - "https://i.imgur.com/kadK1qD.jpg"
          - "https://i.imgur.com/Jc8SziC.jpg"
          - "https://i.imgur.com/vnCltzL.jpg"
          - "https://i.imgur.com/1JsRlrY.jpg"
          - "https://i.imgur.com/4OazAvQ.jpg"
          - "https://i.imgur.com/CdJLBAQ.jpg"
          - "https://i.imgur.com/lVCPf2G.jpg"
          - "https://i.imgur.com/Xquo3AU.jpg"
          - "https://i.imgur.com/y35BAmd.jpg"
videos:
    skinship:
    author:
---
