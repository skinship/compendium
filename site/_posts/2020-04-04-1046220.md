---
layout: skin

skin_name: "Matoi Ryuuko"
forum_thread_id: 1046220
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 12837388
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
tags:
    - "matoi_ryuuko"
    - "kill_la_kill"
categories:
    - anime
skin_collection:
    - name: "Matoi Ryuuko"
      screenshots:
          - "https://i.imgur.com/TIMIspU.png"
          - "https://i.imgur.com/jn5NGCm.pngi.imgur.com/c0h8NHq.png"
          - "https://i.imgur.com/HQQBKIQ.png"
          - "https://i.imgur.com/m0Mz0wl.png"
          - "https://i.imgur.com/RF0Msue.png"
          - "https://i.imgur.com/c0h8NHq.png"
          - "https://i.imgur.com/MPhBfNN.png"
          - "https://i.imgur.com/h6k7KaN.png"
videos:
    skinship:
    author:
---
