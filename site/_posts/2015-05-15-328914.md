---
layout: skin

skin_name: "Simple"
forum_thread_id: 328914
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 1125647
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Simple"
      screenshots:
          - "http://i.imgur.com/TVwNbkI.jpg"
          - "http://i.imgur.com/JLYdKQo.png"
          - "http://i.imgur.com/Utw5X5P.png"
          - "http://i.imgur.com/uHEhIks.jpg"
          - "http://i.imgur.com/L0Fdd7f.png"
          - "http://i.imgur.com/0cbpg7O.png"
videos:
    skinship:
    author:
---
