---
layout: skin

skin_name: "Pure"
forum_thread_id: 254854
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 1884564
resolutions:
    - hd
ratios:
tags:
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "Pure"
      screenshots:
          - "http://i.imgur.com/aMj41AT.jpg"
          - "http://i.imgur.com/DoK04Ph.jpg"
          - "http://i.imgur.com/sAUzosf.png"
          - "http://i.imgur.com/1yTHcbc.png"
          - "http://i.imgur.com/kRrc6qQ.png"
videos:
    skinship:
    author:
---
