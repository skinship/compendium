---
layout: skin

skin_name: "Brawl Stars"
forum_thread_id: 2011080
date_added: 2024-12-04
game_modes:
    - standard
    - taiko
    - catch
author:
    - 3802071
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "21:9"
tags:
    - "brawl_stars"
categories:
    - game
skin_collection:
    - name: "Brawl Stars"
      screenshots:
          - "https://i.imgur.com/Anr3FLV.png"
          - "https://i.imgur.com/LjbP7sW.png"
          - "https://i.imgur.com/pzAh3Vo.png"
          - "https://i.imgur.com/ydzAj9T.png"
          - "https://i.imgur.com/HIlQX8z.png"
          - "https://i.imgur.com/g6IEgWb.png"
          - "https://i.imgur.com/nxV1ecb.png"
          - "https://i.imgur.com/tHgGsZJ.png"
          - "https://i.imgur.com/Rgj7E6E.png"
          - "https://i.imgur.com/qEZnvMQ.png"
          - "https://i.imgur.com/wlJews4.png"
          - "https://i.imgur.com/s1k1lmb.png"
          - "https://i.imgur.com/cMmMXWM.png"
          - "https://i.imgur.com/67Be35x.png"
videos:
    skinship:
    author:
---
