---
layout: skin

skin_name: "Murasaki Purple"
forum_thread_id: 1923479
date_added: 2024-06-20
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 502239
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "techno"
    - "contest_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
    - "contest_5_submission"
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "Murasaki Purple"
      screenshots:
          - "https://i.imgur.com/iZhfwjV.png"
          - "https://i.imgur.com/UFRkBJE.png"
          - "https://i.imgur.com/6DKSBt1.png"
          - "https://i.imgur.com/V6syNZX.png"
          - "https://i.imgur.com/bMTnmDj.png"
          - "https://i.imgur.com/CT8O46z.png"
          - "https://i.imgur.com/LOpX4Sj.png"
          - "https://i.imgur.com/sgkm9qq.png"
          - "https://i.imgur.com/TcGmNff.png"
          - "https://i.imgur.com/hGqGPXF.png"
          - "https://i.imgur.com/wx26xhJ.png"
          - "https://i.imgur.com/9XySo5b.png"
          - "https://i.imgur.com/TQIOttc.png"
          - "https://i.imgur.com/VCqp2Jk.png"
          - "https://i.imgur.com/fCjn34Z.png"
          - "https://i.imgur.com/CdWdlpZ.png"
          - "https://i.imgur.com/C15X64e.png"
          - "https://i.imgur.com/wRzzUpV.png"
          - "https://i.imgur.com/3uVc6dZ.png"
          - "https://i.imgur.com/XwKj7He.png"
          - "https://i.imgur.com/bKnXN6D.png"
          - "https://i.imgur.com/gaU2sFw.png"
          - "https://i.imgur.com/wqOIonI.png"
videos:
    skinship:
        - GGsKKzOHXiA
    author:
---
