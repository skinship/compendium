---
layout: skin

skin_name: "Sunset Material & Midnight Material"
forum_thread_id: 1334702
date_added: 2021-08-03
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 16274977
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
categories:
    - minimalistic
skin_collection:
    - name: "Sunset Material"
      screenshots:
          - "https://cdn.discordapp.com/attachments/824941164020498472/847665998915829770/mainMenu.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847665996785647646/songSelect.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666044729294878/ranking.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666106512441354/pause.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666106697383957/fail.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666041563906068/circleClicking.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666041697599548/spinner.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666076095741972/taiko.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666076238086144/taikoKiai.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666076551872522/taikoFinishers.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666135985553469/catch.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666136086741002/catchBananas.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666177609302026/maniaLongNotes.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847666177621753886/mania.png"
    - name: "Midnight Material"
      screenshots:
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672594077450290/mainMenu.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672593624334356/songSelect.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672597231697980/rankingPanel.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672683210735666/pause.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672683458330654/fail.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672661437317170/circleClicking.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672661462876220/spinner.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672702847287326/taiko.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672702688165897/taikoKiai.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672702847549480/taikoFinishers.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672721843683358/catch.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672722120507422/catchBananas.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672745487761418/mania.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/847672750478852096/maniaLongNotes.png"
videos:
    skinship:
    author:
---
