---
layout: skin

skin_name: "Azerty's Skin"
forum_thread_id: 436105
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6395282
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Azerty's Skin"
      screenshots:
          - "http://i.imgur.com/9srB85n.jpg"
          - "http://i.imgur.com/8rFWqZl.png"
          - "http://i.imgur.com/kOsSBxj.png"
          - "http://i.imgur.com/wvRTJvl.png"
          - "http://i.imgur.com/1ILQeaM.png"
          - "http://i.imgur.com/b2vcbYF.png"
          - "http://i.imgur.com/9sD94tY.png"
          - "http://i.imgur.com/QTERyxb.png"
          - "http://i.imgur.com/4v3F7oZ.png"
videos:
    skinship:
    author:
---
