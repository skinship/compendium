---
layout: skin

skin_name: "Hello, I'm Rafis!"
forum_thread_id: 1440712
date_added: 2021-12-08
game_modes:
    - standard
author:
    - 12329221
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "rafis"
categories:
    - minimalistic
skin_collection:
    - name: "Hello, I'm Rafis!"
      screenshots:
          - "http://i.imgur.com/IiNNpsB.jpg"
          - "http://i.imgur.com/uXBlrrw.png"
          - "http://i.imgur.com/KLH8l1h.jpg"
          - "http://i.imgur.com/FXYocee.jpg"
          - "http://i.imgur.com/vfTBsZU.jpg"
          - "http://i.imgur.com/5xreasW.jpg"
          - "http://i.imgur.com/pmE4CXc.jpg"
          - "http://i.imgur.com/TevklhL.jpg"
          - "http://i.imgur.com/71TkSpi.jpg"
          - "http://i.imgur.com/6lpNOAh.jpg"
          - "http://i.imgur.com/W7NOq6m.jpg"
          - "http://i.imgur.com/UjxolQM.jpg"
          - "http://i.imgur.com/R8HhRpn.jpg"
          - "http://i.imgur.com/nW2tBDB.jpg"
          - "http://i.imgur.com/CCeC6nQ.jpg"
          - "http://i.imgur.com/KmwSq2N.jpg"
          - "http://i.imgur.com/quM5z62.jpg"
          - "http://i.imgur.com/3W7Ii6T.jpg"
          - "http://i.imgur.com/0rAXVlZ.jpg"
          - "http://i.imgur.com/EVh4x5U.jpg"
          - "http://i.imgur.com/AYYAvX0.jpg"
          - "http://i.imgur.com/XvzMjdy.jpg"
          - "http://i.imgur.com/iIXFwrs.jpg"
          - "http://i.imgur.com/yHKId59.jpg"
videos:
    skinship:
    author:
---
