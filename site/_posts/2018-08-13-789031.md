---
layout: skin

skin_name: "Megumin"
forum_thread_id: 789031
date_added: 2021-07-18
game_modes:
    - standard
author:
    - 5946435
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "konosuba"
    - "god's_blessing_on_this_wonderful_world!"
    - "kono_subarashii_sekai_ni_shukufuku_wo!"
    - "megumin"
categories:
    - anime
skin_collection:
    - name: "Megumin"
      screenshots:
          - "https://i.imgur.com/bMeMLJd.jpg"
          - "https://i.imgur.com/QRjfxAL.jpg"
          - "https://i.imgur.com/IFR3RPI.jpg"
          - "https://i.imgur.com/3c8u71Z.png"
          - "https://i.imgur.com/chEcHc1.jpg"
          - "https://i.imgur.com/PHj1vli.jpg"
          - "https://i.imgur.com/2DibAru.png"
          - "https://i.imgur.com/fx3vtyr.jpg"
videos:
    skinship:
    author:
---
