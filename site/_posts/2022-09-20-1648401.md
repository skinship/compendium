---
layout: skin

skin_name: "ĀkalaMania!"
forum_thread_id: 1648401
date_added: 2023-02-07
game_modes:
    - mania
author:
    - 26967931
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "ĀkalaMania!"
      screenshots:
          - "https://i.imgur.com/40GfrzX.jpg"
          - "https://i.imgur.com/ju91UMn.jpg"
          - "https://i.imgur.com/UGvHbl4.jpg"
          - "https://i.imgur.com/LKTjXV8.jpg"
          - "https://i.imgur.com/nI8SbNx.jpg"
          - "https://i.imgur.com/4JcihKS.jpg"
          - "https://i.imgur.com/E1yL1GZ.jpg"
          - "https://i.imgur.com/JO8bhrX.jpg"
          - "https://i.imgur.com/JWIhHvw.jpg"
          - "https://i.imgur.com/CEeTVVG.jpg"
          - "https://i.imgur.com/enfpDou.jpg"
videos:
    skinship:
    author:
---
