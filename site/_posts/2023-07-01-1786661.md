---
layout: skin

skin_name: "edanoma"
forum_thread_id: 1786661
date_added: 2023-12-23
game_modes:
    - standard
author:
    - 20695112
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "edanoma"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "edanoma"
      screenshots:
          - "https://i.imgur.com/r7Phx3i.jpg"
          - "https://i.imgur.com/GyzNLC1.png"
          - "https://i.imgur.com/VC08hwO.jpg"
          - "https://i.imgur.com/n2r9LM5.jpg"
          - "https://i.imgur.com/9wxcgYF.jpg"
          - "https://i.imgur.com/4pzkqX3.png"
          - "https://i.imgur.com/gfUTP0f.jpg"
          - "https://i.imgur.com/0mPlHS6.jpg"
          - "https://i.imgur.com/2rOiG1x.jpg"
          - "https://i.imgur.com/Yj3Pe1W.jpg"
          - "https://i.imgur.com/axmcFHl.jpg"
          - "https://i.imgur.com/9O18pM9.jpg"
          - "https://i.imgur.com/4mbYCRa.jpg"
          - "https://i.imgur.com/33SPrkf.jpg"
          - "https://i.imgur.com/zuLcRoN.png"
          - "https://i.imgur.com/AMt3hA1.png"
    - name: "Gameplay: Blue"
      screenshots:
          - "https://i.imgur.com/9O18pM9.jpeg"
          - "https://i.imgur.com/4mbYCRa.jpeg"
    - name: "Gameplay: Pink"
      screenshots:
          - "https://i.imgur.com/axmcFHl.jpeg"
          - "https://i.imgur.com/Yj3Pe1W.jpeg"
videos:
    skinship:
    author:
---
