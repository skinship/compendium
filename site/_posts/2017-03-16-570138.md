---
layout: skin

skin_name: "CHROMA"
forum_thread_id: 570138
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
author:
    - 6673060
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "CHROMA"
      screenshots:
          - "http://i.imgur.com/T3tsEd1.png"
          - "http://i.imgur.com/IgOttE7.png"
          - "http://i.imgur.com/GbI8K1v.png"
          - "http://i.imgur.com/TPEdiwS.png"
          - "http://i.imgur.com/tFpVzmZ.png"
          - "http://i.imgur.com/ixMqrgD.png"
          - "http://i.imgur.com/e5jlGc3.png"
          - "http://i.imgur.com/dH8a0Nu.png"
          - "http://i.imgur.com/5we5vST.png"
          - "http://i.imgur.com/koPZqhz.png"
          - "http://i.imgur.com/nYdsTlt.png"
          - "http://i.imgur.com/TxpKpyU.png"
          - "http://i.imgur.com/Z9IO3lQ.png"
          - "http://i.imgur.com/Z1uiXCQ.png"
          - "http://i.imgur.com/sIwLYQZ.png"
          - "http://i.imgur.com/3Tny3ts.png"
          - "http://i.imgur.com/OBEzppJ.png"
          - "http://i.imgur.com/piCCkii.png"
videos:
    skinship:
    author:
---
