---
layout: skin

skin_name: "Slanted Plains"
forum_thread_id: 1738710
date_added: 2023-12-23
game_modes:
    - standard
author:
    - 22283039
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Slanted Plains"
      screenshots:
          - "https://i.imgur.com/aaJiCdV.png"
          - "https://i.imgur.com/tW9xzWa.png"
          - "https://i.imgur.com/GN98AX9.png"
          - "https://i.imgur.com/2IDxZab.png"
          - "https://i.imgur.com/G2aIylk.png"
          - "https://i.imgur.com/ts5t1yp.png"
          - "https://i.imgur.com/4ACdXq2.png"
          - "https://i.imgur.com/eiV8gTv.png"
          - "https://i.imgur.com/Dg4vBYf.png"
videos:
    skinship:
    author:
---
