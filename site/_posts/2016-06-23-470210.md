---
layout: skin

skin_name: "Kantai Collection"
forum_thread_id: 470210
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 8493617
resolutions:
ratios:
tags:
    - "kantai_collection"
    - "shimakaze"
categories:
    - anime
skin_collection:
    - name: "Kantai Collection"
      screenshots:
          - "http://i.imgur.com/VRr0j6i.png"
          - "http://i.imgur.com/SQIxMLd.jpg"
          - "http://i.imgur.com/TtFkGFq.jpg"
          - "http://i.imgur.com/C83MfKI.png"
          - "http://i.imgur.com/Dz7JmOH.png"
          - "http://i.imgur.com/I6cfyBV.png"
videos:
    skinship:
    author:
---
