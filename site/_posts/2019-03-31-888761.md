---
layout: skin

skin_name: "Velocity"
forum_thread_id: 888761
date_added: 2021-06-24
game_modes:
    - standard
    - mania
author:
    - 11594132
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Velocity"
      screenshots:
          - "http://i.imgur.com/lAZ4ELG.png"
          - "http://i.imgur.com/ofhEzwh.png"
          - "http://i.imgur.com/iRYL9Fy.png"
          - "http://i.imgur.com/DjqbsWk.png"
          - "http://i.imgur.com/nFRqdrs.png"
          - "http://i.imgur.com/apckYHG.png"
          - "http://i.imgur.com/dLCK54H.png"
videos:
    skinship:
    author:
---
