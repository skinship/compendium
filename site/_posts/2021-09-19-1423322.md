---
layout: skin

skin_name: "KuroAoi"
forum_thread_id: 1423322
date_added: 2021-10-11
game_modes:
    - standard
    - mania
author:
    - 15985798
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "4k"
    - "7k"
categories:
    - minimalistic
skin_collection:
    - name: "KuroAoi"
      screenshots:
          - "https://i.imgur.com/TsK4NiQ.png"
          - "https://i.imgur.com/hYjXOPd.png"
          - "https://i.imgur.com/w2iTVTL.png"
          - "https://i.imgur.com/uN9fyBB.png"
          - "https://i.imgur.com/x6HuE57.png"
          - "https://i.imgur.com/C7iDQtM.png"
          - "https://i.imgur.com/4b4h5BX.png"
          - "https://i.imgur.com/FG2tu77.png"
          - "https://i.imgur.com/Q31jmWr.png"
          - "https://i.imgur.com/ibuA3lq.png"
videos:
    skinship:
    author:
---
