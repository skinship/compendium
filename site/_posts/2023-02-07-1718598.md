---
layout: skin

skin_name: "Kori's Pick"
forum_thread_id: 1718598
date_added: 2023-12-23
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 12490530
resolutions:
    - hd
    - sd
ratios:
    - "all"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - minimalistic
skin_collection:
    - name: "Kori's Pick"
      screenshots:
          - "https://i.imgur.com/3M7EaHQ.png"
          - "https://i.imgur.com/rnvUl7S.png"
          - "https://i.imgur.com/OVbwNRK.png"
          - "https://i.imgur.com/2lXGROE.png"
          - "https://i.imgur.com/DIOIs7L.png"
          - "https://i.imgur.com/Pt0SdCY.png"
          - "https://i.imgur.com/cQPs2Nh.png"
          - "https://i.imgur.com/JKfbvLt.png"
          - "https://i.imgur.com/RhWTPB3.png"
          - "https://i.imgur.com/cANCiaL.png"
          - "https://i.imgur.com/0IutK74.png"
          - "https://i.imgur.com/cb3pgin.png"
          - "https://i.imgur.com/T2tOHPU.png"
          - "https://i.imgur.com/Q0V05nb.png"
          - "https://i.imgur.com/UQBfyVw.png"
videos:
    skinship:
    author:
---
