---
layout: skin

skin_name: "VA-11 Hall-A"
forum_thread_id: 727986
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 7298776
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "21:9"
tags:
    - "va-11_hall-a"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "VA-11 Hall-A"
      screenshots:
          - "https://i.imgur.com/zFTHHGg.png"
          - "https://i.imgur.com/6OLKJ59.png"
          - "https://i.imgur.com/Q8vYHtl.png"
          - "https://i.imgur.com/tHFhpn6.png"
          - "https://i.imgur.com/EtRVLfv.png"
          - "https://i.imgur.com/ToeQXWG.png"
          - "https://i.imgur.com/g8l9oYg.png"
          - "https://i.imgur.com/yrdIcVv.png"
videos:
    skinship:
    author:
---
