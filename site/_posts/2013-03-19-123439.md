---
layout: skin

skin_name: "Baka Accurate"
forum_thread_id: 123439
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2138427
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Baka Accurate"
      screenshots:
          - "http://i.imgur.com/OIz9bEb.jpg"
          - "http://i.imgur.com/JUyvwCg.jpg"
          - "http://i.imgur.com/rjGamUq.png"
          - "http://i.imgur.com/v1NBDeh.png"
          - "http://i.imgur.com/0wPHJpD.png"
videos:
    skinship:
    author:
---
