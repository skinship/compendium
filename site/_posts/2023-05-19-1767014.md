---
layout: skin

skin_name: "PERSONA 5 ROYAL"
forum_thread_id: 1767014
date_added: 2023-05-21
game_modes:
    - standard
author:
    - 15194624
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "animated"
    - "persona_5_royal"
categories:
    - anime
    - eyecandy
    - game
skin_collection:
    - name: "PERSONA 5 ROYAL"
      screenshots:
          - "https://i.imgur.com/iTrqG0l.png"
          - "https://i.imgur.com/2khbZVE.png"
          - "https://i.imgur.com/yQnDpR4.png"
          - "https://i.imgur.com/fgQAmCm.png"
          - "https://i.imgur.com/OE4I1RS.png"
          - "https://i.imgur.com/lKrqvca.png"
          - "https://i.imgur.com/gvCDQvN.png"
          - "https://i.imgur.com/hNdTD2G.png"
          - "https://i.imgur.com/3yeWP8D.png"
          - "https://i.imgur.com/jAmWFoI.png"
          - "https://i.imgur.com/LYTLmvd.png"
          - "https://i.imgur.com/x5cnEfz.png"
          - "https://i.imgur.com/mZeT3jq.png"
          - "https://i.imgur.com/3rVKp5v.png"
          - "https://i.imgur.com/Gq7yZWO.png"
videos:
    skinship:
    author:
        - e1aCaEETST4
---
