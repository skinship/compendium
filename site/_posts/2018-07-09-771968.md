---
layout: skin

skin_name: "TransPride"
forum_thread_id: 771968
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 5679133
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - other
skin_collection:
    - name: "TransPride"
      screenshots:
          - "https://i.imgur.com/lk0wRqm.jpg"
          - "https://i.imgur.com/3LuqQj3.jpg"
          - "https://i.imgur.com/8CADGwU.png"
          - "https://i.imgur.com/iY2qAgy.jpg"
          - "https://i.imgur.com/sLJyjIN.png"
          - "https://i.imgur.com/EzOyfx6.png"
          - "https://i.imgur.com/ReLlJg4.png"
          - "https://i.imgur.com/SFVDZoz.png"
          - "https://i.imgur.com/AKw0sm3.jpg"
          - "https://i.imgur.com/PLkyYdx.jpg"
          - "https://i.imgur.com/jzantrh.jpg"
          - "https://i.imgur.com/SdzF1Pl.jpg"
videos:
    skinship:
    author:
---
