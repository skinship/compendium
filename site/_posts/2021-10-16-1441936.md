---
layout: skin

skin_name: "百鬼ネ DECADENCE"
forum_thread_id: 1441936
date_added: 2022-04-05
game_modes:
    - standard
author:
    - 3858685
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "skinners'_bulletin_showcase"
    - "vtuber"
    - "hololive_2nd_generation"
    - "nakiri_ayame"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "百鬼ネ DECADENCE"
      screenshots:
          - "https://i.imgur.com/H2Q7AXj.png"
          - "https://i.imgur.com/yRFZJ9X.png"
          - "https://i.imgur.com/PSl97c4.png"
          - "https://i.imgur.com/Xe0fSxb.png"
          - "https://i.imgur.com/F9b32JL.png"
          - "https://i.imgur.com/FHZ7Ur2.png"
          - "https://i.imgur.com/5UZPbnW.png"
          - "https://i.imgur.com/g1w78NM.png"
          - "https://i.imgur.com/fx4zIEL.png"
          - "https://i.imgur.com/uDSwuWV.png"
          - "https://i.imgur.com/h8BuWXW.png"
          - "https://i.imgur.com/pH7sihh.png"
          - "https://i.imgur.com/Ey7iuhl.png"
videos:
    skinship:
    author:
---
