---
layout: skin

skin_name: "YUGEN"
forum_thread_id: 365036
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 2130664
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "YUGEN"
      screenshots:
          - "http://i.imgur.com/85yQZBa.jpg"
          - "http://i.imgur.com/1C1SXsT.jpg"
          - "http://i.imgur.com/9SFpoED.png"
          - "http://i.imgur.com/mFaHwpI.png"
          - "http://i.imgur.com/r9dhAHe.png"
          - "http://i.imgur.com/K6iltgN.png"
          - "http://i.imgur.com/ItEPQ0O.png"
          - "http://i.imgur.com/FxtGT0R.png"
          - "http://i.imgur.com/TxtcZvZ.png"
videos:
    skinship:
    author:
---
