---
layout: skin

skin_name: "[アロナ] Arona"
forum_thread_id: 1655951
date_added: 2022-12-13
game_modes:
    - standard
author:
    - 25025133
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "blue_archive"
    - "arona"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "[アロナ] Arona"
      screenshots:
          - "https://i.imgur.com/vvMBaWN.png"
          - "https://i.imgur.com/dAnzvDP.png"
          - "https://i.imgur.com/aBKQL8Q.png"
          - "https://i.imgur.com/KAAWJiW.png"
          - "https://i.imgur.com/jciD7bi.png"
          - "https://i.imgur.com/M9xVifb.png"
          - "https://i.imgur.com/soC3S2w.png"
          - "https://i.imgur.com/yuUUbd7.png"
          - "https://i.imgur.com/BDHabZs.png"
videos:
    skinship:
    author:
---
