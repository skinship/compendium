---
layout: skin

skin_name: "Touhou Project Cirno"
forum_thread_id: 1368485
date_added: 2021-07-15
game_modes:
    - standard
author:
    - 15599657
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "cirno"
    - "touhou_project"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "Touhou Project Cirno"
      screenshots:
          - "https://i.imgur.com/EVxO1ae.png"
          - "https://i.imgur.com/yJFYaM4.png"
          - "https://i.imgur.com/1uUGkrM.png"
          - "https://i.imgur.com/9ZDhgGI.png"
          - "https://i.imgur.com/PdTybBN.png"
          - "https://i.imgur.com/ko6RclK.png"
          - "https://i.imgur.com/x7VFBQo.png"
videos:
    skinship:
    author:
---
