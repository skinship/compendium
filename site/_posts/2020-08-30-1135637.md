---
layout: skin

skin_name: "WaveDistrict"
forum_thread_id: 1135637
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 8388854
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Default"
      screenshots:
          - "http://i.imgur.com/SoS38BI.png"
          - "http://i.imgur.com/7DoLOrG.png"
          - "http://i.imgur.com/FroGzyM.png"
          - "http://i.imgur.com/93W64df.png"
          - "http://i.imgur.com/UByF0wd.png"
          - "http://i.imgur.com/OLimcb6.png"
          - "http://i.imgur.com/Il00ITe.png"
          - "http://i.imgur.com/DR1zrr5.png"
          - "http://i.imgur.com/Xidv0YG.png"
    - name: "Legacy"
      screenshots:
          - "http://i.imgur.com/N8V5t70.png"
          - "http://i.imgur.com/R5ulFkI.png"
          - "http://i.imgur.com/Rz7mFBk.png"
          - "http://i.imgur.com/4IWI2an.png"
          - "http://i.imgur.com/0HI2Gyl.png"
          - "http://i.imgur.com/AeGL3yc.png"
          - "http://i.imgur.com/Cm5sJtE.png"
          - "http://i.imgur.com/abKz9nE.png"
          - "http://i.imgur.com/9d3t7jt.png"
          - "http://i.imgur.com/n9aaLd2.png"
          - "http://i.imgur.com/DvvG5Sh.png"
    - name: "Midnight"
      screenshots:
          - "http://i.imgur.com/dCq6Ytu.png"
          - "http://i.imgur.com/3Br3P8b.png"
          - "http://i.imgur.com/7wf5all.png"
          - "http://i.imgur.com/3NFZgRD.png"
          - "http://i.imgur.com/gni6fiK.png"
          - "http://i.imgur.com/lf8P7W4.png"
          - "http://i.imgur.com/yOHlldt.png"
          - "http://i.imgur.com/T11xbfU.png"
    - name: "Powersave"
      screenshots:
          - "http://i.imgur.com/LRShyxc.png"
          - "http://i.imgur.com/InV7pyE.png"
          - "http://i.imgur.com/YRadphU.png"
          - "http://i.imgur.com/0yXdgrP.png"
          - "http://i.imgur.com/rkaW0eP.png"
          - "http://i.imgur.com/5m1ktTZ.png"
          - "http://i.imgur.com/KLJKBYK.png"
          - "http://i.imgur.com/iLTo6wT.png"
          - "http://i.imgur.com/3OzjIXo.png"
videos:
    skinship:
    author:
---
