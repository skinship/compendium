---
layout: skin

skin_name: "Snow"
forum_thread_id: 1494794
date_added: 2022-01-14
game_modes:
    - standard
    - mania
author:
    - 10557490
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_2_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "winter"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - other
skin_collection:
    - name: "Snow"
      screenshots:
          - "https://i.imgur.com/9LtF5TK.jpeg"
          - "https://i.imgur.com/kfeLfLd.jpeg"
          - "https://i.imgur.com/rMR2a0T.jpeg"
          - "https://i.imgur.com/itM2fIQ.jpeg"
          - "https://i.imgur.com/eRqxJp1.jpeg"
          - "https://i.imgur.com/NmWRJFe.jpeg"
          - "https://i.imgur.com/8w4NpDG.jpeg"
          - "https://i.imgur.com/xMpzI45.jpeg"
          - "https://i.imgur.com/e6omtar.jpeg"
          - "https://i.imgur.com/nVeNbAA.jpeg"
          - "https://i.imgur.com/SPMnVOr.jpeg"
          - "https://i.imgur.com/A2nBw8c.jpeg"
videos:
    skinship:
        - WiF4ep7AUBQ
    author:
---
