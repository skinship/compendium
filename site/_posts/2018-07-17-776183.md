---
layout: skin

skin_name: "mgsv"
forum_thread_id: 776183
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
author:
    - 9648728
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "mgsv"
      screenshots:
          - "http://i.imgur.com/J5ROIhp.png"
          - "http://i.imgur.com/0jDl9YM.png"
          - "http://i.imgur.com/U9BGEPt.png"
          - "http://i.imgur.com/CLF6hSs.png"
          - "http://i.imgur.com/DfsO6E2.png"
          - "http://i.imgur.com/2y2a317.png"
          - "http://i.imgur.com/vXXvyY2.png"
          - "http://i.imgur.com/alTb72L.png"
videos:
    skinship:
    author:
---
