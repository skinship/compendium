---
layout: skin

skin_name: "Decaplets"
forum_thread_id: 1775463
date_added: 2023-07-20
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 25025133
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "animated"
    - "beatmap"
    - "contest_4_submission"
    - "asteroid_field_of_decaplets"
categories:
    - minimalistic
skin_collection:
    - name: "Decaplets"
      screenshots:
          - "https://i.imgur.com/syXmM5h.png"
          - "https://i.imgur.com/aidzbci.png"
          - "https://i.imgur.com/5M008GJ.png"
          - "https://i.imgur.com/dwkNcua.png"
          - "https://i.imgur.com/7mDyeeQ.png"
          - "https://i.imgur.com/LX1Yh67.png"
          - "https://i.imgur.com/ziuKA9c.png"
          - "https://i.imgur.com/FVuctD9.png"
          - "https://i.imgur.com/AllQTOo.png"
          - "https://i.imgur.com/pZlN3j6.png"
videos:
    skinship:
        - _jEdu6wHgXo
    author:
---
