---
layout: skin

skin_name: "Beyond Journey's End (Frieren)"
forum_thread_id: 1962160
date_added: 2025-01-25
game_modes:
    - standard
author:
    - 9990518
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "frieren:_beyond_journey's_end"
    - "sousou_no_frieren"
    - "frieren"
categories:
    - anime
skin_collection:
    - name: "Beyond Journey's End (Frieren)"
      screenshots:
          - "https://i.imgur.com/qeH9n1c.jpeg"
          - "https://i.imgur.com/cwXGNUt.jpeg"
          - "https://i.imgur.com/M3GVsNl.jpeg"
          - "https://i.imgur.com/jOrkbOr.jpeg"
          - "https://i.imgur.com/zmmgOGQ.jpeg"
          - "https://i.imgur.com/kmwTgWh.jpeg"
          - "https://i.imgur.com/93DBEs2.jpeg"
videos:
    skinship:
    author:
---
