---
layout: skin

skin_name: "符玄 Master Diviner"
forum_thread_id: 1811913
date_added: 2023-12-17
game_modes:
    - standard
author:
    - 12630408
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hsr"
    - "honkai:_star_rail"
    - "fu_xuan"
categories:
    - anime
    - eyecandy
    - game
skin_collection:
    - name: "符玄 Master Diviner"
      screenshots:
          - "https://i.imgur.com/cDhHkVL.jpg"
          - "https://i.imgur.com/veE7yzI.jpg"
          - "https://i.imgur.com/zMRSu11.jpg"
          - "https://i.imgur.com/lJv38Zp.png"
          - "https://i.imgur.com/whvjh2t.jpg"
          - "https://i.imgur.com/WYBU6kG.png"
          - "https://i.imgur.com/MKGSSsi.png"
          - "https://i.imgur.com/ckwuclh.jpg"
          - "https://i.imgur.com/rqZxQII.png"
videos:
    skinship:
    author:
---
