---
layout: skin

skin_name: "[杏山カズサ] Kyouyama Kazusa"
forum_thread_id: 1706943
date_added: 2023-06-03
game_modes:
    - standard
author:
    - 23630035
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "blue_archive"
    - "kyouyama_kazusa"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "[杏山カズサ] Kyouyama Kazusa"
      screenshots:
          - "https://i.imgur.com/PhIQAS5.png"
          - "https://i.imgur.com/OLNhm9A.png"
          - "https://i.imgur.com/8nPrziq.png"
          - "https://i.imgur.com/dii62st.png"
          - "https://i.imgur.com/dNxcCcQ.jpg"
          - "https://i.imgur.com/eYv5Mpe.png"
          - "https://i.imgur.com/C132JOM.png"
          - "https://i.imgur.com/ENX07xc.png"
          - "https://i.imgur.com/wJcD1aS.png"
          - "https://i.imgur.com/xzUbmRa.png"
          - "https://i.imgur.com/pRmQXID.png"
          - "https://i.imgur.com/8LliEw1.png"
          - "https://i.imgur.com/a8yvU8w.png"
          - "https://i.imgur.com/9axg5Lz.png"
videos:
    skinship:
    author:
---
