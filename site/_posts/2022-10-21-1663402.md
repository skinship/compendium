---
layout: skin

skin_name: "Kagemori Michiru"
forum_thread_id: 1663402
date_added: 2022-12-13
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 25025133
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "bna"
    - "kagemori_michiru"
categories:
    - anime
skin_collection:
    - name: "Kagemori Michiru"
      screenshots:
          - "https://i.imgur.com/s63gjt8.jpg"
          - "https://i.imgur.com/Rxi0Rab.png"
          - "https://i.imgur.com/OfjZVDt.png"
          - "https://i.imgur.com/dvRaXIg.png"
          - "https://i.imgur.com/kmHddtK.png"
          - "https://i.imgur.com/O2vsIoY.png"
          - "https://i.imgur.com/vPuVSRt.png"
          - "https://i.imgur.com/rn9dQYA.png"
          - "https://i.imgur.com/xahCt3C.png"
          - "https://i.imgur.com/XAdYTTd.png"
          - "https://i.imgur.com/kejdAuc.png"
          - "https://i.imgur.com/rbywoIy.png"
          - "https://i.imgur.com/mZT9ulr.png"
          - "https://i.imgur.com/limFrBh.png"
          - "https://i.imgur.com/TyGJFx3.png"
videos:
    skinship:
    author:
---
