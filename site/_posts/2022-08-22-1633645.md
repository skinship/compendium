---
layout: skin

skin_name: "Desert Tempest"
forum_thread_id: 1633645
date_added: 2022-09-06
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 19082107
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "contest_3_submission"
    - "desert"
    - "deuteranopia"
categories:
    - minimalistic
skin_collection:
    - name: "Desert Tempest"
      screenshots:
          - "https://i.imgur.com/q8Ocdfe.png"
          - "https://i.imgur.com/W0UFnKV.png"
          - "https://i.imgur.com/TpYtKPS.png"
          - "https://i.imgur.com/pRPwP8O.png"
          - "https://i.imgur.com/oUkxmjO.png"
          - "https://i.imgur.com/9n7AIhP.png"
          - "https://i.imgur.com/9qf1Vot.png"
          - "https://i.imgur.com/EFcGZQ0.png"
          - "https://i.imgur.com/G8Q1HxC.png"
          - "https://i.imgur.com/EYjiIwY.png"
          - "https://i.imgur.com/WvSJtUm.png"
          - "https://i.imgur.com/m2l3tbK.png"
videos:
    skinship:
        - LVnWvV_jo6s
    author:
---
