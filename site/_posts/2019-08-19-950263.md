---
layout: skin

skin_name: "sakuraGLASS"
forum_thread_id: 950263
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 10974678
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "sakuraGLASS"
      screenshots:
          - "http://i.imgur.com/2gGnXzW.png"
          - "http://i.imgur.com/VpKXxaD.png"
          - "http://i.imgur.com/F76zAZH.png"
          - "http://i.imgur.com/pepwWjU.png"
          - "http://i.imgur.com/zpIy3lA.png"
          - "http://i.imgur.com/FIjD1bb.png"
          - "http://i.imgur.com/5Tinzbs.png"
          - "http://i.imgur.com/g4M1g9o.png"
          - "http://i.imgur.com/18ndWjb.png"
          - "http://i.imgur.com/T96Cv42.png"
          - "http://i.imgur.com/ZILLPUk.jpg"
          - "http://i.imgur.com/6WwQ5Eu.png"
          - "http://i.imgur.com/QkqynrU.jpg"
          - "http://i.imgur.com/K0mHxNN.png"
          - "http://i.imgur.com/FanPsgn.png"
          - "http://i.imgur.com/H86DFKK.jpg"
          - "http://i.imgur.com/PfcskxF.png"
videos:
    skinship:
    author:
---
