---
layout: skin

skin_name: "Shiro"
forum_thread_id: 1612702
date_added: 2022-07-18
game_modes:
    - standard
author:
    - 21559352
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "animated"
    - "shiro"
    - "no_game_no_life"
    - "ngnl"
categories:
    - anime
skin_collection:
    - name: "Shiro"
      screenshots:
          - "https://i.imgur.com/yTG9vNu.png"
          - "https://i.imgur.com/Q6f71Dj.png"
          - "https://i.imgur.com/OylMJBh.png"
          - "https://i.imgur.com/uMOhp5s.png"
          - "https://i.imgur.com/xLVISkY.png"
          - "https://i.imgur.com/7yLwbKr.png"
          - "https://i.imgur.com/1TKysja.png"
          - "https://i.imgur.com/W7GUYhl.png"
          - "https://i.imgur.com/rU2nlPD.png"
          - "https://i.imgur.com/XqmaGXA.png"
          - "https://i.imgur.com/LllQin4.png"
          - "https://i.imgur.com/HdUR6gs.png"
          - "https://i.imgur.com/XBa45fN.png"
          - "https://i.imgur.com/56KrZlK.png"
          - "https://i.imgur.com/dsjOOrI.png"
videos:
    skinship:
    author:
---
