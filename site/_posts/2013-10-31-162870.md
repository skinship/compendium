---
layout: skin

skin_name: "AXIOM"
forum_thread_id: 162870
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
author:
    - 9974
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "AXIOM"
      screenshots:
          - "http://i.imgur.com/igat8XE.jpg"
          - "http://i.imgur.com/aXl2bq3.jpg"
          - "http://i.imgur.com/74pfshB.png"
          - "http://i.imgur.com/ctUxZF1.png"
          - "http://i.imgur.com/xb7besA.png"
          - "http://i.imgur.com/uCIAwXa.jpg"
          - "http://i.imgur.com/YLDP5LD.png"
videos:
    skinship:
    author:
---
