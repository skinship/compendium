---
layout: skin

skin_name: "NeOsu 3"
forum_thread_id: 132898
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 852867
resolutions:
ratios:
tags:
    - "neosu"
categories:
    - minimalistic
skin_collection:
    - name: "NeOsu 3"
      screenshots:
          - "http://i.imgur.com/7bgfeXa.jpg"
          - "http://i.imgur.com/bEM1kAP.jpg"
          - "http://i.imgur.com/rEOKFi5.png"
          - "http://i.imgur.com/IwnarAF.png"
          - "http://i.imgur.com/aikx4rS.png"
          - "http://i.imgur.com/J40EPsR.png"
          - "http://i.imgur.com/JBjlYAa.png"
videos:
    skinship:
    author:
---
