---
layout: skin

skin_name: "ショボーン(´･ω･)"
forum_thread_id: 1533613
date_added: 2022-03-20
game_modes:
    - standard
    - mania
author:
    - 18186622
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "4k"
categories:
    - anime
skin_collection:
    - name: "ショボーン(´･ω･)"
      screenshots:
          - "https://i.imgur.com/G7viNWy.png"
          - "https://i.imgur.com/og7uCTO.png"
          - "https://i.imgur.com/q1Hh8LD.png"
          - "https://i.imgur.com/WRXGqnF.png"
          - "https://i.imgur.com/hznLAM2.png"
          - "https://i.imgur.com/NT9tS7g.png"
          - "https://i.imgur.com/5W13fb4.png"
          - "https://i.imgur.com/V1BKMT5.png"
          - "https://i.imgur.com/9lfgdHx.png"
          - "https://i.imgur.com/rDzhSOE.png"
videos:
    skinship:
    author:
---
