---
layout: skin

skin_name: "白上フブキ 日本語"
forum_thread_id: 1122171
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 12805532
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "shirakami_fubuki"
    - "hololive_gamers"
    - "hololive_1st_generation"
    - "vtuber"
categories:
    - anime
skin_collection:
    - name: "白上フブキ 日本語"
      screenshots:
          - "https://i.imgur.com/OW9GDqPg.png"
          - "https://i.imgur.com/OW9GDqPg.png"
          - "https://i.imgur.com/w8zMri8g.jpg"
          - "https://i.imgur.com/w8zMri8g.jpg"
          - "https://i.imgur.com/L15HosJg.jpg"
          - "https://i.imgur.com/iXi85Fyg.jpg"
          - "https://i.imgur.com/iXi85Fyg.jpg"
videos:
    skinship:
    author:
---
