---
layout: skin

skin_name: "Date a Live - Itsuka Kotori"
forum_thread_id: 1261998
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 12612018
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "itsuka_kotori"
    - "date_a_live"
categories:
    - anime
skin_collection:
    - name: "Date a Live - Itsuka Kotori"
      screenshots:
          - "https://i.imgur.com/scpsScr.png"
          - "https://i.imgur.com/Ugq8g7s.jpg"
          - "https://i.imgur.com/PbAWzNk.png"
          - "https://i.imgur.com/WcjQNDY.png"
          - "https://i.imgur.com/gyTfRQ7.png"
          - "https://i.imgur.com/Jbyn6iq.png"
          - "https://i.imgur.com/Dw5drvL.png"
          - "https://i.imgur.com/7FIyc9g.png"
          - "https://i.imgur.com/sTQy9up.png"
          - "https://i.imgur.com/M3MZzf5.png"
          - "https://i.imgur.com/LW1zkSt.png"
          - "https://i.imgur.com/qOSVLz9.png"
          - "https://i.imgur.com/KuggomL.png"
videos:
    skinship:
    author:
---
