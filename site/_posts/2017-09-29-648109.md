---
layout: skin

skin_name: "Blue Exorcist"
forum_thread_id: 648109
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 5212789
resolutions:
    - hd
    - sd
ratios:
tags:
    - "blue_exorcist"
    - "ao_no_exorcist"
    - "rin_okumura"
categories:
    - anime
skin_collection:
    - name: "Blue Exorcist"
      screenshots:
          - "http://i.imgur.com/ycJgYxE.jpg"
          - "http://i.imgur.com/mqKHe5g.jpg"
          - "http://i.imgur.com/QjSpArp.jpg"
          - "http://i.imgur.com/YY0Yru2.png"
          - "http://i.imgur.com/xPeGCDe.jpg"
          - "http://i.imgur.com/pSr1RhK.jpg"
          - "http://i.imgur.com/jtAZphZ.png"
          - "http://i.imgur.com/PxY4VfS.jpg"
          - "http://i.imgur.com/AEkWfXS.jpg"
videos:
    skinship:
    author:
---
