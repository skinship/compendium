---
layout: skin

skin_name: "FocusBlue"
forum_thread_id: 1210476
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 4236855
resolutions:
    - hd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "FocusBlue"
      screenshots:
          - "https://i.imgur.com/8kBgDzf.png"
          - "https://i.imgur.com/Gbyfrcn.png"
          - "https://i.imgur.com/0PSE84r.png"
          - "https://i.imgur.com/qk69m5N.png"
          - "https://i.imgur.com/DQJ6DHO.png"
          - "https://i.imgur.com/6zovMIT.png"
          - "https://i.imgur.com/OpuJkEc.png"
          - "https://i.imgur.com/hSej96Y.png"
          - "https://i.imgur.com/ETPodcY.png"
          - "https://i.imgur.com/YAf9uxb.png"
          - "https://i.imgur.com/9Umya6i.png"
          - "https://i.imgur.com/kDvLYu3.png"
videos:
    skinship:
    author:
---
