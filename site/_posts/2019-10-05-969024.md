---
layout: skin

skin_name: "Hitori Bocchi"
forum_thread_id: 969024
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 7087699
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hitoribocchi_no_marumaru_seikatsu"
categories:
    - anime
skin_collection:
    - name: "Hitori Bocchi"
      screenshots:
          - "http://i.imgur.com/ar9VPJs.jpg"
          - "http://i.imgur.com/UwAvgMZ.jpg"
          - "http://i.imgur.com/KNXFJrF.jpg"
          - "http://i.imgur.com/dKrfXAS.jpg"
          - "http://i.imgur.com/2gWklLY.jpg"
          - "http://i.imgur.com/5e93cTQ.jpg"
          - "http://i.imgur.com/7G7Ugfk.jpg"
          - "http://i.imgur.com/rhSa0rb.jpg"
          - "http://i.imgur.com/PjA2P4B.jpg"
          - "http://i.imgur.com/pCcsXi4.jpg"
videos:
    skinship:
    author:
---
