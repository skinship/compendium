---
layout: skin

skin_name: "Yuyuko Saigyouji"
forum_thread_id: 793236
date_added: 2021-07-19
game_modes:
    - standard
author:
    - 11932306
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "saigyouji_yuyuko"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "Yuyuko Saigyouji"
      screenshots:
          - "https://i.imgur.com/UJHQzUe.png"
          - "https://i.imgur.com/wl2lj9x.png"
          - "https://i.imgur.com/JHyiMjF.png"
          - "https://i.imgur.com/YLhGXwC.png"
          - "https://i.imgur.com/tdwaXWf.png"
          - "https://i.imgur.com/VoIb76k.png"
          - "https://i.imgur.com/gIsXGg7.png"
          - "https://i.imgur.com/9Dr7X3Z.png"
          - "https://i.imgur.com/gU1QymH.png"
          - "https://i.imgur.com/4jIuZEq.png"
videos:
    skinship:
    author:
---
