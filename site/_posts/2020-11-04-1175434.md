---
layout: skin

skin_name: "The Journey of Elaina"
forum_thread_id: 1175434
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 15258786
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "16:10"
tags:
    - "majo_no_tabitabi"
    - "wandering_witch:_the_journey_of_elaina"
    - "elaina"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "The Journey of Elaina"
      screenshots:
          - "https://i.imgur.com/t0FlnRB.png"
          - "https://i.imgur.com/UA1WefD.jpg"
          - "https://i.imgur.com/wPdtJW0.png"
          - "https://i.imgur.com/6lWrS5H.png"
          - "https://i.imgur.com/o8A0k4A.png"
          - "https://i.imgur.com/6HmhW9l.png"
          - "https://i.imgur.com/h6oPHMH.png"
          - "https://i.imgur.com/77SsPXo.png"
          - "https://i.imgur.com/F4S6XLR.png"
          - "https://i.imgur.com/kjUY4xo.png"
          - "https://i.imgur.com/SGHvNJv.png"
          - "https://i.imgur.com/3JvcrZJ.png"
          - "https://i.imgur.com/9jasygR.png"
videos:
    skinship:
    author:
---
