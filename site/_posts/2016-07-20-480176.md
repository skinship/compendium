---
layout: skin

skin_name: "ze_or"
forum_thread_id: 480176
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 6497935
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "ze_or"
      screenshots:
          - "http://i.imgur.com/DiabWEZ.jpg"
          - "http://i.imgur.com/It9yj19.jpg"
          - "http://i.imgur.com/ZzAYOlE.png"
          - "http://i.imgur.com/NUxVANG.png"
          - "http://i.imgur.com/MpY9KUN.png"
videos:
    skinship:
    author:
---
