---
layout: skin

skin_name: "Neynda. Skin Remaster"
forum_thread_id: 1291076
date_added: 2021-07-15
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 8196177
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - minimalistic
skin_collection:
    - name: "Neynda. Skin Remaster"
      screenshots:
          - "https://i.imgur.com/7QECwnI.jpeg"
          - "https://i.imgur.com/7bgwNOL.jpeg"
          - "https://i.imgur.com/EFrFT9S.jpeg"
          - "https://i.imgur.com/0r6hmQo.jpeg"
          - "https://i.imgur.com/FnA5jpS.jpeg"
          - "https://i.imgur.com/5ofX7Jw.jpeg"
          - "https://i.imgur.com/ge4RLR7.jpeg"
          - "https://i.imgur.com/BMk7iJh.jpeg"
          - "https://i.imgur.com/jzk5JS2.jpeg"
          - "https://i.imgur.com/cmn1ZMb.jpeg"
          - "https://i.imgur.com/WGQ5XUR.jpeg"
videos:
    skinship:
        - zRmFAE0qnRY
    author:
        - K50dvtaE9WY
---
