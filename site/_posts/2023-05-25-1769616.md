---
layout: skin

skin_name: "xoq"
forum_thread_id: 1769616
date_added: 2023-12-23
game_modes:
    - standard
author:
    - 6785191
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "xoq"
      screenshots:
          - "https://i.imgur.com/aG53hEN.png"
          - "https://i.imgur.com/3eoVdaS.png"
          - "https://i.imgur.com/DydkoqL.png"
          - "https://i.imgur.com/uXRVSx6.png"
          - "https://i.imgur.com/F5xLdVE.png"
          - "https://i.imgur.com/DPUA3Lw.png"
          - "https://i.imgur.com/kmM0waF.png"
          - "https://i.imgur.com/Bu1wTkB.png"
          - "https://i.imgur.com/7yff8U1.png"
          - "https://i.imgur.com/lcNaLvB.png"
          - "https://i.imgur.com/kumLkGm.png"
          - "https://i.imgur.com/5jxiC4Y.png"
          - "https://i.imgur.com/Bh9YgnF.png"
          - "https://i.imgur.com/cYDEG2h.png"
videos:
    skinship:
    author:
---
