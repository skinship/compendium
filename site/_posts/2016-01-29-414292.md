---
layout: skin

skin_name: "re_metrosu!"
forum_thread_id: 414292
date_added: 2021-06-28
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 5610209
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "re_metrosu!"
      screenshots:
          - "http://i.imgur.com/pImqpf2.jpg"
          - "http://i.imgur.com/taP7qfG.jpg"
          - "http://i.imgur.com/dshAQ29.jpg"
          - "http://i.imgur.com/tEwnNgf.png"
          - "http://i.imgur.com/v5Lm4pK.png"
          - "http://i.imgur.com/TA1udy9.png"
          - "http://i.imgur.com/WsMf3Z5.png"
videos:
    skinship:
    author:
---
