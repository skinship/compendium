---
layout: skin

skin_name: "Tewi Inaba 2"
forum_thread_id: 228705
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 3991274
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "inaba_tewi"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "Tewi Inaba 2"
      screenshots:
          - "http://i.imgur.com/pp4FcYt.jpg"
          - "http://i.imgur.com/S1Q9P22.jpg"
          - "http://i.imgur.com/RRMoryq.png"
          - "http://i.imgur.com/WgH1qUJ.png"
          - "http://i.imgur.com/R5rx4OB.png"
          - "http://i.imgur.com/sfXG6xi.png"
videos:
    skinship:
    author:
---
