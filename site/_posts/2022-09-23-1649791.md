---
layout: skin

skin_name: "HAYASE YUUKA"
forum_thread_id: 1649791
date_added: 2023-11-06
game_modes:
    - standard
author:
    - 4561368
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "blue_archive"
    - "hayase_yuuka"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "HAYASE YUUKA"
      screenshots:
          - "https://i.imgur.com/DHj51RM.jpg"
          - "https://i.imgur.com/sFYz7Is.jpg"
          - "https://i.imgur.com/bk1skMq.jpg"
          - "https://i.imgur.com/mtxOpzS.jpg"
          - "https://i.imgur.com/mwLntzv.jpg"
          - "https://i.imgur.com/C3cUVNR.jpg"
          - "https://i.imgur.com/EpNaskf.jpg"
          - "https://i.imgur.com/cWsOVsK.jpg"
          - "https://i.imgur.com/JLbNkCH.jpg"
          - "https://i.imgur.com/oplFY1O.jpg"
          - "https://i.imgur.com/Co3NjT2.jpg"
videos:
    skinship:
    author:
---
