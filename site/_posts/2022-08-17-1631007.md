---
layout: skin

skin_name: "minionalist."
forum_thread_id: 1631007
date_added: 2022-09-06
game_modes:
    - standard
    - catch
author:
    - 2837685
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_3_submission"
    - "minion"
    - "despicable_me"
    - "protanopia"
categories:
    - minimalistic
    - eyecandy
    - joke
skin_collection:
    - name: "minionalist."
      screenshots:
          - "https://i.imgur.com/Npy1TJX.jpg"
          - "https://i.imgur.com/jBpH5i1.png"
          - "https://i.imgur.com/8zFAOdl.png"
          - "https://i.imgur.com/WN8qPRI.png"
          - "https://i.imgur.com/B5qrdL0.png"
          - "https://i.imgur.com/tXGJOcM.png"
          - "https://i.imgur.com/tQlyJRf.png"
          - "https://i.imgur.com/SVpVZaK.png"
          - "https://i.imgur.com/EiKwcVm.png"
          - "https://i.imgur.com/vlaePCg.png"
          - "https://i.imgur.com/1DylzjO.png"
          - "https://i.imgur.com/acGnu2X.png"
videos:
    skinship:
        - CXrCZGU4sXo
    author:
---
