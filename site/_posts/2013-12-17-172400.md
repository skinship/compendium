---
layout: skin

skin_name: "Slightly Minimalistic Skin"
forum_thread_id: 172400
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 466288
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Slightly Minimalistic Skin"
      screenshots:
          - "http://i.imgur.com/PsjYLDP.jpg"
          - "http://i.imgur.com/c1DA1GR.jpg"
          - "http://i.imgur.com/jNtT7G2.png"
          - "http://i.imgur.com/OxYMKY7.png"
          - "http://i.imgur.com/JQln1Dv.png"
videos:
    skinship:
    author:
---
