---
layout: skin

skin_name: "osu!checkmate"
forum_thread_id: 1926184
date_added: 2024-06-05
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 16849319
resolutions:
    - hd
    - sd
ratios:
    - "all"
tags:
    - "contest_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
    - "chess"
    - "contest_5_submission"
categories:
    - minimalistic
skin_collection:
    - name: "osu!checkmate"
      screenshots:
          - "https://i.imgur.com/ccCK9uU.png"
          - "https://i.imgur.com/gRJHINU.png"
          - "https://i.imgur.com/ckRLsAU.png"
videos:
    skinship:
        - LnmRJmultS4
    author:
---
