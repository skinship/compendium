---
layout: skin

skin_name: "Bidoof"
forum_thread_id: 1661844
date_added: 2023-01-25
game_modes:
    - standard
author:
    - 18707960
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "pokemon"
    - "bidoof"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "Bidoof"
      screenshots:
          - "https://i.imgur.com/r1EwS6S.png"
          - "https://i.imgur.com/hSp5jX4.png"
          - "https://i.imgur.com/N9IlDe6.png"
          - "https://i.imgur.com/w28z682.png"
          - "https://i.imgur.com/B3ZmfBj.png"
          - "https://i.imgur.com/WCIAJhv.png"
          - "https://i.imgur.com/VczxUuL.png"
          - "https://i.imgur.com/fmfUztM.png"
          - "https://i.imgur.com/ARUQsdD.png"
          - "https://i.imgur.com/Z84vhVd.png"
videos:
    skinship:
    author:
---
