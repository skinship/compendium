---
layout: skin

skin_name: "[TUYU] Being Low as Dirt"
forum_thread_id: 1461868
date_added: 2021-12-08
game_modes:
    - standard
author:
    - 9782756
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "tuyu"
    - "jpop"
    - "music"
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "[TUYU] Being Low as Dirt"
      screenshots:
          - "http://i.imgur.com/0xT4VRI.jpg"
          - "http://i.imgur.com/3Qlx3df.jpg"
          - "http://i.imgur.com/RvojOLu.jpg"
          - "http://i.imgur.com/JCSqCxT.jpg"
          - "http://i.imgur.com/1y6UAdS.jpg"
          - "http://i.imgur.com/ceku7XW.jpg"
          - "http://i.imgur.com/EAAlhGv.jpg"
          - "http://i.imgur.com/JaCJuKV.jpg"
          - "http://i.imgur.com/LbUpgG2.jpg"
          - "http://i.imgur.com/jqAaN1Y.jpg"
videos:
    skinship:
    author:
---
