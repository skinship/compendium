---
layout: skin

skin_name: "The Album"
forum_thread_id: 1264415
date_added: 2021-07-15
game_modes:
    - standard
    - mania
author:
    - 4236855
    - 9178563
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "k-pop"
    - "blackpink"
    - "music"
    - "4k"
    - "7k"
categories:
    - minimalistic
skin_collection:
    - name: "The Queen"
      screenshots:
          - "https://i.imgur.com/iV4AzrO.png"
          - "https://i.imgur.com/MDcvlvr.png"
          - "https://i.imgur.com/M56cY0c.png"
          - "https://i.imgur.com/q8iRQQP.png"
          - "https://i.imgur.com/PGo15yb.png"
          - "https://i.imgur.com/YxKFR6u.png"
          - "https://i.imgur.com/6zeqVla.png"
          - "https://i.imgur.com/91riRZU.png"
          - "https://i.imgur.com/P5e54rQ.png"
          - "https://i.imgur.com/bVBbker.png"
          - "https://i.imgur.com/5QhU5Sf.png"
          - "https://i.imgur.com/Tw7GBiM.png"
          - "https://i.imgur.com/qiLoiyr.png"
          - "https://i.imgur.com/121JhZw.png"
    - name: "The Warrior"
      screenshots:
          - "https://i.imgur.com/uXS5mZa.png"
          - "https://i.imgur.com/TW8EUbr.png"
          - "https://i.imgur.com/B9BaK0x.png"
          - "https://i.imgur.com/61Hu5Lr.png"
          - "https://i.imgur.com/c8M7Z0A.png"
          - "https://i.imgur.com/sqHOLcs.png"
          - "https://i.imgur.com/mzK8GkV.png"
          - "https://i.imgur.com/K6z4E3n.png"
          - "https://i.imgur.com/CYRRVae.png"
          - "https://i.imgur.com/H0KsD2w.png"
          - "https://i.imgur.com/BNeIFxk.png"
          - "https://i.imgur.com/6P9xUVv.png"
          - "https://i.imgur.com/ry3s0vW.png"
videos:
    skinship:
    author:
---
