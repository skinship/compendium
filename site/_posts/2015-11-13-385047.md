---
layout: skin

skin_name: "Hexarcle"
forum_thread_id: 385047
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - catch
author:
    - 5610209
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Hexarcle"
      screenshots:
          - "http://i.imgur.com/WidUe5g.jpg"
          - "http://i.imgur.com/eRpqrnp.png"
          - "http://i.imgur.com/ceRSpCS.png"
          - "http://i.imgur.com/qn93RWi.png"
          - "http://i.imgur.com/PBDPM0Q.png"
videos:
    skinship:
    author:
---
