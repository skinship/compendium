---
layout: skin

skin_name: "YUGEN REMASTERED"
forum_thread_id: 1999325
date_added: 2025-01-25
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 2130664
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
tags:
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
categories:
    - minimalistic
skin_collection:
    - name: "YUGEN REMASTERED"
      screenshots:
          - "https://i.imgur.com/Itg58q2.png"
          - "https://i.imgur.com/kNoN2HR.png"
          - "https://i.imgur.com/rpFY4eb.png"
          - "https://i.imgur.com/twYjjI5.png"
          - "https://i.imgur.com/WxcabCm.png"
          - "https://i.imgur.com/n6dIc59.png"
videos:
    skinship:
    author:
        - eQOs5eefVJI
---
