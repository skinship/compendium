---
layout: skin

skin_name: "ssuniie"
forum_thread_id: 1330492
date_added: 2021-07-17
game_modes:
    - standard
author:
    - 15031615
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "ssuniie"
      screenshots:
          - "https://i.imgur.com/qt3eVUu.png"
          - "https://i.imgur.com/NIAku9W.png"
          - "https://i.imgur.com/ZyHc2bB.png"
          - "https://i.imgur.com/78l64Ws.png"
          - "https://i.imgur.com/3u7FJpu.png"
          - "https://i.imgur.com/GgJOqen.png"
videos:
    skinship:
    author:
---
