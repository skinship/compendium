---
layout: skin

skin_name: "#moonscape 2.0"
forum_thread_id: 1761474
date_added: 2023-05-21
game_modes:
    - standard
author:
    - 10324621
resolutions:
    - hd
    - sd
ratios:
    - "all"
tags:
    - "stars"
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "#moonscape 2.0"
      screenshots:
          - "https://i.imgur.com/Lv7b1pY.png"
          - "https://i.imgur.com/q5awrMd.png"
          - "https://i.imgur.com/8av1BHU.png"
          - "https://i.imgur.com/3DsI7mz.png"
          - "https://i.imgur.com/bmpFrcA.png"
          - "https://i.imgur.com/8rFzku3.png"
          - "https://i.imgur.com/Onhca9c.png"
          - "https://i.imgur.com/QFMi7xz.png"
          - "https://i.imgur.com/cwJlLfF.png"
          - "https://i.imgur.com/ifAklht.png"
          - "https://i.imgur.com/AxRD0kl.png"
          - "https://i.imgur.com/ndV7o5j.png"
          - "https://i.imgur.com/dZGzFjm.png"
          - "https://i.imgur.com/ixN9veT.png"
          - "https://i.imgur.com/2QsU62j.png"
          - "https://i.imgur.com/oLnDLJc.png"
          - "https://i.imgur.com/dqQYqZq.png"
          - "https://i.imgur.com/aXbzTk7.png"
          - "https://i.imgur.com/1KyRabJ.png"
videos:
    skinship:
    author:
---
