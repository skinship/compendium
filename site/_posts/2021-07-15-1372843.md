---
layout: skin

skin_name: "Minimal Green"
forum_thread_id: 1372843
date_added: 2021-07-17
game_modes:
    - standard
author:
    - 8406057
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Minimal Green"
      screenshots:
          - "https://i.imgur.com/DmMIWqq.png"
          - "https://i.imgur.com/1qOruDG.png"
          - "https://i.imgur.com/V0MgF4C.png"
          - "https://i.imgur.com/kQ4zaTe.png"
          - "https://i.imgur.com/BXTBvRo.png"
          - "https://i.imgur.com/YLNwETv.png"
          - "https://i.imgur.com/ed7mxOY.png"
          - "https://i.imgur.com/t74TYWE.png"
          - "https://i.imgur.com/1ZITMJf.png"
          - "https://i.imgur.com/IfMv4ie.png"
          - "https://i.imgur.com/SkpQT4B.png"
videos:
    skinship:
    author:
---
