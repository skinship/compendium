---
layout: skin

skin_name: "『 MarineSummer 』"
forum_thread_id: 1458935
date_added: 2022-04-02
game_modes:
    - standard
author:
    - 17561095
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "vtuber"
    - "hololive_3rd_generation"
    - "houshou_marine"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "『 MarineSummer 』"
      screenshots:
          - "https://i.imgur.com/woV0jMa.png"
          - "https://i.imgur.com/FsJ5xRL.png"
          - "https://i.imgur.com/ENmAh0X.png"
          - "https://i.imgur.com/MVaUSRT.png"
          - "https://i.imgur.com/xK38dmr.png"
          - "https://i.imgur.com/pbCJKhb.png"
          - "https://i.imgur.com/Um6W2CC.png"
          - "https://i.imgur.com/y7rjjH9.png"
          - "https://i.imgur.com/4jd7W6f.png"
          - "https://i.imgur.com/m3oLtTj.png"
          - "https://i.imgur.com/SI4zsOK.png"
          - "https://i.imgur.com/0I05f1x.png"
          - "https://i.imgur.com/nY6DXMP.png"
videos:
    skinship:
    author:
---
