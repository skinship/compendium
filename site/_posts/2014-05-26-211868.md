---
layout: skin

skin_name: "東方Project - Hinanawi Tenshi"
forum_thread_id: 211868
date_added: 2021-06-28
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 3717733
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hinanawi_tenshi"
    - "touhou_project"
categories:
    - anime
    - game
skin_collection:
    - name: "東方Project - Hinanawi Tenshi"
      screenshots:
          - "http://i.imgur.com/UCwMyxf.jpg"
          - "http://i.imgur.com/8dUMCEq.jpg"
          - "http://i.imgur.com/PObfsFE.png"
          - "http://i.imgur.com/NpGEd4U.jpg"
          - "http://i.imgur.com/MjgchJW.jpg"
          - "http://i.imgur.com/vlUXfIE.png"
videos:
    skinship:
    author:
---
