---
layout: skin

skin_name: "Lyoko vCrayon2"
forum_thread_id: 783254
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 12068337
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "16:10"
    - "21:9"
tags:
    - "handdrawn"
categories:
    - other
skin_collection:
    - name: "Lyoko vCrayon2"
      screenshots:
          - "http://i.imgur.com/wLprwJc.png"
          - "http://i.imgur.com/mzazwNn.png"
          - "http://i.imgur.com/Xz0bWf5.png"
          - "http://i.imgur.com/dq8Z7nn.png"
videos:
    skinship:
    author:
---
