---
layout: skin

skin_name: "TENHA"
forum_thread_id: 711441
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 10289337
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "TENHA"
      screenshots:
          - "https://i.imgur.com/7ZfihEq.jpg"
          - "https://i.imgur.com/n4JgfzQ.jpg"
videos:
    skinship:
    author:
---
