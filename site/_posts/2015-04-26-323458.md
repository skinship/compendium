---
layout: skin

skin_name: "Dota 2"
forum_thread_id: 323458
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2255108
resolutions:
    - hd
    - sd
ratios:
tags:
    - "dota"
categories:
    - game
skin_collection:
    - name: "Dota 2"
      screenshots:
          - "http://i.imgur.com/U41rPWs.jpg"
          - "http://i.imgur.com/ZzfqffL.jpg"
          - "http://i.imgur.com/SiVCERA.png"
          - "http://i.imgur.com/00ToG4O.jpg"
          - "http://i.imgur.com/bO5odm8.png"
videos:
    skinship:
    author:
---
