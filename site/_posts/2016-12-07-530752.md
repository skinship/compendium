---
layout: skin

skin_name: "Karen Desu"
forum_thread_id: 530752
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6084778
resolutions:
ratios:
tags:
    - "kiniro_mosaic"
    - "karen_kujo"
categories:
    - anime
skin_collection:
    - name: "Karen Desu"
      screenshots:
          - "http://i.imgur.com/kaqkQrV.jpg"
          - "http://i.imgur.com/mdzqhoH.jpg"
          - "http://i.imgur.com/ynTZiBg.jpg"
          - "http://i.imgur.com/9N0jWvs.jpg"
          - "http://i.imgur.com/21x4Aq0.jpg"
          - "http://i.imgur.com/9ysqyje.jpg"
          - "http://i.imgur.com/9vyomTg.jpg"
          - "http://i.imgur.com/rZ4nxH4.jpg"
          - "http://i.imgur.com/HA2nbLt.jpg"
          - "http://i.imgur.com/tGwXHfL.jpg"
          - "http://i.imgur.com/Jys46qD.jpg"
          - "http://i.imgur.com/OZ0u4Iv.jpg"
          - "http://i.imgur.com/rjsg1R7.jpg"
          - "http://i.imgur.com/txhX93k.jpg"
videos:
    skinship:
    author:
---
