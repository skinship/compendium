---
layout: skin

skin_name: "NIGHTGATE"
forum_thread_id: 504196
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 6932016
resolutions:
ratios:
tags:
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "NIGHTGATE"
      screenshots:
          - "http://i.imgur.com/Ixd9vxV.png"
          - "http://i.imgur.com/JxGgFMY.jpg"
          - "http://i.imgur.com/dYH03Xc.png"
          - "http://i.imgur.com/TZ7uNV3.png"
          - "http://i.imgur.com/qdmFRwz.jpg"
          - "http://i.imgur.com/41y1LmU.png"
          - "http://i.imgur.com/B2W6BvJ.png"
          - "http://i.imgur.com/Da4BxF8.jpg"
          - "http://i.imgur.com/LbxzirB.png"
          - "http://i.imgur.com/kdewhRE.jpg"
          - "http://i.imgur.com/vxtSJr7.png"
videos:
    skinship:
    author:
---
