---
layout: skin

skin_name: "Mai Sakurajima"
forum_thread_id: 1076552
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 7087699
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "mai_sakurajima"
    - "rascal_does_not_dream_of_bunny_girl_senpai"
    - "seishun_buta_yarou_wa_bunny_girl_senpai_no_yume_wo_minai"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Mai Sakurajima"
      screenshots:
          - "https://imgur.com/bnwvhx6.png"
          - "https://imgur.com/xvDXRa9.png"
          - "https://imgur.com/2WkHJJ1.png"
          - "https://imgur.com/53cJVuS.png"
          - "https://imgur.com/zH186pL.png"
          - "https://imgur.com/xJiBKJJ.png"
          - "https://imgur.com/uxVOF8m.png"
          - "https://imgur.com/TjHmA08.png"
          - "https://imgur.com/edhY4Py.png"
          - "https://imgur.com/RVX6WLe.png"
          - "https://imgur.com/dgmP68A.png"
          - "https://imgur.com/fsIDRO8.png"
          - "https://imgur.com/glJ9UJ3.png"
          - "https://imgur.com/8HOS4ff.png"
          - "https://imgur.com/wMfU6eQ.png"
          - "https://imgur.com/IM1xaSI.png"
          - "https://imgur.com/Ew4Rk8U.png"
videos:
    skinship:
    author:
---
