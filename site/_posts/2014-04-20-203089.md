---
layout: skin

skin_name: "Love Live! School idol Project"
forum_thread_id: 203089
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 946153
resolutions:
ratios:
tags:
    - "love_live"
categories:
    - anime
skin_collection:
    - name: "Love Live! School idol Project"
      screenshots:
          - "http://i.imgur.com/SYIHx6w.jpg"
          - "http://i.imgur.com/VoU3bkC.jpg"
          - "http://i.imgur.com/a16fhWD.jpg"
          - "http://i.imgur.com/icq7A0J.jpg"
          - "http://i.imgur.com/BrEcwV3.jpg"
          - "http://i.imgur.com/eoUL9PN.jpg"
          - "http://i.imgur.com/lBgcr1h.jpg"
          - "http://i.imgur.com/sDgV8oV.jpg"
videos:
    skinship:
    author:
---
