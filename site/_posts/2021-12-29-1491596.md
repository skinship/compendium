---
layout: skin

skin_name: "Gawr Gura『サメちゃん』"
forum_thread_id: 1491596
date_added: 2022-03-20
game_modes:
    - standard
    - taiko
    - catch
author:
    - 3717733
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "holomyth"
    - "holoen"
    - "vtuber"
    - "gawr_gura"
categories:
    - anime
skin_collection:
    - name: "Gawr Gura『サメちゃん』"
      screenshots:
          - "https://i.imgur.com/l1Sfxaj.png"
          - "https://i.imgur.com/zPIO61t.png"
          - "https://i.imgur.com/7P5zxbn.png"
          - "https://i.imgur.com/M2KgZS1.png"
          - "https://i.imgur.com/TgwlkjT.png"
          - "https://i.imgur.com/Er6bkX6.png"
          - "https://i.imgur.com/immGwbJ.png"
          - "https://i.imgur.com/2uAH7pe.png"
          - "https://i.imgur.com/X9qnWZE.png"
          - "https://i.imgur.com/JbWQTtH.png"
          - "https://i.imgur.com/L7fN8gw.png"
          - "https://i.imgur.com/sEAJTi6.png"
          - "https://i.imgur.com/kii8Dfm.png"
          - "https://i.imgur.com/4idqeSP.png"
          - "https://i.imgur.com/gc6Og0B.png"
          - "https://i.imgur.com/pknZql9.png"
          - "https://i.imgur.com/pknZql9.png"
          - "https://i.imgur.com/b5V1MWT.png"
          - "https://i.imgur.com/1rHp8hq.png"
          - "https://i.imgur.com/oaNCoFx.gif"
          - "https://i.imgur.com/iTB7XlP.gif"
          - "https://i.imgur.com/j5GMEAo.gif"
videos:
    skinship:
    author:
---
