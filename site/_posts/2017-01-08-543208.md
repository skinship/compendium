---
layout: skin

skin_name: "Galaxy"
forum_thread_id: 543208
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 3060807
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Galaxy"
      screenshots:
          - "http://i.imgur.com/gmtPSUt.jpg"
          - "http://i.imgur.com/ahIau8w.png"
          - "http://i.imgur.com/vXOUokf.png"
          - "http://i.imgur.com/rYclPcv.png"
          - "http://i.imgur.com/AWhFPCY.png"
          - "http://i.imgur.com/UtCSM4l.png"
          - "http://i.imgur.com/uUySwEh.png"
          - "http://i.imgur.com/DvJKI8r.png"
videos:
    skinship:
    author:
---
