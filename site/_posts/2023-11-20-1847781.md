---
layout: skin

skin_name: "Ougi Oshino 2.0"
forum_thread_id: 1847781
date_added: 2024-01-09
game_modes:
    - standard
author:
    - 13778459
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "monogatari"
    - "ougi_oshino"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Ougi Oshino 2.0"
      screenshots:
          - "https://i.imgur.com/lcaHZIK.jpg"
          - "https://i.imgur.com/J7XsrFC.jpg"
          - "https://i.imgur.com/lQ8FycQ.jpg"
          - "https://i.imgur.com/ZYVRWmx.jpg"
          - "https://i.imgur.com/ToX8jIM.jpg"
          - "https://i.imgur.com/8RRLOeI.jpg"
          - "https://i.imgur.com/cIKnlaj.jpg"
          - "https://i.imgur.com/5YzZauE.jpg"
          - "https://i.imgur.com/jhV7uWQ.jpg"
videos:
    skinship:
    author:
        - k2VJEswNIp8
---
