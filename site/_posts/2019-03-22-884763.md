---
layout: skin

skin_name: "Steins;Gate"
forum_thread_id: 884763
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2414531
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "steins;gate"
categories:
    - anime
skin_collection:
    - name: "Steins;Gate"
      screenshots:
          - "http://i.imgur.com/3avrjti.jpg"
          - "http://i.imgur.com/uzBYmaB.jpg"
          - "http://i.imgur.com/nHFx4Jr.jpg"
          - "http://i.imgur.com/ZyT2nFi.jpg"
          - "http://i.imgur.com/NT6DdwY.jpg"
          - "http://i.imgur.com/F0c4hfC.jpg"
          - "http://i.imgur.com/6DgbVfT.jpg"
          - "http://i.imgur.com/XfVPArk.jpg"
          - "http://i.imgur.com/rMNlYNp.jpg"
          - "http://i.imgur.com/0H1yez2.jpg"
          - "http://i.imgur.com/zUENqQB.jpg"
          - "http://i.imgur.com/Oyi8bKG.jpg"
          - "http://i.imgur.com/velvl6X.jpg"
          - "http://i.imgur.com/PuBWtC7.jpg"
videos:
    skinship:
    author:
---
