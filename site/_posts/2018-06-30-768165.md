---
layout: skin

skin_name: "ExPROject"
forum_thread_id: 768165
date_added: 2021-06-24
game_modes:
author:
    - 8241730
resolutions:
    - hd
    - sd
ratios:
    - "all"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "ExPROject"
      screenshots:
          - "https://i.imgur.com/mqfHNKc.png"
          - "https://i.imgur.com/WwJZTSF.png"
          - "https://i.imgur.com/rXp3dau.png"
          - "https://i.imgur.com/r9hvklm.png"
          - "https://i.imgur.com/Tgns19q.png"
videos:
    skinship:
    author:
---
