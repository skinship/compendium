---
layout: skin

skin_name: "AtmoS"
forum_thread_id: 749840
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7865082
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "AtmoS"
      screenshots:
          - "https://i.imgur.com/UEiV9b2.jpg"
          - "https://i.imgur.com/h305iPW.jpg"
          - "https://i.imgur.com/g83ci4X.jpg"
          - "https://i.imgur.com/hTD3QYT.jpg"
          - "https://i.imgur.com/02Zt4k7.jpg"
          - "https://i.imgur.com/ZU6tvCO.jpg"
          - "https://i.imgur.com/atJdci9.jpg"
          - "https://i.imgur.com/eQzMoDC.jpg"
videos:
    skinship:
    author:
---
