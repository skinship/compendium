---
layout: skin

skin_name: "IIDX osu! - Happy Sky"
forum_thread_id: 5934
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 17894
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "IIDX osu! - Happy Sky"
      screenshots:
          - "http://i.imgur.com/D75ZnK2.png"
          - "http://i.imgur.com/tD3jC4T.jpg"
          - "http://i.imgur.com/Ey1qn3j.png"
          - "http://i.imgur.com/BONAoBI.png"
          - "http://i.imgur.com/0AUwWOa.png"
videos:
    skinship:
    author:
---
