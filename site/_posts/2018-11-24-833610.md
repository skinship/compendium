---
layout: skin

skin_name: "Sakuya Izayoi - Koumajou Densetsu"
forum_thread_id: 833610
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 11932306
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "izayoi_sakuya"
    - "touhou_project"
categories:
    - anime
    - eyecandy
    - game
skin_collection:
    - name: "Sakuya Izayoi - Koumajou Densetsu"
      screenshots:
          - "http://i.imgur.com/BYf3U6K.png"
          - "http://i.imgur.com/eXZ5z2L.png"
          - "http://i.imgur.com/4O7qIct.png"
          - "http://i.imgur.com/n1G0NnT.png"
          - "http://i.imgur.com/H7Qm8Kx.png"
          - "http://i.imgur.com/N0DAje8.png"
          - "http://i.imgur.com/TgndPa6.png"
          - "http://i.imgur.com/3NaiZVR.png"
          - "http://i.imgur.com/XoR6Q0l.png"
          - "http://i.imgur.com/KsAiDjf.png"
          - "http://i.imgur.com/xpNRD3J.png"
          - "http://i.imgur.com/J5jbmnS.png"
videos:
    skinship:
    author:
---
