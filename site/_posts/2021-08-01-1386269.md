---
layout: skin

skin_name: "Alice Zuberg"
forum_thread_id: 1386269
date_added: 2021-08-03
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 16274977
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
    - "21:9"
tags:
    - "sword_art_online"
    - "sao"
    - "alice_zuberg"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Alice Zuberg"
      screenshots:
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469458319896586/mainMenu.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469486216216626/songSelect.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469485478010920/modSelect.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469541459365908/osu1.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469542575067157/osu2.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469514083160155/taiko1.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469514368385085/taiko2.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469571331215400/pauseScreen.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469571243130920/failScreen.png"
          - "https://cdn.discordapp.com/attachments/824941164020498472/871469603090464838/ranking.png"
videos:
    skinship:
    author:
---
