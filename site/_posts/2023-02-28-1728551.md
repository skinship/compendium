---
layout: skin

skin_name: "Neuro-sama's Little Gymbag"
forum_thread_id: 1728551
date_added: 2024-01-09
game_modes:
    - standard
author:
    - 11670013
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "vtuber"
    - "neuro-sama"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Neuro-sama's Little Gymbag"
      screenshots:
          - "https://i.imgur.com/fQN8M4y.png"
          - "https://i.imgur.com/2IZSujM.png"
          - "https://i.imgur.com/VSXf1ws.png"
          - "https://i.imgur.com/4haHnrb.png"
          - "https://i.imgur.com/x7c7leC.png"
          - "https://i.imgur.com/Pkrgyqo.png"
          - "https://i.imgur.com/ycfoDK9.png"
          - "https://i.imgur.com/2Qtj3Nj.png"
          - "https://i.imgur.com/yUQ0ZV1.png"
          - "https://i.imgur.com/HCgCeIy.png"
videos:
    skinship:
    author:
---
