---
layout: skin

skin_name: "Retome"
forum_thread_id: 630525
date_added: 2021-06-24
game_modes:
    - standard
    - catch
author:
    - 3996811
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Retome"
      screenshots:
          - "http://i.imgur.com/rxQabwE.jpg"
          - "http://i.imgur.com/fj1NSg3.jpg"
          - "http://i.imgur.com/GZl2LAu.jpg"
          - "http://i.imgur.com/vzzbfSX.jpg"
          - "http://i.imgur.com/FwjiSAL.jpg"
          - "http://i.imgur.com/p5J2Ahi.jpg"
          - "http://i.imgur.com/Sw5QgAO.jpg"
          - "http://i.imgur.com/5lgKXx0.jpg"
          - "http://i.imgur.com/CbvycTf.jpg"
          - "http://i.imgur.com/pobnP8N.jpg"
          - "http://i.imgur.com/lrw6BqF.jpg"
videos:
    skinship:
    author:
---
