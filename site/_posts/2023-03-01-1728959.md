---
layout: skin

skin_name: "WoopyBoy"
forum_thread_id: 1728959
date_added: 2023-05-28
game_modes:
    - mania
author:
    - 22165084
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "pokemon"
    - "4k"
    - "clodsire"
    - "wooper"
categories:
    - anime
    - game
skin_collection:
    - name: "WoopyBoy"
      screenshots:
          - "https://i.imgur.com/CdSqJOp.png"
          - "https://i.imgur.com/EN8c0M4.png"
          - "https://i.imgur.com/UQqGyAN.png"
          - "https://i.imgur.com/7ttj0N2.png"
          - "https://i.imgur.com/zsk9rlO.png"
videos:
    skinship:
    author:
---
