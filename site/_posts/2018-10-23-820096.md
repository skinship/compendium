---
layout: skin

skin_name: "O2Jam - XenoJam"
forum_thread_id: 820096
date_added: 2021-06-24
game_modes:
    - mania
author:
    - 8381177
resolutions:
    - sd
ratios:
    - "16:9"
tags:
    - "o2jam"
    - "vsrg"
categories:
    - game
skin_collection:
    - name: "O2Jam - XenoJam"
      screenshots:
          - "http://i.imgur.com/xP6f6mh.jpg"
          - "http://i.imgur.com/YMi56ue.jpg"
videos:
    skinship:
    author:
---
