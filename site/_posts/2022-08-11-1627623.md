---
layout: skin

skin_name: "UmbraBlue Interface"
forum_thread_id: 1627623
date_added: 2022-09-06
game_modes:
    - standard
    - taiko
    - catch
author:
    - 12091015
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "techno"
    - "futuristic"
    - "contest_submission"
    - "contest_3_submission"
    - "deuteranopia"
categories:
    - minimalistic
skin_collection:
    - name: "UmbraBlue Interface"
      screenshots:
          - "https://i.imgur.com/yfCQPUJ.png"
          - "https://i.imgur.com/FQeNutH.png"
          - "https://i.imgur.com/YEdi8pW.png"
          - "https://i.imgur.com/27OF2iZ.png"
          - "https://i.imgur.com/XGKs8O4.png"
          - "https://i.imgur.com/bUkd8DK.png"
          - "https://i.imgur.com/lJnOrQW.png"
          - "https://i.imgur.com/KqV5gyl.png"
          - "https://i.imgur.com/ztDx6G8.png"
          - "https://i.imgur.com/c0ldirD.png"
          - "https://i.imgur.com/mmHULb8.png"
          - "https://i.imgur.com/xdgafJy.png"
          - "https://i.imgur.com/lLcy4aD.png"
          - "https://i.imgur.com/1uN322m.png"
          - "https://i.imgur.com/JGzPxiJ.png"
          - "https://i.imgur.com/Oy3ej59.png"
          - "https://i.imgur.com/I0IirJY.png"
          - "https://i.imgur.com/PGE9xhx.png"
          - "https://i.imgur.com/9gmjeAe.png"
videos:
    skinship:
        - 5-fjbS9WKus
    author:
---
