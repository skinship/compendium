---
layout: skin

skin_name: "OT!skin collab"
forum_thread_id: 1246838
date_added: 2021-07-15
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 15646039
    - 4428094
    - 17753835
    - 11554533
    - 14455025
    - 12377250
    - 3176668
    - 18113570
    - 16063282
    - 10412881
    - 11120814
    - 9388535
    - 2419080
    - 12149247
    - 4635891
    - 16298129
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "offtopic"
    - "osu!forum"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
categories:
    - joke
skin_collection:
    - name: "OT!skin collab"
      screenshots:
          - "https://i.imgur.com/xGEqKfV.png"
          - "https://i.imgur.com/22IL0wW.png"
          - "https://i.imgur.com/YBFSD28.png"
          - "https://i.imgur.com/mmjXSxr.png"
          - "https://i.imgur.com/o2kHl3r.png"
          - "https://i.imgur.com/esQnWHz.png"
          - "https://i.imgur.com/sJ2Zo4w.png"
          - "https://i.imgur.com/2J23Fa7.png"
          - "https://i.imgur.com/RUH8U9h.png"
          - "https://i.imgur.com/v7EzPBG.png"
          - "https://i.imgur.com/n4q7fd7.png"
          - "https://i.imgur.com/SIgG2lt.png"
          - "https://i.imgur.com/4e9XIRZ.png"
          - "https://i.imgur.com/rpSsi5U.png"
          - "https://i.imgur.com/pRZGdf2.png"
          - "https://i.imgur.com/4Gn21l2.png"
          - "https://i.imgur.com/LETodGy.png"
          - "https://i.imgur.com/cdiKDEj.png"
          - "https://i.imgur.com/ZJlqbAX.png"
          - "https://i.imgur.com/58x2MgV.png"
          - "https://i.imgur.com/xOj051B.png"
          - "https://i.imgur.com/t47an4K.png"
          - "https://i.imgur.com/id9AGCB.png"
          - "https://i.imgur.com/VYeDLFP.png"
videos:
    skinship:
    author:
---
