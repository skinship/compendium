---
layout: skin

skin_name: "Minami Kotori - The Legendary Minalinsky"
forum_thread_id: 488896
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 2636217
resolutions:
    - hd
    - sd
ratios:
tags:
    - "kotori_minami"
    - "love_live"
    - "printemps"
categories:
    - anime
skin_collection:
    - name: "Minami Kotori - The Legendary Minalinsky"
      screenshots:
          - "http://i.imgur.com/vKhEvZf.jpg"
          - "https://i.imgur.com/cizz4jr.jpg"
          - "https://i.imgur.com/5Qp65VN.png"
          - "https://i.imgur.com/1YB2sYN.jpg"
          - "https://i.imgur.com/02ALFeb.png"
          - "https://i.imgur.com/bW8zMUv.png"
          - "https://i.imgur.com/B42KQrS.png"
          - "https://i.imgur.com/SN7i21n.jpg"
          - "https://i.imgur.com/DDLcvZZ.png"
          - "https://i.imgur.com/uKW5leF.png"
          - "http://i.imgur.com/R4nxBdT.jpg"
videos:
    skinship:
    author:
---
