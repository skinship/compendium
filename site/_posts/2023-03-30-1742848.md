---
layout: skin

skin_name: "Yuyuko Saigyouji (Touhou)"
forum_thread_id: 1742848
date_added: 2023-05-23
game_modes:
    - standard
author:
    - 12091015
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "saigyouji_yuyuko"
    - "touhou_project"
categories:
    - anime
    - minimalistic
    - eyecandy
    - game
skin_collection:
    - name: "Yuyuko Saigyouji (Touhou)"
      screenshots:
          - "https://i.imgur.com/QohdrIz.png"
          - "https://i.imgur.com/MlqjGPU.png"
          - "https://i.imgur.com/JUybCsD.png"
          - "https://i.imgur.com/GiEyOIk.png"
          - "https://i.imgur.com/C7JJpit.png"
          - "https://i.imgur.com/pDi1wvV.png"
          - "https://i.imgur.com/RYaZkQN.png"
          - "https://i.imgur.com/SWlNjur.png"
          - "https://i.imgur.com/U5Nu7tJ.png"
          - "https://i.imgur.com/Y7F5mbe.png"
          - "https://i.imgur.com/bv5jFgS.png"
          - "https://i.imgur.com/BmYVCHm.png"
          - "https://i.imgur.com/RoVU5rA.png"
          - "https://i.imgur.com/g60M7AG.png"
          - "https://i.imgur.com/JhwsvtA.png"
videos:
    skinship:
    author:
        - h23o7Z5LhHs
---
