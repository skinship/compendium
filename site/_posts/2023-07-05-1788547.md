---
layout: skin

skin_name: "United by Music"
forum_thread_id: 1788547
date_added: 2023-12-23
game_modes:
    - standard
author:
    - 12898432
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "music"
    - "eurovision"
categories:
    - minimalistic
skin_collection:
    - name: "United by Music"
      screenshots:
          - "https://i.imgur.com/clVpV9T.png"
          - "https://i.imgur.com/fyZxabG.png"
          - "https://i.imgur.com/JFeUix0.png"
          - "https://i.imgur.com/9STK01U.png"
          - "https://i.imgur.com/Iypqgmv.png"
          - "https://i.imgur.com/DdrgNnc.png"
          - "https://i.imgur.com/q9jFcZY.png"
          - "https://i.imgur.com/Fo7QnXi.png"
          - "https://i.imgur.com/6C5C5Kl.png"
          - "https://i.imgur.com/C0f3kRc.png"
          - "https://i.imgur.com/ni985NW.png"
videos:
    skinship:
    author:
---
