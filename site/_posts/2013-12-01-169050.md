---
layout: skin

skin_name: "Rainbow Dash"
forum_thread_id: 169050
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6347
resolutions:
ratios:
tags:
    - "my_little_pony"
    - "rainbow_dash"
categories:
    - other
skin_collection:
    - name: "Rainbow Dash"
      screenshots:
          - "http://i.imgur.com/IzBOo7b.jpg"
          - "http://i.imgur.com/ubF9tx2.jpg"
          - "http://i.imgur.com/4ZP5uSY.jpg"
          - "http://i.imgur.com/XnMWWic.jpg"
videos:
    skinship:
    author:
---
