---
layout: skin

skin_name: "Simplified Dark"
forum_thread_id: 1270022
date_added: 2021-07-15
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 7087699
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_1_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - minimalistic
skin_collection:
    - name: "Simplified Dark"
      screenshots:
          - "https://i.imgur.com/VDgDCoF.jpeg"
          - "https://i.imgur.com/JGJsJ9B.jpeg"
          - "https://i.imgur.com/LGUxaf7.jpeg"
          - "https://i.imgur.com/0DaVdOL.jpeg"
          - "https://i.imgur.com/VdELV48.jpeg"
          - "https://i.imgur.com/zZhEqls.jpeg"
          - "https://i.imgur.com/MBMnHao.jpeg"
          - "https://i.imgur.com/htOWqxs.jpeg"
          - "https://i.imgur.com/sVyxv9p.jpeg"
          - "https://i.imgur.com/ABiWsFL.jpeg"
          - "https://i.imgur.com/QN2SUVb.jpeg"
          - "https://i.imgur.com/hNhJUhs.jpeg"
          - "https://i.imgur.com/AsXeAxw.jpeg"
          - "https://i.imgur.com/GDxGawx.jpeg"
          - "https://i.imgur.com/IwoDOKL.jpeg"
          - "https://i.imgur.com/3wntzzd.jpeg"
          - "https://i.imgur.com/DrLvafZ.jpeg"
videos:
    skinship:
        - VY7OU96dnlQ
    author:
---
