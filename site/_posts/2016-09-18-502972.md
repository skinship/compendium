---
layout: skin

skin_name: "KRUEL"
forum_thread_id: 502972
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - catch
author:
    - 7095266
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "KRUEL"
      screenshots:
          - "http://i.imgur.com/5T2CyIQ.jpg"
          - "http://i.imgur.com/XF6osX1.jpg"
          - "http://i.imgur.com/CCMJ2Xj.jpg"
          - "http://i.imgur.com/xgrYCOu.jpg"
          - "http://i.imgur.com/STaJHMr.jpg"
          - "http://i.imgur.com/GeREvNZ.jpg"
          - "http://i.imgur.com/311s1ya.jpg"
          - "http://i.imgur.com/N3amaWw.jpg"
          - "http://i.imgur.com/D0Sa7BW.jpg"
          - "http://i.imgur.com/itMkOgT.jpg"
          - "http://i.imgur.com/LEqE58m.jpg"
          - "http://i.imgur.com/ZoFdt4P.jpg"
videos:
    skinship:
    author:
---
