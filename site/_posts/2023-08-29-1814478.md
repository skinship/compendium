---
layout: skin

skin_name: "Lunar Tempest"
forum_thread_id: 1814478
date_added: 2024-01-09
game_modes:
    - standard
    - mania
author:
    - 10506141
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hatsune_miku"
    - "vocaloid"
    - "4k"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Original"
      screenshots:
          - "https://i.imgur.com/UcMVx9C.png"
          - "https://i.imgur.com/2jXTOVf.png"
          - "https://i.imgur.com/PLbqQyi.png"
          - "https://i.imgur.com/3Af5aBo.png"
          - "https://i.imgur.com/cnhzCpT.png"
          - "https://i.imgur.com/Xz32Zl4.png"
          - "https://i.imgur.com/jw4tius.png"
          - "https://i.imgur.com/Rnp7aXN.png"
          - "https://i.imgur.com/vraFPrA.png"
          - "https://i.imgur.com/tPSddSA.png"
    - name: "Hatsune Miku"
      screenshots:
          - "https://i.imgur.com/yTU9nOT.png"
          - "https://i.imgur.com/q2fa97q.png"
          - "https://i.imgur.com/kVUNb8J.png"
          - "https://i.imgur.com/HddKmaz.png"
          - "https://i.imgur.com/mF5Mucu.png"
          - "https://i.imgur.com/h1ibqkQ.png"
          - "https://i.imgur.com/TYkvo4U.png"
          - "https://i.imgur.com/3xUHQN1.png"
          - "https://i.imgur.com/8p2tOoO.png"
          - "https://i.imgur.com/Ot9paO1.png"
videos:
    skinship:
    author:
        - r_0AS8LHaJE
        - yHjj9fWTZdY
---
