---
layout: skin

skin_name: "Pastel Rainbow"
forum_thread_id: 1589863
date_added: 2022-06-25
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 27745918
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
categories:
    - minimalistic
skin_collection:
    - name: "Pastel Rainbow"
      screenshots:
          - "https://i.imgur.com/KIke2I0.jpeg"
          - "https://i.imgur.com/F0QvARN.jpeg"
          - "https://i.imgur.com/CoCuCd9.jpeg"
          - "https://i.imgur.com/mLRoi3q.jpeg"
          - "https://i.imgur.com/Yy5qP94.jpeg"
          - "https://i.imgur.com/Nd2SwFa.jpeg"
          - "https://i.imgur.com/VOMSq7k.jpeg"
          - "https://i.imgur.com/5pRsbsk.jpeg"
          - "https://i.imgur.com/pZ1hkQL.jpeg"
          - "https://i.imgur.com/JPAa8lb.jpeg"
          - "https://i.imgur.com/DhK3Bkn.jpeg"
          - "https://i.imgur.com/VleHDwZ.jpeg"
          - "https://i.imgur.com/gEmRRE5.jpeg"
          - "https://i.imgur.com/OrSSRQB.jpeg"
          - "https://i.imgur.com/prUyhHK.jpeg"
videos:
    skinship:
    author:
---
