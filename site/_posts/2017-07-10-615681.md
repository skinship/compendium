---
layout: skin

skin_name: "QuasiCrystal"
forum_thread_id: 615681
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 8972308
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "QuasiCrystal"
      screenshots:
          - "http://i.imgur.com/qQPHK2W.jpg"
          - "http://i.imgur.com/XilBiLo.jpg"
          - "http://i.imgur.com/IU69QpY.jpg"
          - "http://i.imgur.com/ksgvR7t.jpg"
          - "http://i.imgur.com/1ssUjcx.png"
          - "http://i.imgur.com/WxvzgQ0.png"
          - "http://i.imgur.com/tC6ra6k.png"
          - "http://i.imgur.com/ej8kIZu.png"
          - "http://i.imgur.com/PQcvHIF.png"
videos:
    skinship:
    author:
---
