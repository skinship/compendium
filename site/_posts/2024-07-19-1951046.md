---
layout: skin

skin_name: "Kochiya Sanae"
forum_thread_id: 1951046
date_added: 2024-07-20
game_modes:
    - standard
author:
    - 27835528
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "sanae_kochiya"
    - "touhou_project"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Kochiya Sanae"
      screenshots:
          - "https://i.imgur.com/XgrVYwU.png?1"
          - "https://i.imgur.com/3pdpNiZ.png?1"
          - "https://i.imgur.com/1WCIPzX.png?1"
          - "https://i.imgur.com/OiRPuJ8.png?1"
          - "https://i.imgur.com/SCMLlh5.png?1"
          - "https://i.imgur.com/Z30YIxX.png?1"
          - "https://i.imgur.com/ZfPPrKO.jpg"
          - "https://i.imgur.com/I4R9RaE.jpg"
videos:
    skinship:
    author:
---
