---
layout: skin

skin_name: "Hikari"
forum_thread_id: 347474
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 4375851
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Hikari"
      screenshots:
          - "http://i.imgur.com/lbSdAmW.png"
          - "http://i.imgur.com/u9Xx5Ar.png"
          - "http://i.imgur.com/bveitTK.png"
          - "http://i.imgur.com/lrhvJIy.png"
          - "http://i.imgur.com/M7KlDBo.png"
          - "http://i.imgur.com/nG4HaAy.png"
          - "http://i.imgur.com/INOi3rf.png"
          - "http://i.imgur.com/oRYaL6B.png"
          - "http://i.imgur.com/h6e1T1k.png"
videos:
    skinship:
    author:
---
