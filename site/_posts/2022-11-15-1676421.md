---
layout: skin

skin_name: "Hakos Baelz"
forum_thread_id: 1676421
date_added: 2023-01-25
game_modes:
    - standard
author:
    - 25025133
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "holoen"
    - "holocouncil"
    - "vtuber"
    - "hakos_baelz"
categories:
    - anime
    - minimalistic
    - eyecandy
skin_collection:
    - name: "Hakos Baelz"
      screenshots:
          - "https://i.imgur.com/kOKRHEu.jpg"
          - "https://i.imgur.com/WR0ijRe.png"
          - "https://i.imgur.com/xHRE3lw.png"
          - "https://i.imgur.com/gclYNTm.png"
          - "https://i.imgur.com/7gOpnWn.png"
          - "https://i.imgur.com/sTmstlb.png"
          - "https://i.imgur.com/D6lHxES.png"
          - "https://i.imgur.com/AEpf0xZ.png"
          - "https://i.imgur.com/oLFdgtM.png"
videos:
    skinship:
    author:
---
