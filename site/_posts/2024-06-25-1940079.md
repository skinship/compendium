---
layout: skin

skin_name: "Saint Cecilia"
forum_thread_id: 1940079
date_added: 2024-07-11
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 27249602
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "saint_cecilia"
    - "saint_cecilia_and_pastor_lawrence"
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "Saint Cecilia"
      screenshots:
          - "https://i.imgur.com/P251m9l.png"
          - "https://i.imgur.com/aMATQvo.png"
          - "https://i.imgur.com/UEhQEof.png"
          - "https://i.imgur.com/4q9kK5n.png"
          - "https://i.imgur.com/C4iQAZK.png"
          - "https://i.imgur.com/ltQfOOh.png"
          - "https://i.imgur.com/dHMKq4q.png"
          - "https://i.imgur.com/rJoCvj4.png"
          - "https://i.imgur.com/VmUbifp.png"
          - "https://i.imgur.com/JJ1z3LR.png"
          - "https://i.imgur.com/xW1PPK8.png"
          - "https://i.imgur.com/bZ5L1eD.png"
          - "https://i.imgur.com/6WyBIFc.png"
          - "https://i.imgur.com/VJTyCiS.png"
          - "https://i.imgur.com/NDx0Jl7.png"
          - "https://i.imgur.com/HXIYAHu.png"
          - "https://i.imgur.com/j8MP8YU.png"
videos:
    skinship:
    author:
        - cA6ZL0jV6t0
---
