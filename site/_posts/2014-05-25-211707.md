---
layout: skin

skin_name: "Gochiusa"
forum_thread_id: 211707
date_added: 2021-07-18
game_modes:
    - standard
author:
    - 1806962
resolutions:
    - sd
ratios:
    - "16:9"
tags:
    - "is_the_order_a_rabbit?"
    - "gochuumon_wa_usagi_desu_ka?"
    - "gochiusa"
categories:
    - anime
skin_collection:
    - name: "Gochiusa"
      screenshots:
          - "http://i.imgur.com/EjLw5KJ.jpg"
          - "http://i.imgur.com/PMM1n7y.jpg"
          - "http://i.imgur.com/BEWL8Sn.png"
          - "http://i.imgur.com/bBvrk0T.png"
          - "http://i.imgur.com/akUUA77.jpg"
videos:
    skinship:
    author:
---
