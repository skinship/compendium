---
layout: skin

skin_name: "Phosphor"
forum_thread_id: 1920571
date_added: 2024-06-20
game_modes:
    - standard
    - mania
    - catch
author:
    - 12630408
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "retro"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "contest_5_submission"
    - "computer"
    - "contest_5_winner"
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "Phosphor"
      screenshots:
          - "https://i.imgur.com/Vo6d470.png"
          - "https://i.imgur.com/wjuWi0f.png"
          - "https://i.imgur.com/CjIKvco.png"
          - "https://i.imgur.com/Pp4FbxD.png"
          - "https://i.imgur.com/041ygX4.png"
          - "https://i.imgur.com/R6uzuis.png"
          - "https://i.imgur.com/lWvDfZN.png"
          - "https://i.imgur.com/I2K4N0D.png"
          - "https://i.imgur.com/1uxvUfi.png"
          - "https://i.imgur.com/6gGrcm4.png"
          - "https://i.imgur.com/elzQgLf.png"
          - "https://i.imgur.com/Ek2qrim.png"
          - "https://i.imgur.com/MthWja2.png"
          - "https://i.imgur.com/RT4Xx9N.png"
videos:
    skinship:
        - sw1nhClFPLg
    author:
---
