---
layout: skin

skin_name: "ogipote"
forum_thread_id: 891578
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 6084778
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "ogipote"
categories:
    - anime
skin_collection:
    - name: "ogipote"
      screenshots:
          - "http://i.imgur.com/SMN0AiS.png"
          - "http://i.imgur.com/jU0ximI.png"
          - "http://i.imgur.com/nRNO0Zx.png"
          - "http://i.imgur.com/eH3gFmX.png"
          - "http://i.imgur.com/bzsypbX.png"
          - "http://i.imgur.com/Hoyn05T.png"
          - "http://i.imgur.com/sHwrr3Y.png"
          - "http://i.imgur.com/mXvFERR.png"
          - "http://i.imgur.com/KrHmNqv.png"
          - "http://i.imgur.com/vcHsbeh.png"
          - "http://i.imgur.com/dv8kO6F.png"
videos:
    skinship:
    author:
---
