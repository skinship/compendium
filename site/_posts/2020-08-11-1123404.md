---
layout: skin

skin_name: "白上フブキ 英語"
forum_thread_id: 1123404
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 12805532
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "shirakami_fubuki"
    - "hololive_gamers"
    - "hololive_1st_generation"
categories:
    - anime
skin_collection:
    - name: "白上フブキ 英語"
      screenshots:
          - "https://i.imgur.com/ZT1OmY0.png"
          - "https://i.imgur.com/ZDtXdcZ.png"
          - "https://i.imgur.com/VJkbJ4U.png"
          - "https://i.imgur.com/UAXeeic.png"
videos:
    skinship:
    author:
---
