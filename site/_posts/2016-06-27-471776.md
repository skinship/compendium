---
layout: skin

skin_name: "Project VOEZ"
forum_thread_id: 471776
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 6932016
resolutions:
ratios:
tags:
    - "voez"
    - "vsrg"
categories:
    - eyecandy
    - game
skin_collection:
    - name: "Project VOEZ"
      screenshots:
          - "https://i.imgur.com/onSCqTk.png"
          - "https://i.imgur.com/Rn8jUTM.png"
          - "https://i.imgur.com/3m4vuhv.png"
          - "https://i.imgur.com/AMWV4XR.png"
          - "https://i.imgur.com/q2W2519.png"
          - "https://i.imgur.com/m1U8f8h.png"
          - "https://i.imgur.com/Z9QYYVS.png"
          - "https://i.imgur.com/fcPQihL.png"
          - "https://i.imgur.com/MBwhi2g.png"
          - "https://i.imgur.com/Ifb0KrA.png"
          - "https://i.imgur.com/Q5yD8v3.png"
          - "https://i.imgur.com/2d3PW7t.png"
          - "https://i.imgur.com/WaCN1nK.png"
          - "https://i.imgur.com/RIVLnFH.png"
          - "https://i.imgur.com/tS9Ty35.png"
          - "https://i.imgur.com/wZGG0SC.png"
          - "https://i.imgur.com/bvJN1Qf.png"
videos:
    skinship:
    author:
---
