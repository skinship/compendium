---
layout: skin

skin_name: "Sangonomiya Kokomi"
forum_thread_id: 1445600
date_added: 2021-11-05
game_modes:
    - standard
author:
    - 18854672
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "genshin_impact"
    - "sangonomiya_kokomi"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "Sangonomiya Kokomi"
      screenshots:
          - "https://i.imgur.com/6hpucZf.jpeg"
          - "https://i.imgur.com/oMSVnga.jpeg"
          - "https://i.imgur.com/TaE9Xx3.jpeg"
          - "https://i.imgur.com/wCuH3KE.jpeg"
          - "https://i.imgur.com/Ih19bvh.jpeg"
          - "https://i.imgur.com/L26Uhve.jpeg"
          - "https://i.imgur.com/vhod0ec.jpeg"
          - "https://i.imgur.com/vQp18DI.jpeg"
          - "https://i.imgur.com/3tbhfUB.jpeg"
          - "https://i.imgur.com/V3NHKZl.jpeg"
          - "https://i.imgur.com/iohOwa7.jpeg"
          - "https://i.imgur.com/lXZuDx0.jpeg"
          - "https://i.imgur.com/vqnD1Wd.jpeg"
          - "https://i.imgur.com/Arj6PBz.jpeg"
          - "https://i.imgur.com/09ciieN.jpeg"
          - "https://i.imgur.com/Gb8HFCC.jpeg"
videos:
    skinship:
    author:
---
