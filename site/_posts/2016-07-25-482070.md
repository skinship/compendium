---
layout: skin

skin_name: "OscilloSCAPE – Sci-Fi Skin"
forum_thread_id: 482070
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 616511
resolutions:
ratios:
tags:
categories:
    - eyecandy
skin_collection:
    - name: "OscilloSCAPE – Sci-Fi Skin"
      screenshots:
          - "http://i.imgur.com/BsmTkZN.jpg"
          - "http://i.imgur.com/I8GKJ7g.jpg"
          - "http://i.imgur.com/JIjo91d.png"
          - "http://i.imgur.com/iEVt9Eg.png"
          - "http://i.imgur.com/aJxQ53h.png"
          - "http://i.imgur.com/2QIvVsh.png"
          - "http://i.imgur.com/3xp4jNS.png"
          - "http://i.imgur.com/DclqKkf.png"
          - "http://i.imgur.com/W8BUBs8.jpg"
          - "http://i.imgur.com/u6kaVIl.png"
          - "http://i.imgur.com/0bfQ8SY.jpg"
videos:
    skinship:
    author:
---
