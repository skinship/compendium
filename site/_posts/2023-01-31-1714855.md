---
layout: skin

skin_name: "Simply Mania"
forum_thread_id: 1714855
date_added: 2023-12-23
game_modes:
    - mania
author:
    - 8023488
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "stepmania"
    - "4k"
categories:
    - minimalistic
skin_collection:
    - name: "Simply Mania"
      screenshots:
          - "https://i.imgur.com/02FQofz.png"
          - "https://i.imgur.com/gk1iQ0x.png"
          - "https://i.imgur.com/R5psos0.png"
          - "https://i.imgur.com/er7enni.png"
          - "https://i.imgur.com/hfYHJ00.png"
          - "https://i.imgur.com/MaMA1M5.png"
          - "https://i.imgur.com/jwwed3T.png"
          - "https://i.imgur.com/c6vzvao.png"
          - "https://i.imgur.com/L2v9a9I.png"
          - "https://i.imgur.com/iNrByVz.png"
          - "https://i.imgur.com/pL5hFAp.png"
          - "https://i.imgur.com/GbwVIkG.png"
    - name: "Orange - Purple - Purple - Orange"
      screenshots:
          - "https://i.imgur.com/wOU2FeR.png"
          - "https://i.imgur.com/7ajAlpE.png"
          - "https://i.imgur.com/HL1oWyx.png"
    - name: "Green - Cyan - Green - Cyan"
      screenshots:
          - "https://i.imgur.com/JeZSWLk.png"
          - "https://i.imgur.com/ozlAeCn.png"
          - "https://i.imgur.com/Si7IBqH.png"
    - name: "Red - Orange - Green - Blue"
      screenshots:
          - "https://i.imgur.com/x9S7uW1.png"
          - "https://i.imgur.com/JYzhAwE.png"
          - "https://i.imgur.com/S65xW8w.png"
videos:
    skinship:
    author:
---
