---
layout: skin

skin_name: "氢/Hydrogen"
forum_thread_id: 1876344
date_added: 2025-01-24
game_modes:
    - standard
author:
    - 12936947
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - minimalistic
skin_collection:
    - name: "氢/Hydrogen"
      screenshots:
          - "https://i.imgur.com/F5iRWrw.jpeg"
          - "https://i.imgur.com/Wy6Isda.png"
          - "https://i.imgur.com/qewAZMX.png"
          - "https://i.imgur.com/oB4Ae4f.png"
          - "https://i.imgur.com/jrwdgOD.png"
          - "https://i.imgur.com/WwMnS0o.png"
          - "https://i.imgur.com/r76D2R8.png"
          - "https://i.imgur.com/qebb1oh.png"
          - "https://i.imgur.com/sl7dcNb.png"
          - "https://i.imgur.com/1OTUStM.png"
          - "https://i.imgur.com/EbFSpek.png"
videos:
    skinship:
    author:
---
