---
layout: skin

skin_name: "[VISIONS]"
forum_thread_id: 1925961
date_added: 2024-06-20
game_modes:
    - standard
    - catch
author:
    - 13799581
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_5_submission"
categories:
    - minimalistic
    - eyecandy
skin_collection:
    - name: "[VISIONS]"
      screenshots:
          - "https://i.imgur.com/Vic1Xlr.png"
          - "https://i.imgur.com/q0BK8W2.png"
          - "https://i.imgur.com/KORWoVX.png"
          - "https://i.imgur.com/o6jHhTz.png"
          - "https://i.imgur.com/LQrRNUl.png"
          - "https://i.imgur.com/rddsDfa.png"
          - "https://i.imgur.com/OcJzhJO.png"
          - "https://i.imgur.com/o6jHhTz.png"
          - "https://i.imgur.com/n2EDPBy.png"
          - "https://i.imgur.com/StdMn6g.png"
          - "https://i.imgur.com/h0EPcNh.png"
          - "https://i.imgur.com/wuXNwlv.png"
          - "https://i.imgur.com/a5XAlTE.png"
          - "https://i.imgur.com/4iSF71A.png"
          - "https://i.imgur.com/afANAJg.png"
videos:
    skinship:
        - KXl0G1Tt6Ww
    author:
        - Ok3kvkJM9oU
---
