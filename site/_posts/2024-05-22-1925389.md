---
layout: skin

skin_name: "I REFUSE TO LOSE"
forum_thread_id: 1925389
date_added: 2024-06-20
game_modes:
    - standard
    - mania
author:
    - 19795875
resolutions:
    - hd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "contest_5_submission"
categories:
    - minimalistic
skin_collection:
    - name: "I REFUSE TO LOSE"
      screenshots:
          - "https://i.imgur.com/5NYyfTp.png"
          - "https://i.imgur.com/0vm67KZ.png"
          - "https://i.imgur.com/L3ZNfgd.png"
          - "https://i.imgur.com/pGapwkd.png"
          - "https://i.imgur.com/i4tZjle.png"
          - "https://i.imgur.com/1AeCmPE.png"
          - "https://i.imgur.com/oDvLgfe.png"
videos:
    skinship:
        - 3ltvXvdZEVQ
    author:
---
