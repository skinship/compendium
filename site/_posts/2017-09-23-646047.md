---
layout: skin

skin_name: "Sunset Horizons"
forum_thread_id: 646047
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 3344355
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - anime
skin_collection:
    - name: "Sunset Horizons"
      screenshots:
          - "https://i.imgur.com/M0QiMsl.png"
          - "https://i.imgur.com/xIEycAt.png"
          - "https://i.imgur.com/iz5KKrj.png"
          - "https://i.imgur.com/JcLY5Ah.png"
          - "https://i.imgur.com/3SYffGI.png"
          - "https://i.imgur.com/18w1fcw.png"
          - "https://i.imgur.com/2Z9VG8n.png"
          - "https://i.imgur.com/UvkrgIu.png"
          - "https://i.imgur.com/MydN2Tm.jpg"
videos:
    skinship:
    author:
---
