---
layout: skin

skin_name: "MY WORLD"
forum_thread_id: 1909545
date_added: 2024-06-20
game_modes:
    - standard
    - catch
author:
    - 16085671
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "contest_5_submission"
categories:
    - other
skin_collection:
    - name: "MY WORLD"
      screenshots:
          - "https://i.imgur.com/wia8wRH.jpg"
          - "https://i.imgur.com/8M73E2W.jpg"
          - "https://i.imgur.com/zANcoq6.jpg"
          - "https://i.imgur.com/PmXQB7a.jpg"
          - "https://i.imgur.com/n8Qs3wT.jpg"
          - "https://i.imgur.com/XL19Tkg.jpg"
          - "https://i.imgur.com/xZL8JeJ.jpg"
          - "https://i.imgur.com/0NAJTlL.jpg"
videos:
    skinship:
        - PTbOogQoy7Y
    author:
        - fd_jFbbuMN4
---
