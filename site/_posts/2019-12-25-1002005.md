---
layout: skin

skin_name: "h3oSkin"
forum_thread_id: 1002005
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 8712289
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "kagamine_rin"
    - "vocaloid"
categories:
    - anime
skin_collection:
    - name: "h3oSkin"
      screenshots:
          - "https://i.imgur.com/hCirUrB.png"
          - "https://i.imgur.com/A7KDVoP.png"
          - "https://i.imgur.com/sU0gWpx.png"
          - "https://i.imgur.com/G5faZbx.png"
          - "https://i.imgur.com/4AUxfii.png"
          - "https://i.imgur.com/lwd3Dhq.png"
          - "https://i.imgur.com/8E3O4Vl.png"
          - "https://i.imgur.com/VrW6hW4.png"
          - "https://i.imgur.com/ud7JLtt.png"
          - "https://i.imgur.com/Lxn4udu.png"
          - "https://i.imgur.com/bym8awA.png"
          - "https://i.imgur.com/pa9WIW8.png"
          - "https://i.imgur.com/v4ut6O1.png"
          - "https://i.imgur.com/j2WQEhK.png"
          - "https://i.imgur.com/9qslxqH.png"
          - "https://i.imgur.com/VXUzUF7.png"
          - "https://i.imgur.com/wyK03In.png"
          - "https://i.imgur.com/MEpO9kQ.png"
          - "https://i.imgur.com/faFwj1h.png"
          - "https://i.imgur.com/sF6U0cD.png"
          - "https://i.imgur.com/LS7yME4.png"
          - "https://i.imgur.com/nyCMdx9.png"
          - "https://i.imgur.com/T69SUc1.png"
          - "https://i.imgur.com/Azn8vUX.png"
          - "https://i.imgur.com/1BocAMZ.png"
          - "https://i.imgur.com/dqVOpRB.png"
          - "https://i.imgur.com/XycFgeA.png"
          - "https://i.imgur.com/pvcGuaV.png"
          - "https://i.imgur.com/RHzhOvv.png"
          - "https://i.imgur.com/cs7WNB2.png"
          - "https://i.imgur.com/FhJw7cn.png"
          - "https://i.imgur.com/0t7DdQQ.png"
          - "https://i.imgur.com/hlPnQox.png"
videos:
    skinship:
    author:
---
