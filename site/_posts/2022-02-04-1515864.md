---
layout: skin

skin_name: "Catgirl Oziko v2"
forum_thread_id: 1515864
date_added: 2022-03-20
game_modes:
    - standard
author:
    - 12091015
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "oziko"
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "Catgirl Oziko v2"
      screenshots:
          - "https://i.imgur.com/fWuIHkB.png"
          - "https://i.imgur.com/nurZnrg.png"
          - "https://i.imgur.com/7Uwbre7.png"
          - "https://i.imgur.com/1vFropi.png"
          - "https://i.imgur.com/5VxLVxj.png"
          - "https://i.imgur.com/glMD1H4.png"
          - "https://i.imgur.com/sAJdl9a.png"
          - "https://i.imgur.com/C0HWmkX.png"
          - "https://i.imgur.com/iVkXSIz.png"
          - "https://i.imgur.com/vgUWBeO.png"
videos:
    skinship:
    author:
---
