---
layout: skin

skin_name: "Experiment 82"
forum_thread_id: 253908
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 1722000
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Experiment 82"
      screenshots:
          - "http://i.imgur.com/59FOXa1.jpg"
          - "http://i.imgur.com/rT5Unye.png"
          - "http://i.imgur.com/vqBRVkz.png"
          - "http://i.imgur.com/lhQ5lWw.png"
          - "http://i.imgur.com/19Jcz5B.png"
videos:
    skinship:
    author:
---
