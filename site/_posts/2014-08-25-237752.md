---
layout: skin

skin_name: "Chitoge Style"
forum_thread_id: 237752
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 3211392
resolutions:
    - hd
ratios:
tags:
    - "nisekoi:_false_love"
    - "chitoge_kirisaki"
categories:
    - anime
skin_collection:
    - name: "Chitoge Style"
      screenshots:
          - "http://i.imgur.com/XL474Kh.jpg"
          - "http://i.imgur.com/CjYxtw9.jpg"
          - "http://i.imgur.com/9X6nonU.png"
          - "http://i.imgur.com/mGC8fpE.jpg"
          - "http://i.imgur.com/wvcFWFT.png"
videos:
    skinship:
    author:
---
