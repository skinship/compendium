---
layout: skin

skin_name: "Maruyama Aya"
forum_thread_id: 906743
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 1125647
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "bandori"
    - "bang_dream!"
    - "maruyama_aya"
    - "pastel*palettes"
    - "pasupare"
categories:
    - anime
    - game
skin_collection:
    - name: "Maruyama Aya"
      screenshots:
          - "https://i.imgur.com/xYW0I8N.jpg"
          - "https://i.imgur.com/HUsEAav.png"
          - "https://i.imgur.com/s5SGSVE.png"
          - "https://i.imgur.com/E5VcdYW.jpg"
          - "https://i.imgur.com/vRAs4ep.jpg"
          - "https://i.imgur.com/v9Flyu2.png"
          - "https://i.imgur.com/48vUiqc.jpg"
          - "https://i.imgur.com/Tjvv3G1.jpg"
          - "https://i.imgur.com/zjnUTQi.jpg"
          - "https://i.imgur.com/uJ0gxwy.jpg"
          - "https://i.imgur.com/hiMzhK6.jpg"
          - "https://i.imgur.com/WVqXIZE.jpg"
          - "https://i.imgur.com/csEuUtd.jpg"
videos:
    skinship:
    author:
---
