---
layout: skin

skin_name: "Cherry Blossoms ft. Yae Miko"
forum_thread_id: 1776432
date_added: 2024-01-09
game_modes:
    - standard
    - mania
    - taiko
author:
    - 10506141
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "genshin_impact"
    - "yae_miko"
categories:
    - anime
    - minimalistic
    - game
skin_collection:
    - name: "Cherry Blossoms ft. Yae Miko"
      screenshots:
          - "https://i.imgur.com/UP1yjn8.png"
          - "https://i.imgur.com/xy58PtP.png"
          - "https://i.imgur.com/C4Qrc0a.png"
          - "https://i.imgur.com/NnPaGk6.png"
          - "https://i.imgur.com/FntFA10.png"
          - "https://i.imgur.com/L0Wu0zF.png"
          - "https://i.imgur.com/Lz07nGK.png"
          - "https://i.imgur.com/j4ne1GN.png"
          - "https://i.imgur.com/5kiMwQu.png"
          - "https://i.imgur.com/sTowWYV.png"
          - "https://i.imgur.com/kxYAbFE.png"
          - "https://i.imgur.com/vQ6TgGU.png"
videos:
    skinship:
    author:
---
