---
layout: skin

skin_name: "Xi-Style G2"
forum_thread_id: 59787
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
author:
    - 17894
resolutions:
ratios:
tags:
categories:
    - other
skin_collection:
    - name: "Xi-Style G2"
      screenshots:
          - "http://i.imgur.com/nqoIXe7.jpg"
          - "http://i.imgur.com/Nheh5RD.jpg"
          - "http://i.imgur.com/JjwhcNY.png"
          - "http://i.imgur.com/WLFFMUn.png"
          - "http://i.imgur.com/4Emwuyf.png"
          - "http://i.imgur.com/xWSFUc7.png"
          - "http://i.imgur.com/6HNKX6W.png"
videos:
    skinship:
    author:
---
