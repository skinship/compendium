---
layout: skin

skin_name: "Kiruya Momochi"
forum_thread_id: 1104598
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 15211207
resolutions:
    - sd
ratios:
    - "16:9"
tags:
    - "kiruya_momochi"
    - "princess_connect!_re:dive"
categories:
    - anime
skin_collection:
    - name: "Kiruya Momochi"
      screenshots:
          - "https://i.ibb.co/jr52K3w/Capture.png"
          - "https://i.ibb.co/wNR0Dg0/Captur2e.png"
          - "https://i.ibb.co/M6NYXxq/Captur7e.png"
          - "https://i.ibb.co/tZt7wCw/Captur4e.png"
          - "https://i.ibb.co/0qwFLKS/Capture8.png"
          - "https://i.ibb.co/HrVfV8L/Capture6.png"
          - "https://i.ibb.co/2SGmyfP/Captur5e.png"
          - "https://i.ibb.co/W0fm4NX/Captur3e.png"
videos:
    skinship:
    author:
---
