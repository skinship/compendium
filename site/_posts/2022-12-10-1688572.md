---
layout: skin

skin_name: "whiteave"
forum_thread_id: 1688572
date_added: 2022-12-13
game_modes:
    - standard
author:
    - 6785191
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "whiteave"
      screenshots:
          - "https://i.imgur.com/cdV4cU9.png"
          - "https://i.imgur.com/dh0uAW8.png"
          - "https://i.imgur.com/BCILIPj.png"
          - "https://i.imgur.com/Ac1VkV9.png"
          - "https://i.imgur.com/QASjz4p.png"
          - "https://i.imgur.com/BaUJMAO.png"
          - "https://i.imgur.com/Rkb0Scm.png"
          - "https://i.imgur.com/B3EJPjG.png"
          - "https://i.imgur.com/WD29dNb.png"
          - "https://i.imgur.com/MgZubvJ.png"
          - "https://i.imgur.com/MuZttMP.png"
          - "https://i.imgur.com/NPYREHL.png"
          - "https://i.imgur.com/FZTFdjl.png"
          - "https://i.imgur.com/n79Pfab.png"
          - "https://i.imgur.com/8IBq6TZ.png"
videos:
    skinship:
    author:
---
