---
layout: skin

skin_name: "re:Shigetora"
forum_thread_id: 486160
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
    - catch
author:
    - 6286572
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "re:Shigetora"
      screenshots:
          - "http://i.imgur.com/7FxzHe1.jpg"
          - "http://i.imgur.com/YXtMFmE.png"
          - "http://i.imgur.com/9Xn13QJ.jpg"
          - "http://i.imgur.com/GTBGUNV.png"
          - "http://i.imgur.com/gmfLLua.jpg"
          - "http://i.imgur.com/KQHOLT4.png"
          - "http://i.imgur.com/507UDN7.png"
videos:
    skinship:
    author:
---
