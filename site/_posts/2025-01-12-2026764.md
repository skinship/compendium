---
layout: skin

skin_name: "Gradients"
forum_thread_id: 2026764
date_added: 2025-01-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 30298378
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "rainbow"
    - "1k"
    - "2k"
    - "3k"
    - "4k"
    - "5k"
    - "6k"
    - "7k"
    - "8k"
    - "9k"
    - "10k"
    - "12k"
    - "14k"
    - "16k"
    - "18k"
categories:
    - minimalistic
skin_collection:
    - name: "Gradients"
      screenshots:
          - "https://i.imgur.com/kM0RFWe.jpeg"
          - "https://i.imgur.com/DU0hU7W.jpeg"
          - "https://i.imgur.com/Cq2wWzy.jpeg"
          - "https://i.imgur.com/7ENxRkh.jpeg"
          - "https://i.imgur.com/3ZKnjh6.jpeg"
          - "https://i.imgur.com/DFfXd0f.jpeg"
          - "https://i.imgur.com/8mAx3Q5.jpeg"
          - "https://i.imgur.com/9U6AKEc.jpeg"
          - "https://i.imgur.com/FmrqGtC.jpeg"
          - "https://i.imgur.com/aHo7gCu.jpeg"
          - "https://i.imgur.com/lZuZbXE.jpeg"
          - "https://i.imgur.com/rRzuNYU.jpeg"
          - "https://i.imgur.com/10eKwJx.jpeg"
          - "https://i.imgur.com/kv91seP.jpeg"
          - "https://i.imgur.com/rSbHCfu.jpeg"
          - "https://i.imgur.com/Vw4gTQA.jpeg"
          - "https://i.imgur.com/sDwgFxN.jpeg"
          - "https://i.imgur.com/XBzZZ3m.jpeg"
          - "https://i.imgur.com/xgUAYPs.jpeg"
          - "https://i.imgur.com/wcFkv0E.jpeg"
          - "https://i.imgur.com/9hHpLIW.jpeg"
          - "https://i.imgur.com/p3upa9d.jpeg"
          - "https://i.imgur.com/XzxOZON.jpeg"
          - "https://i.imgur.com/KuvTbqW.jpeg"
          - "https://i.imgur.com/wX30Wl9.jpeg"
          - "https://i.imgur.com/1TCMIlu.jpeg"
          - "https://i.imgur.com/CFPFUqh.jpeg"
          - "https://i.imgur.com/X8aOIFv.jpeg"
          - "https://i.imgur.com/JBuONXk.jpeg"
          - "https://i.imgur.com/EjWnclL.jpeg"
          - "https://i.imgur.com/ri3pQCz.jpeg"
          - "https://i.imgur.com/Afv7wPF.jpeg"
          - "https://i.imgur.com/4Uonohb.jpeg"
          - "https://i.imgur.com/8B8WZMn.jpeg"
          - "https://i.imgur.com/M79PYoL.jpeg"
          - "https://i.imgur.com/ATUX5Jd.jpeg"
          - "https://i.imgur.com/fvtAXd5.jpeg"
          - "https://i.imgur.com/IxW0P5j.jpeg"
          - "https://i.imgur.com/G1I0q5C.jpeg"
          - "https://i.imgur.com/Za5kGK9.jpeg"
          - "https://i.imgur.com/SkZCD1D.jpeg"
          - "https://i.imgur.com/SwIWP6L.jpeg"
          - "https://i.imgur.com/9Q6y49S.jpeg"
          - "https://i.imgur.com/H3qxWIS.jpeg"
          - "https://i.imgur.com/T6iF7op.jpeg"
          - "https://i.imgur.com/sH07XJ9.jpeg"
          - "https://i.imgur.com/pSolg7a.jpeg"
          - "https://i.imgur.com/SmGttCJ.jpeg"
          - "https://i.imgur.com/Ag4Doh4.jpeg"
          - "https://i.imgur.com/aQVgC0U.jpeg"
videos:
    skinship:
    author:
---
