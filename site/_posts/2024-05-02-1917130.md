---
layout: skin

skin_name: "CosySan キノココレクター"
forum_thread_id: 1917130
date_added: 2024-05-16
game_modes:
    - standard
author:
    - 17561095
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - anime
    - minimalistic
skin_collection:
    - name: "CosySan キノココレクター"
      screenshots:
          - "https://i.imgur.com/Y1moAox.png"
          - "https://i.imgur.com/1DRSq43.png"
          - "https://i.imgur.com/TmVXHs4.jpg"
          - "https://i.imgur.com/HLiT9l5.jpg"
          - "https://i.imgur.com/0TItsvM.jpg"
          - "https://i.imgur.com/kh67iuK.png"
          - "https://i.imgur.com/8ft0wpM.png"
          - "https://i.imgur.com/6I9B5Bn.png"
          - "https://i.imgur.com/H0SNd59.png"
          - "https://i.imgur.com/xLM292H.png"
          - "https://i.imgur.com/45FTmXy.png"
          - "https://i.imgur.com/vHQoJNR.png"
          - "https://i.imgur.com/EWlq4qw.png"
videos:
    skinship:
    author:
        - HPcuEhi6goc
---
