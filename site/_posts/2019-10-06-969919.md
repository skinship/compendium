---
layout: skin

skin_name: "Akai Iota"
forum_thread_id: 969919
date_added: 2021-06-28
game_modes:
    - standard
author:
    - 10411839
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "002"
    - "ditf"
    - "darifura"
    - "darling_in_the_franxx"
    - "zero_two"
categories:
    - anime
skin_collection:
    - name: "Akai Iota"
      screenshots:
          - "https://i.imgur.com/5FBb3HJ.png"
          - "https://i.imgur.com/yNaWE9J.png"
          - "https://i.imgur.com/FSfEskR.png"
          - "https://i.imgur.com/uRFlawQ.png"
          - "https://i.imgur.com/0iuCbBl.png"
          - "https://i.imgur.com/ZaDq5Q2.png"
          - "https://i.imgur.com/wP1Bk3r.png"
          - "https://i.imgur.com/XrCsKwk.png"
          - "https://i.imgur.com/NEbbRKF.png"
          - "https://i.imgur.com/S2vow5t.png"
          - "https://i.imgur.com/FhCqs68.png"
          - "https://i.imgur.com/tGCOsMN.png"
          - "https://i.imgur.com/y4YaOjY.png"
videos:
    skinship:
    author:
---
