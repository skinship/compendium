---
layout: skin

skin_name: "New Game!"
forum_thread_id: 632640
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 4753647
resolutions:
    - hd
    - sd
ratios:
tags:
    - "new_game!"
categories:
    - anime
skin_collection:
    - name: "New Game!"
      screenshots:
          - "https://i.imgur.com/QxuYcob.jpg"
          - "https://i.imgur.com/Bvnv0wl.jpg"
          - "https://i.imgur.com/1pMvdTC.jpg"
          - "https://i.imgur.com/EKR86YN.jpg"
          - "https://puu.sh/xftcL/b992dd9435.jpg"
          - "https://puu.sh/xftk2/ec3004c56e.jpg"
          - "https://i.imgur.com/7upsEmG.jpg"
          - "https://i.imgur.com/6yTjhAX.jpg"
          - "https://i.imgur.com/4mbMxTv.jpg"
          - "https://i.imgur.com/n1bSeeK.jpg"
videos:
    skinship:
    author:
---
