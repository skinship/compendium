---
layout: skin

skin_name: "NEON"
forum_thread_id: 606812
date_added: 2021-07-18
game_modes:
    - standard
author:
    - 3250626
resolutions:
    - hd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "NEON"
      screenshots:
          - "http://i.imgur.com/qi18kHm.png"
          - "http://i.imgur.com/K8k6QjT.png"
          - "http://i.imgur.com/PnNo3ao.png"
          - "http://i.imgur.com/8cDFp8B.png"
          - "http://i.imgur.com/34nTfDI.png"
          - "http://i.imgur.com/m1KaSFC.png"
          - "http://i.imgur.com/wQWWYbY.png"
videos:
    skinship:
    author:
---
