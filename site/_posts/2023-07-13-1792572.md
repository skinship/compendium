---
layout: skin

skin_name: "Verflucht"
forum_thread_id: 1792572
date_added: 2023-07-20
game_modes:
    - standard
    - catch
author:
    - 20149300
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "contest_submission"
    - "beatmap"
    - "contest_4_submission"
    - "beatmania_iidx_21_spada"
    - "verflucht"
categories:
    - minimalistic
    - game
skin_collection:
    - name: "Verflucht"
      screenshots:
          - "https://i.imgur.com/aKI4Qs5.png"
          - "https://i.imgur.com/Dd3bGOD.png"
          - "https://i.imgur.com/A23b5tw.jpg"
          - "https://i.imgur.com/AWFTed2.jpg"
          - "https://i.imgur.com/8qACB4j.png"
          - "https://i.imgur.com/Z6unqlQ.png"
          - "https://i.imgur.com/Ez95up2.png"
          - "https://i.imgur.com/m1QkdEm.png"
          - "https://i.imgur.com/0Vco3iR.png"
          - "https://i.imgur.com/xHiLAvR.png"
          - "https://i.imgur.com/eJhXRvT.png"
          - "https://i.imgur.com/mQPRAF4.png"
          - "https://i.imgur.com/jgm1Yqd.png"
          - "https://i.imgur.com/WcgzpMZ.png"
videos:
    skinship:
        - ImsTUzLzh4k
    author:
---
