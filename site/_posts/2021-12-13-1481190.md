---
layout: skin

skin_name: "CACTUS'"
forum_thread_id: 1481190
date_added: 2022-01-13
game_modes:
    - standard
    - catch
author:
    - 2168518
resolutions:
    - hd
    - sd
ratios:
    - "4:3"
    - "16:9"
tags:
    - "contest_submission"
    - "contest_2_submission"
    - "winter"
categories:
    - eyecandy
skin_collection:
    - name: "CACTUS'"
      screenshots:
          - "https://i.imgur.com/6pLbFp1.jpeg"
          - "https://i.imgur.com/TO0dSph.jpeg"
          - "https://i.imgur.com/Bxz1ull.jpeg"
          - "https://i.imgur.com/Nv9Q7gB.jpeg"
          - "https://i.imgur.com/oiMT3Iv.jpeg"
          - "https://i.imgur.com/nMacWuo.jpeg"
          - "https://i.imgur.com/dufqhHR.jpeg"
          - "https://i.imgur.com/NRKCC1w.jpeg"
          - "https://i.imgur.com/i8e846T.jpeg"
          - "https://i.imgur.com/J9timKQ.jpeg"
          - "https://i.imgur.com/yLLXiDR.jpeg"
          - "https://i.imgur.com/yJAfiVD.jpeg"
          - "https://i.imgur.com/HKLZHqy.jpeg"
videos:
    skinship:
        - EfLyoFl-1QY
    author:
---
