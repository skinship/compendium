---
layout: skin

skin_name: "Shigatsu (Your Lie in April)"
forum_thread_id: 602376
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
author:
    - 4956097
resolutions:
ratios:
tags:
    - "shigatsu_wa_kimi_no_uso"
    - "your_lie_in_april"
categories:
    - anime
skin_collection:
    - name: "Shigatsu (Your Lie in April)"
      screenshots:
          - "http://i.imgur.com/v26ZuzR.jpg"
          - "http://i.imgur.com/nVYf3nc.jpg"
          - "http://i.imgur.com/wChdy9n.jpg"
          - "http://i.imgur.com/JtDVp8F.jpg"
          - "http://i.imgur.com/HAbNJ0O.jpg"
          - "http://i.imgur.com/nfHsEPF.jpg"
          - "http://i.imgur.com/vb9tpJn.png"
          - "http://i.imgur.com/Qx8pmr5.png"
          - "http://i.imgur.com/C8wsQ3j.jpg"
          - "http://i.imgur.com/yn6ENrR.jpg"
videos:
    skinship:
    author:
---
