---
layout: skin

skin_name: "Clear Skin Ultra 2.4"
forum_thread_id: 300001
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 5212789
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Clear Skin Ultra 2.4"
      screenshots:
          - "http://i.imgur.com/DXANTZE.jpg"
          - "http://i.imgur.com/GqpdQiO.jpg"
          - "http://i.imgur.com/uzRb6G5.png"
          - "http://i.imgur.com/z12P7iV.png"
          - "http://i.imgur.com/UHLObJx.png"
videos:
    skinship:
    author:
---
