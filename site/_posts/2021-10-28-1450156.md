---
layout: skin

skin_name: "up%down - LOONA"
forum_thread_id: 1450156
date_added: 2023-01-07
game_modes:
    - standard
author:
    - 2168518
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "loona"
    - "code_geass"
    - "skinners'_bulletin_showcase"
    - "music"
    - "kpop"
    - "heejin"
    - "hyunjin"
    - "vivi"
    - "haseul"
    - "yeojin"
    - "kim_lip"
    - "jinsoul"
    - "choerry"
    - "yves"
    - "chuu"
    - "go_won"
    - "olivia_hye"
    - "c.c."
categories:
    - anime
    - eyecandy
skin_collection:
    - name: "HEEJIN"
      screenshots:
          - "https://i.imgur.com/uZD8ZXf.png"
          - "https://i.imgur.com/bgjggMJ.png"
          - "https://i.imgur.com/a37CP7M.png"
          - "https://i.imgur.com/OpRCwB2.png"
          - "https://i.imgur.com/b54i7FW.png"
          - "https://i.imgur.com/XNyJoOk.png"
          - "https://i.imgur.com/omLuZ2I.png"
          - "https://i.imgur.com/267cS5T.png"
          - "https://i.imgur.com/kxFqUmD.png"
    - name: "HYUNJIN"
      screenshots:
          - "https://i.imgur.com/YwdryJH.png"
          - "https://i.imgur.com/QUS8FML.png"
          - "https://i.imgur.com/QZnodl9.png"
          - "https://i.imgur.com/5feN2O1.png"
          - "https://i.imgur.com/cAU1WMx.png"
          - "https://i.imgur.com/1FfM1Jf.png"
          - "https://i.imgur.com/ncx3vah.png"
          - "https://i.imgur.com/TflkrpU.png"
    - name: "HASEUL"
      screenshots:
          - "https://i.imgur.com/Ug1popO.png"
          - "https://i.imgur.com/CvEu9Tg.png"
          - "https://i.imgur.com/E2Yq69z.png"
          - "https://i.imgur.com/9hH73hk.png"
          - "https://i.imgur.com/WzJ4w6l.png"
          - "https://i.imgur.com/zihfSlG.png"
          - "https://i.imgur.com/EqTYBH5.png"
          - "https://i.imgur.com/Datpb1D.png"
    - name: "YEOJIN"
      screenshots:
          - "https://i.imgur.com/DHErExG.png"
          - "https://i.imgur.com/flq37YC.png"
          - "https://i.imgur.com/dbQkNtU.png"
          - "https://i.imgur.com/lXFDgHh.png"
          - "https://i.imgur.com/G47AmCI.png"
          - "https://i.imgur.com/rSXLBfI.png"
    - name: "VIVI"
      screenshots:
          - "https://i.imgur.com/5uummbE.png"
          - "https://i.imgur.com/2nTkuQz.png"
          - "https://i.imgur.com/ROXwQRH.png"
          - "https://i.imgur.com/Oirbl2y.png"
          - "https://i.imgur.com/DZZUmFS.png"
          - "https://i.imgur.com/DiIAjIt.png"
          - "https://i.imgur.com/XE407sy.png"
    - name: "KIM LIP"
      screenshots:
          - "https://i.imgur.com/rDTnHDl.png"
          - "https://i.imgur.com/l0z95fK.png"
          - "https://i.imgur.com/tgm86DR.png"
          - "https://i.imgur.com/pmBfakS.png"
          - "https://i.imgur.com/eyg4hbp.png"
          - "https://i.imgur.com/YTT0pSu.png"
          - "https://i.imgur.com/g4DGxDl.png"
    - name: "JINSOUL"
      screenshots:
          - "https://i.imgur.com/7PvWBaN.png"
          - "https://i.imgur.com/0lRbn9l.png"
          - "https://i.imgur.com/SYyJ0Uw.png"
          - "https://i.imgur.com/CmBwy56.png"
          - "https://i.imgur.com/2P61vEd.png"
    - name: "CHOERRY"
      screenshots:
          - "https://i.imgur.com/uQ6Q5P0.png"
          - "https://i.imgur.com/yZ6KS13.png"
          - "https://i.imgur.com/IxKVKjS.png"
          - "https://i.imgur.com/7uhU07X.png"
          - "https://i.imgur.com/OrQSwnu.png"
          - "https://i.imgur.com/tNAZDcZ.png"
          - "https://i.imgur.com/2uANFub.png"
    - name: "YVES"
      screenshots:
          - "https://i.imgur.com/dUxKl4M.png"
          - "https://i.imgur.com/1pfVJlE.png"
          - "https://i.imgur.com/m83tJZv.png"
          - "https://i.imgur.com/RgesIBo.png"
          - "https://i.imgur.com/z0TiFsj.png"
          - "https://i.imgur.com/x1C21ZF.png"
          - "https://i.imgur.com/nWb8fPr.png"
    - name: "CHUU"
      screenshots:
          - "https://i.imgur.com/US4u2WF.png"
          - "https://i.imgur.com/dLL4suf.png"
          - "https://i.imgur.com/jRjmymO.png"
          - "https://i.imgur.com/vTf5l6a.png"
          - "https://i.imgur.com/7L0qi06.png"
          - "https://i.imgur.com/ou50OBe.png"
    - name: "GO WON"
      screenshots:
          - "https://i.imgur.com/RC3MkoF.png"
          - "https://i.imgur.com/YghqtJS.png"
          - "https://i.imgur.com/Y8CsNFy.png"
          - "https://i.imgur.com/DfQ6Sva.png"
          - "https://i.imgur.com/9CZvn2r.png"
          - "https://i.imgur.com/jRnzvwk.png"
    - name: "OLIVIA HYE"
      screenshots:
          - "https://i.imgur.com/J2FqYsw.png"
          - "https://i.imgur.com/h1kK1cM.png"
          - "https://i.imgur.com/aEbOddu.png"
          - "https://i.imgur.com/BnAQtr9.png"
          - "https://i.imgur.com/ylKTiV0.png"
          - "https://i.imgur.com/HdFKxla.png"
          - "https://i.imgur.com/5VC2iVH.png"
    - name: "C.C."
      screenshots:
          - "https://i.imgur.com/DSwfCyy.png"
          - "https://i.imgur.com/ctMtfNi.png"
          - "https://i.imgur.com/OBRCYnK.png"
          - "https://i.imgur.com/20p7KjN.png"
          - "https://i.imgur.com/Xym00hp.png"
          - "https://i.imgur.com/Ln9QJa7.png"
          - "https://i.imgur.com/EGyq44F.png"
          - "https://i.imgur.com/8co4HUy.png"
          - "https://i.imgur.com/0FpImIM.png"
videos:
    skinship:
    author:
---
