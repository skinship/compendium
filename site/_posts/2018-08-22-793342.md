---
layout: skin

skin_name: "New Retro Wave"
forum_thread_id: 793342
date_added: 2021-06-24
game_modes:
    - standard
    - mania
    - taiko
    - catch
author:
    - 11370771
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
categories:
    - eyecandy
skin_collection:
    - name: "New Retro Wave"
      screenshots:
          - "https://i.imgur.com/XNCaiEh.png"
          - "https://i.imgur.com/Y8Ncf0a.png"
          - "https://i.imgur.com/MSuuTfe.png"
          - "https://i.imgur.com/soDEK6n.png"
          - "https://i.imgur.com/Nk9tkCp.png"
          - "http://i.imgur.com/wr9NLRW.jpg"
          - "http://i.imgur.com/XUyUQVa.jpg"
          - "http://i.imgur.com/agplQxW.jpg"
          - "http://i.imgur.com/NRntEH7.jpg"
videos:
    skinship:
    author:
---
