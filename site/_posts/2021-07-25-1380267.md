---
layout: skin

skin_name: "Kureiji Ollie"
forum_thread_id: 1380267
date_added: 2021-08-03
game_modes:
    - standard
author:
    - 16086912
resolutions:
    - hd
    - sd
ratios:
    - "16:9"
tags:
    - "hololive"
    - "kureiji_ollie"
    - "vtuber"
categories:
    - anime
skin_collection:
    - name: "Kureiji Ollie"
      screenshots:
          - "https://i.imgur.com/Im0CCqf.png"
          - "https://i.imgur.com/zYHL1On.png"
          - "https://i.imgur.com/N1jBKSB.png"
          - "https://i.imgur.com/8QyXJmU.png"
          - "https://i.imgur.com/Uv9sSYs.png"
          - "https://i.imgur.com/xriooQC.png"
          - "https://i.imgur.com/JcUDu2v.png"
          - "https://i.imgur.com/mm7hX4J.png"
          - "https://i.imgur.com/LQBurqr.png"
          - "https://i.imgur.com/qvfcFWm.png"
          - "https://i.imgur.com/AbJgWK9.png"
videos:
    skinship:
    author:
---
