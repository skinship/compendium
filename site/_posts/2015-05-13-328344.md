---
layout: skin

skin_name: "Chaosu 3 Freestyle"
forum_thread_id: 328344
date_added: 2021-06-24
game_modes:
    - standard
    - taiko
    - catch
author:
    - 3621552
resolutions:
    - hd
    - sd
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "Chaosu 3 Freestyle"
      screenshots:
          - "http://i.imgur.com/CpJDKrp.jpg"
          - "http://i.imgur.com/oaKBFcU.jpg"
          - "http://i.imgur.com/t2jhHIN.jpg"
          - "http://i.imgur.com/bnmtS5y.jpg"
          - "http://i.imgur.com/iDlIK9y.jpg"
          - "http://i.imgur.com/9LjkymZ.jpg"
          - "http://i.imgur.com/NObJiFQ.jpg"
          - "http://i.imgur.com/jywCvSD.jpg"
          - "http://i.imgur.com/ypKhzT2.jpg"
          - "http://i.imgur.com/MEaM4Dl.jpg"
          - "http://i.imgur.com/14Yvtxr.jpg"
          - "http://i.imgur.com/6wynBK3.jpg"
          - "http://i.imgur.com/tWhONRL.jpg"
          - "http://i.imgur.com/Q4dTiB1.jpg"
          - "http://i.imgur.com/9zwDVtU.jpg"
videos:
    skinship:
    author:
---
