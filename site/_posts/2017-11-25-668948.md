---
layout: skin

skin_name: "mint!"
forum_thread_id: 668948
date_added: 2021-06-24
game_modes:
    - standard
author:
    - 8194107
resolutions:
ratios:
tags:
categories:
    - minimalistic
skin_collection:
    - name: "mint!"
      screenshots:
          - "https://i.imgur.com/lpvOWYh.jpg"
          - "https://i.imgur.com/krBxplS.jpg"
          - "https://i.imgur.com/eClvdMM.jpg"
          - "https://i.imgur.com/zJmbRSY.png"
          - "https://i.imgur.com/LInFEFw.jpg"
          - "https://i.imgur.com/YLAkDXZ.jpg"
          - "https://i.imgur.com/9jT0B5h.png"
          - "https://i.imgur.com/vyjLnYE.png"
          - "https://i.imgur.com/PXCS4Wf.png"
          - "https://i.imgur.com/KswOPxg.png"
          - "https://i.imgur.com/uwYG3dd.jpg"
          - "https://i.imgur.com/ft1VQUM.jpg"
videos:
    skinship:
    author:
---
