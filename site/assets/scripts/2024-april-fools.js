'use strict';

/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function getSkinIndex(url = "/search.json") {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const response = yield fetch(url);
            if (response.status != 200) {
                throw new Error(`Search fetch failed: ${response.status}`);
            }
            return JSON.parse(yield response.text(), function (key, value) {
                if (["date_added", "date_released"].includes(key)) {
                    return new Date(value);
                }
                else if (["game_modes", "author", "resolutions", "ratios", "tags", "categories", "skin_collection"].includes(key)) {
                    if (value == null) {
                        return [];
                    }
                }
                return value;
            });
        }
        catch (e) {
            console.error(e.message);
        }
    });
}

const chevronLeft = `<svg class="icon chevron-left" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>`;
const chevronRight = `<svg class="icon chevron-right" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>`;
const cross = `<svg class="icon cross" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>`;
const fullscreen = `<svg class="icon fullscreen" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4"></path></svg>`;
const taiko = `<svg class="icon taiko" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 0C5.37686 0 0 5.37686 0 12C0 18.6232 5.37686 24 12 24C18.6231 24 24 18.6232 24 12C24.0178 5.37686 18.641 0 12 0ZM12 21.9526C6.51632 21.9526 2.04748 17.4837 2.04748 12C2.04748 6.51633 6.51632 2.04748 12 2.04748C17.4836 2.04748 21.9525 6.51633 21.9525 12C21.9525 17.5015 17.5015 21.9526 12 21.9526Z"></path><path d="M12.0003 4.34426C7.76293 4.34426 4.32672 7.78046 4.32672 12.0178C4.32672 16.2553 7.76293 19.6915 12.0003 19.6915C16.2377 19.6915 19.6739 16.2553 19.6739 12.0178C19.6739 7.76266 16.2377 4.34426 12.0003 4.34426ZM6.89052 12.0001C6.89052 9.61429 8.5285 7.62022 10.7362 7.05049V16.9496C8.5285 16.3798 6.89052 14.3858 6.89052 12.0001ZM13.2822 16.9496V7.05049C15.4899 7.62022 17.1101 9.61429 17.1101 12.0001C17.1101 14.3858 15.4899 16.3798 13.2822 16.9496Z"></path></svg>`;
const mania = `<svg class="icon mania" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 0C5.37686 0 0 5.37686 0 12C0 18.6232 5.37686 24 12 24C18.6231 24 24 18.6232 24 12C24.0178 5.37686 18.641 0 12 0ZM12 21.9526C6.51632 21.9526 2.04748 17.4837 2.04748 12C2.04748 6.51633 6.51632 2.04748 12 2.04748C17.4836 2.04748 21.9525 6.51633 21.9525 12C21.9525 17.5015 17.5015 21.9526 12 21.9526Z"></path><path d="M12.0001 4.273C11.288 4.273 10.7182 4.84274 10.7182 5.55491V18.4451C10.7182 19.1573 11.288 19.727 12.0001 19.727C12.7123 19.727 13.282 19.1573 13.282 18.4451V5.55491C13.282 4.84274 12.7123 4.273 12.0001 4.273Z"></path><path d="M16.1124 7.88724C15.4002 7.88724 14.8305 8.45697 14.8305 9.16915V14.8131C14.8305 15.5253 15.4002 16.095 16.1124 16.095C16.8245 16.095 17.3943 15.5253 17.3943 14.8131V9.18694C17.3943 8.47478 16.8245 7.88724 16.1124 7.88724Z"></path><path d="M7.90508 7.90498C7.19291 7.90498 6.62318 8.47471 6.62318 9.18688V14.8308C6.62318 15.543 7.19291 16.1127 7.90508 16.1127C8.61724 16.1127 9.18698 15.543 9.18698 14.8308V9.18688C9.18698 8.47471 8.61724 7.90498 7.90508 7.90498Z"></path></svg>`;
const ctb = `<svg class="icon catch" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 18.6231 5.37686 24 12 24C18.6232 24 24 18.6231 24 12C24 5.37688 18.6232 4.4322e-05 12 4.4322e-05C5.37686 -0.017807 0 5.35903 0 12ZM21.9526 12C21.9526 17.4837 17.4837 21.9525 12 21.9525C6.51633 21.9525 2.04748 17.4837 2.04748 12C2.04748 6.51635 6.51633 2.04747 12 2.04747C17.5015 2.04747 21.9526 6.4985 21.9526 12Z"></path><path d="M13.0681 12.0178C13.0681 13.0861 13.9405 13.9585 15.0087 13.9585C16.077 13.9585 16.9494 13.0861 16.9494 12.0178C16.9494 10.9496 16.077 10.0772 15.0087 10.0772C13.9227 10.095 13.0681 10.9496 13.0681 12.0178Z"></path><path d="M12.4808 15.8991C12.4808 14.8273 11.6119 13.9584 10.5401 13.9584C9.46831 13.9584 8.59945 14.8273 8.59945 15.8991C8.59945 16.9709 9.46831 17.8397 10.5401 17.8397C11.6119 17.8397 12.4808 16.9709 12.4808 15.8991Z"></path><path d="M12.4808 8.10091C12.4808 7.02911 11.6119 6.16026 10.5401 6.16026C9.4683 6.16026 8.59944 7.02911 8.59944 8.10091C8.59944 9.1727 9.4683 10.0416 10.5401 10.0416C11.6119 10.0416 12.4808 9.1727 12.4808 8.10091Z"></path></svg>`;
const standard = `<svg class="icon standard" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M4.32678 12.0178C4.32678 7.78046 7.76299 4.34425 12.0003 4.34425C16.2378 4.34425 19.674 7.76266 19.674 12.0178C19.674 16.2553 16.2378 19.6915 12.0003 19.6915C7.76299 19.6915 4.32678 16.2553 4.32678 12.0178Z"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM0 12C0 5.37258 5.37258 0 12 0C18.6274 0 24 5.37258 24 12C24 18.6274 18.6274 24 12 24C5.37258 24 0 18.6274 0 12Z"></path></svg>`;
const documentSolid = `<svg class="icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z" clip-rule="evenodd"></path></svg>`;
const image = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><rect x="32" y="48" width="192" height="160" rx="8" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></rect><path d="M32,168l50.3-50.3a8,8,0,0,1,11.4,0l44.6,44.6a8,8,0,0,0,11.4,0l20.6-20.6a8,8,0,0,1,11.4,0L224,184" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path><circle cx="156" cy="100" r="12"></circle></svg>`;
const arrowUp = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><polyline points="94.1 121.9 128 88 161.9 121.9" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polyline><line x1="128" y1="168" x2="128" y2="88" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const arrowDown = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><polyline points="94.1 134.1 128 168 161.9 134.1" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polyline><line x1="128" y1="88" x2="128" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const trash = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><line x1="216" y1="56" x2="40" y2="56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="104" y1="104" x2="104" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="152" y1="104" x2="152" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><path d="M200,56V208a8,8,0,0,1-8,8H64a8,8,0,0,1-8-8V56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path><path d="M168,56V40a16,16,0,0,0-16-16H104A16,16,0,0,0,88,40V56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path></svg>`;
const add = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><line x1="88" y1="128" x2="168" y2="128" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="128" y1="88" x2="128" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const playSolid = `<svg fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm14.024-.983a1.125 1.125 0 010 1.966l-5.603 3.113A1.125 1.125 0 019 15.113V8.887c0-.857.921-1.4 1.671-.983l5.603 3.113z" fill-rule="evenodd"></path></svg>`;

var icons = /*#__PURE__*/Object.freeze({
    __proto__: null,
    chevronLeft: chevronLeft,
    chevronRight: chevronRight,
    cross: cross,
    fullscreen: fullscreen,
    taiko: taiko,
    mania: mania,
    ctb: ctb,
    standard: standard,
    documentSolid: documentSolid,
    image: image,
    arrowUp: arrowUp,
    arrowDown: arrowDown,
    trash: trash,
    add: add,
    playSolid: playSolid
});

function createElement(tagName, options = {}) {
    const { attributes = {}, style = {}, className = "", children = [] } = options;
    const element = document.createElement(tagName);
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: className }));
    Object.assign(element.style, style);
    element.append(...children);
    return element;
}
function ensuredSelector(query) {
    const element = document.querySelector(query);
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`);
    }
    return element;
}
class MissingElementException extends Error {
    constructor(message) {
        super(message);
        this.name = "MissingElementException";
    }
}
function createIcon(name) {
    return createElementFromHTML(icons[name]);
}
function createElementFromHTML(html) {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild;
}

class CoinsController {
    constructor() {
        var _a;
        this.channel = new BroadcastChannel("coins_channel");
        this.coins = parseInt((_a = localStorage.getItem("coins")) !== null && _a !== void 0 ? _a : "1000");
        setInterval(() => {
            this.increaseCoinsRandomly(10);
        }, 15000);
    }
    increaseCoinsRandomly(factor) {
        const newCoins = Math.floor(Math.random() * 50 * factor);
        this.coins = this.coins + newCoins;
        this.saveCoins();
        return newCoins;
    }
    spendCoins(amount) {
        this.coins = this.coins - amount;
        this.saveCoins();
    }
    saveCoins() {
        localStorage.setItem("coins", this.coins.toString());
        this.channel.postMessage("saved");
    }
    getCoins() {
        return this.coins;
    }
}

function getSkinCollection() {
    const storage = localStorage.getItem("skin-collection");
    if (!storage) {
        return {};
    }
    else {
        const collection = JSON.parse(storage);
        return collection;
    }
}
function saveItemToSkinCollection(id) {
    const collection = getSkinCollection();
    if (!collection[id]) {
        collection[id] = true;
    }
    localStorage.setItem("skin-collection", JSON.stringify(collection));
}

class GachaController {
    constructor(skins, bannerName, bannerKey) {
        this.bannerName = bannerName;
        this.bannerKey = bannerKey;
        this.skins3 = [];
        this.skins4 = [];
        this.skins5 = [];
        this.odds4 = 0.1;
        this.odds5 = 0.02;
        this.hard4 = 10;
        this.hard5 = 50;
        for (const skin of skins) {
            switch (this.getSkinRarity(skin)) {
                case 5:
                    this.skins5.push(skin);
                    break;
                case 4:
                    this.skins4.push(skin);
                    break;
                case 3:
                    this.skins3.push(skin);
                    break;
            }
        }
    }
    getTotalPulls() {
        var _a;
        return parseInt((_a = localStorage.getItem(`${this.bannerKey}-total`)) !== null && _a !== void 0 ? _a : "0");
    }
    getGachaName() {
        return this.bannerName;
    }
    getPityKey(rarity) {
        return `${this.bannerKey}-pity-${rarity}`;
    }
    getPity(rarity) {
        var _a;
        return parseInt((_a = localStorage.getItem(this.getPityKey(rarity))) !== null && _a !== void 0 ? _a : "0");
    }
    getSkins(rarity) {
        switch (rarity) {
            case 3:
                return this.skins3;
            case 4:
                return this.skins4;
            case 5:
                return this.skins5;
        }
    }
    pull() {
        let pity4 = this.getPity(4);
        let pity5 = this.getPity(5);
        const pick4 = () => {
            pity4 = 0;
            pull = pickRandom(this.skins4);
        };
        const pick5 = () => {
            pity4 = 0;
            pity5 = 0;
            pull = pickRandom(this.skins5);
        };
        let pull = pickRandom(this.skins3);
        const roll = Math.random();
        if (pity5 >= this.hard5 - 1) {
            pick5();
        }
        else if (pity4 >= this.hard4 - 1) {
            if (roll < this.odds5) {
                pick5();
                pity5 = 0;
                pity4 = 0;
            }
            else {
                pick4();
            }
        }
        else {
            if (roll < this.odds5) {
                pick5();
                pity5 = 0;
                pity4 = 0;
            }
            else if (roll < this.odds4 + this.odds5) {
                pick4();
                pity4 = 0;
                pity5++;
            }
            else {
                pity4++;
                pity5++;
            }
        }
        localStorage.setItem(this.getPityKey(4), pity4.toString());
        localStorage.setItem(this.getPityKey(5), pity5.toString());
        localStorage.setItem(`${this.bannerKey}-total`, (this.getTotalPulls() + 1).toString());
        saveItemToSkinCollection(pull.forum_thread_id);
        return pull;
        function pickRandom(skins) {
            return skins[Math.floor(Math.random() * skins.length)];
        }
    }
    getSkinRarity(skin) {
        const tags4 = ["contest_submission", /skin_of_the_year_\d{4}_top_10/gm];
        const tags5 = [/contest_\d{1,2}_(winner|2nd_place|3rd_place)/gm, /skin_of_the_year_\d{4}_(winner|2nd_place|3rd_place)/gm];
        for (const tag of tags5) {
            for (const skinTag of skin.tags) {
                if (skinTag.match(tag)) {
                    return 5;
                }
            }
        }
        for (const tag of tags4) {
            for (const skinTag of skin.tags) {
                if (skinTag.match(tag)) {
                    return 4;
                }
            }
        }
        return 3;
    }
}

let prestigeMap = {};

class SkinItem extends HTMLElement {
    constructor(skin) {
        super();
        this.skin = skin;
        this.classList.add("skin-item");
        this.append(createElement("a", {
            className: "skin-item__link",
            attributes: {
                href: skin.url,
            },
        }));
        this.append(this.createCover());
        this.append(this.createInformationArea());
        this.append(this.createTagsArea());
    }
    createCover() {
        const cover = createElement("div", {
            className: "skin-item__cover",
            children: [
                createElement("img", {
                    className: "cover",
                    attributes: {
                        src: `/assets/img/covers/${this.skin.forum_thread_id}.webp`,
                    },
                }),
            ],
        });
        const versionCount = this.skin.skin_collection.length;
        if (versionCount > 1) {
            cover.append(createElement("div", {
                className: "skin-item__cover-versions",
                children: [
                    createIcon("documentSolid"),
                    createElement("span", {
                        className: "version-count",
                        attributes: {
                            innerText: versionCount.toString(),
                        },
                    }),
                ],
            }));
        }
        return cover;
    }
    createInformationArea() {
        return createElement("div", {
            className: "skin-item__information",
            children: [
                createElement("div", {
                    className: "skin-item__information-row",
                    children: [
                        createElement("span", {
                            className: "skin-name",
                            attributes: {
                                innerText: this.skin.skin_name,
                            },
                        }),
                        createElement("div", {
                            className: "game-modes",
                            children: this.skin.game_modes.map((mode) => createIcon(mode == "catch" ? "ctb" : mode)),
                        }),
                    ],
                }),
                createElement("div", {
                    className: "skin-item__information-row",
                    children: [
                        createElement("span", {
                            className: "author",
                            attributes: {
                                innerText: this.skin.formatted_author,
                            },
                        }),
                        createElement("span", {
                            className: "information-seperator",
                        }),
                        ...this.skin.resolutions.map((e) => {
                            return createElement("span", {
                                className: "resolution",
                                attributes: {
                                    innerText: e.toUpperCase(),
                                },
                            });
                        }),
                    ],
                }),
            ],
        });
    }
    createTagsArea() {
        const tags = this.generateTagsData().map((tag) => createTag(tag));
        return createElement("div", {
            className: "skin-item__tags-list-container",
            children: [
                createElement("div", {
                    className: "tags-list",
                    children: tags,
                }),
            ],
        });
        function createTag(tag) {
            return createElement("a", {
                className: `tag${tag.prestige != "" ? " tag--prestige tag--prestige-" + tag.prestige : ""}`,
                attributes: {
                    innerText: tag.tag.replace(/_/g, " "),
                    href: `?${tag.searchKey}=${tag.tag}`,
                },
            });
        }
    }
    generateTagsData() {
        const tagsData = [];
        this.skin.resolutions.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "res",
                tag: e.toUpperCase(),
            });
        });
        this.skin.ratios.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "ratio",
                tag: e,
            });
        });
        this.skin.tags.sort().forEach((e) => {
            tagsData.push({
                prestige: prestigeMap[e],
                searchKey: "search",
                tag: e,
            });
        });
        this.skin.categories.forEach((e) => {
            tagsData.push({
                prestige: "",
                searchKey: "tagin",
                tag: e,
            });
        });
        return tagsData;
    }
}
customElements.define("skin-item", SkinItem);

function wait(ms) {
    return new Promise((r) => setTimeout(r, ms));
}

class BannerMenu extends HTMLElement {
    constructor(controller, allSkins, gameController) {
        super();
        this.controller = controller;
        this.pullLabel = createElement("span", {
            className: "banner-menu__title-total-pulls",
        });
        this.pity4Label = createElement("span", {
            className: "banner-menu__title-pity-label banner-menu__title-pity-label--4",
        });
        this.pity5Label = createElement("span", {
            className: "banner-menu__title-pity-label banner-menu__title-pity-label--5",
        });
        this.resultGrid = createElement("div", {
            className: "skin-grid",
            children: [],
        });
        this.updateTitleLabels();
        this.classList.add("banner-menu");
        this.append(createElement("button", {
            className: "button button--secondary",
            attributes: {
                innerText: "<= back to gacha selector",
                onclick: () => {
                    this.replaceWith(new GachaSelector(allSkins, gameController));
                },
            },
        }));
        this.append(createElement("div", {
            className: "banner-menu__title",
            children: [
                createElement("span", { className: "banner-menu__title-label", attributes: { innerText: controller.getGachaName() } }),
                createElement("div", { className: "banner-menu__title-stats", children: [this.pullLabel, this.pity4Label, this.pity5Label] }),
            ],
        }));
        this.append(this.getRarityDisplaySection(5));
        this.append(this.getRarityDisplaySection(4));
        this.append(this.getRarityDisplaySection(3));
        const pullButton1 = createElement("button", {
            className: "banner-menu__pull-button button button--primary",
            attributes: {
                innerText: "spend 100 osu!coins to perform 1 pulls",
            },
        });
        const pullButton10 = createElement("button", {
            className: "banner-menu__pull-button button button--primary",
            attributes: {
                innerText: "spend 727 osu!coins to perform 10 pulls",
            },
        });
        const pull = (times, coins) => __awaiter(this, void 0, void 0, function* () {
            if (coins > gameController.getCoins()) {
                window.alert("You do not have enough coins for this action! Please obtain more before trying again");
                return;
            }
            else {
                gameController.spendCoins(coins);
                const pulls = [];
                for (let i = 0; i < times; i++) {
                    yield wait(50);
                    pulls.push(this.controller.pull());
                }
                this.updateTitleLabels();
                this.resultGrid.innerHTML = "";
                pulls.forEach((pull) => {
                    const item = new SkinItem(pull);
                    item.classList.add(`skin-item--rarity-${controller.getSkinRarity(pull)}`);
                    this.resultGrid.prepend(item);
                });
            }
        });
        pullButton1.addEventListener("click", () => {
            pull(1, 100);
        });
        pullButton10.addEventListener("click", () => __awaiter(this, void 0, void 0, function* () {
            pull(10, 727);
        }));
        this.append(pullButton1, pullButton10);
        this.append(this.resultGrid);
    }
    updateTitleLabels() {
        this.pullLabel.innerText = "total pulls: " + this.controller.getTotalPulls().toString();
        this.pity4Label.innerText = `4* pity (${this.controller.getPity(4).toString()}/${this.controller.hard4})`;
        this.pity5Label.innerText = `5* pity (${this.controller.getPity(5).toString()}/${this.controller.hard5})`;
    }
    getRarityDisplaySection(rarity) {
        const menu = createElement("div", {
            className: `skin-preview skin-preview--${rarity}`,
            children: [
                createElement("span", {
                    className: "skin-preview-label",
                    attributes: {
                        innerText: `${this.controller.getSkins(rarity).length} different ${rarity}* skins`,
                    },
                }),
            ],
        });
        if (rarity != 3) {
            menu.append(createElement("div", {
                className: "skin-preview-list",
                children: this.controller.getSkins(rarity).map((skin) => createElement("div", {
                    className: "skin-preview-item",
                    children: [
                        createElement("img", { className: "skin-preview-item__cover", attributes: { src: `/assets/img/covers/${skin.forum_thread_id}.webp` } }),
                        createElement("div", {
                            className: "skin-preview-item__title",
                            children: [createElement("span", { attributes: { innerText: skin.skin_name } }), ...skin.game_modes.map((mode) => createIcon(mode == "catch" ? "ctb" : mode))],
                        }),
                        createElement("span", { className: "skin-preview-item__author", attributes: { innerText: skin.formatted_author } }),
                    ],
                })),
            }));
        }
        return menu;
    }
}
customElements.define("banner-menu", BannerMenu);

class SkinCollection extends HTMLElement {
    constructor(allSkins, coinsController) {
        super();
        this.classList.add("skin-collection");
        this.append(createElement("button", {
            className: "button button--secondary",
            attributes: {
                innerText: "<= back to gacha selector",
                onclick: () => {
                    this.replaceWith(new GachaSelector(allSkins, coinsController));
                },
            },
        }));
        const sortController = new GachaController(allSkins, "collection", "collection");
        const total5 = sortController.getSkins(5);
        const total4 = sortController.getSkins(4);
        const total3 = sortController.getSkins(3);
        let owned3 = 0;
        let owned4 = 0;
        let owned5 = 0;
        const collection = getSkinCollection();
        console.log(collection);
        total5.forEach((skin) => {
            if (collection[skin.forum_thread_id]) {
                owned5++;
            }
        });
        total4.forEach((skin) => {
            if (collection[skin.forum_thread_id]) {
                owned4++;
            }
        });
        total3.forEach((skin) => {
            if (collection[skin.forum_thread_id]) {
                owned3++;
            }
        });
        const label5 = createElement("span", {
            className: "skin-collection__section-label",
            attributes: {
                innerText: `[${Math.round((100 / total5.length) * owned5 * 10) / 10}%] (${owned5}/${total5.length}) 5* skins`,
            },
        });
        const label4 = createElement("span", {
            className: "skin-collection__section-label",
            attributes: {
                innerText: `[${Math.round((100 / total4.length) * owned4 * 10) / 10}%] (${owned4}/${total4.length}) 4* skins`,
            },
        });
        const label3 = createElement("span", {
            className: "skin-collection__section-label",
            attributes: {
                innerText: `[${Math.round((100 / total3.length) * owned3 * 10) / 10}%] (${owned3}/${total3.length}) 3* skins`,
            },
        });
        const grid5 = createElement("div", { className: "skin-grid" });
        const grid4 = createElement("div", { className: "skin-grid" });
        const grid3 = createElement("div", { className: "skin-grid" });
        this.append(createElement("span", {
            className: "skin-collection__header",
            attributes: {
                innerText: "Skin Collection",
            },
        }));
        this.append(label5, grid5, label4, grid4, label3, grid3);
        sortController.getSkins(5).forEach((skin) => {
            const item = new SkinItem(skin);
            if (!collection[skin.forum_thread_id]) {
                item.classList.add("skin-item--not-owned");
            }
            else {
                item.classList.add(`skin-item--rarity-${sortController.getSkinRarity(skin)}`);
            }
            grid5.append(item);
        });
        sortController.getSkins(4).forEach((skin) => {
            const item = new SkinItem(skin);
            if (!collection[skin.forum_thread_id]) {
                item.classList.add("skin-item--not-owned");
            }
            else {
                item.classList.add(`skin-item--rarity-${sortController.getSkinRarity(skin)}`);
            }
            grid4.append(item);
        });
        sortController.getSkins(3).forEach((skin) => {
            const item = new SkinItem(skin);
            if (!collection[skin.forum_thread_id]) {
                item.classList.add("skin-item--not-owned");
            }
            else {
                item.classList.add(`skin-item--rarity-${sortController.getSkinRarity(skin)}`);
            }
            grid3.append(item);
        });
    }
}
customElements.define("skin-collection", SkinCollection);

class GachaSelector extends HTMLElement {
    constructor(allSkins, coinsController) {
        super();
        const filteredSkins = new Map();
        const groups = ["anime", "minimalistic", "eyecandy", "game"];
        allSkins.forEach((skin) => {
            var _a;
            let filtered = false;
            groups.forEach((group) => {
                var _a;
                if (skin.categories.includes(group)) {
                    const newResult = (_a = filteredSkins.get(group)) !== null && _a !== void 0 ? _a : [];
                    newResult.push(skin);
                    filteredSkins.set(group, newResult);
                    filtered = true;
                }
            });
            if (!filtered) {
                const newResult = (_a = filteredSkins.get("other")) !== null && _a !== void 0 ? _a : [];
                newResult.push(skin);
                filteredSkins.set("other", newResult);
            }
        });
        for (const entry of filteredSkins.entries()) {
            const skins = entry[1];
            const name = entry[0];
            const controller = new GachaController(skins, name, name);
            const selectorItem = createElement("div", {
                className: "banner-selector-item",
                children: [
                    createElement("span", { className: "banner-selector-item__name", attributes: { innerText: name } }),
                    createElement("span", { className: "banner-selector-item__total", attributes: { innerText: controller.getTotalPulls().toString() } }),
                    createElement("span", { className: "banner-selector-item__pity-4", attributes: { innerText: controller.getPity(4).toString() } }),
                    createElement("span", { className: "banner-selector-item__pity-5", attributes: { innerText: controller.getPity(5).toString() } }),
                    createElement("img", { className: "banner-selector-item__cover", attributes: { src: `/assets/img/gacha/banners/${name}.webp` } }),
                ],
            });
            selectorItem.addEventListener("click", () => {
                this.replaceWith(new BannerMenu(controller, allSkins, coinsController));
            });
            this.append(selectorItem);
        }
        this.prepend(createElement("div", {
            className: "main-menu__header",
            children: [
                createElement("span", { className: "main-menu__header-label", attributes: { innerText: "Available Gacha Banners" } }),
                createElement("span", { className: "main-menu__header-legend", attributes: { innerText: " total pulls | 4* pity | 5* pity" } }),
            ],
        }));
        this.classList.add("main-menu");
        this.append(createElement("button", {
            className: "button button--primary",
            attributes: {
                innerText: "View Collection",
                onclick: () => {
                    this.replaceWith(new SkinCollection(allSkins, coinsController));
                },
            },
        }));
    }
}
customElements.define("gacha-selector", GachaSelector);

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        const coinsController = new CoinsController();
        const target = ensuredSelector("#gacha-root");
        const allSkins = (_a = (yield getSkinIndex())) !== null && _a !== void 0 ? _a : [];
        target.append(new GachaSelector(allSkins, coinsController));
        const purseLabel = createElement("span", {
            className: "coin-purse__label",
        });
        updatePurseLabel();
        target.prepend(createElement("div", {
            className: "coin-purse",
            children: [
                createElement("img", {
                    className: "coin-purse__img",
                    attributes: {
                        src: "/assets/img/gacha/coins.webp",
                    },
                }),
                purseLabel,
            ],
        }));
        const channel = new BroadcastChannel("coins_channel");
        channel.addEventListener("message", () => {
            updatePurseLabel();
        });
        document.addEventListener("click", (e) => {
            const coins = coinsController.increaseCoinsRandomly(1);
            const x = e.pageX;
            const y = e.pageY;
            const popup = createElement("div", {
                className: "coin-popup",
                children: [
                    createElement("img", {
                        className: "coin-popup__coin",
                        attributes: {
                            src: "/assets/img/gacha/coins.webp",
                        },
                    }),
                    createElement("span", {
                        className: "coin-popup__label",
                        attributes: {
                            innerText: `+${coins}`,
                        },
                    }),
                ],
                style: {
                    top: `${y}px`,
                    left: `${x}px`,
                },
            });
            document.body.append(popup);
            setTimeout(() => {
                popup.remove();
            }, 5000);
        });
        function updatePurseLabel() {
            purseLabel.innerText = `${coinsController.getCoins()} osu!coins`;
        }
    });
}
