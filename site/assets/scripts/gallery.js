'use strict';

const chevronLeft = `<svg class="icon chevron-left" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 19l-7-7 7-7"></path></svg>`;
const chevronRight = `<svg class="icon chevron-right" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>`;
const cross = `<svg class="icon cross" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path></svg>`;
const fullscreen = `<svg class="icon fullscreen" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 8V4m0 0h4M4 4l5 5m11-1V4m0 0h-4m4 0l-5 5M4 16v4m0 0h4m-4 0l5-5m11 5l-5-5m5 5v-4m0 4h-4"></path></svg>`;
const taiko = `<svg class="icon taiko" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 0C5.37686 0 0 5.37686 0 12C0 18.6232 5.37686 24 12 24C18.6231 24 24 18.6232 24 12C24.0178 5.37686 18.641 0 12 0ZM12 21.9526C6.51632 21.9526 2.04748 17.4837 2.04748 12C2.04748 6.51633 6.51632 2.04748 12 2.04748C17.4836 2.04748 21.9525 6.51633 21.9525 12C21.9525 17.5015 17.5015 21.9526 12 21.9526Z"></path><path d="M12.0003 4.34426C7.76293 4.34426 4.32672 7.78046 4.32672 12.0178C4.32672 16.2553 7.76293 19.6915 12.0003 19.6915C16.2377 19.6915 19.6739 16.2553 19.6739 12.0178C19.6739 7.76266 16.2377 4.34426 12.0003 4.34426ZM6.89052 12.0001C6.89052 9.61429 8.5285 7.62022 10.7362 7.05049V16.9496C8.5285 16.3798 6.89052 14.3858 6.89052 12.0001ZM13.2822 16.9496V7.05049C15.4899 7.62022 17.1101 9.61429 17.1101 12.0001C17.1101 14.3858 15.4899 16.3798 13.2822 16.9496Z"></path></svg>`;
const mania = `<svg class="icon mania" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M12 0C5.37686 0 0 5.37686 0 12C0 18.6232 5.37686 24 12 24C18.6231 24 24 18.6232 24 12C24.0178 5.37686 18.641 0 12 0ZM12 21.9526C6.51632 21.9526 2.04748 17.4837 2.04748 12C2.04748 6.51633 6.51632 2.04748 12 2.04748C17.4836 2.04748 21.9525 6.51633 21.9525 12C21.9525 17.5015 17.5015 21.9526 12 21.9526Z"></path><path d="M12.0001 4.273C11.288 4.273 10.7182 4.84274 10.7182 5.55491V18.4451C10.7182 19.1573 11.288 19.727 12.0001 19.727C12.7123 19.727 13.282 19.1573 13.282 18.4451V5.55491C13.282 4.84274 12.7123 4.273 12.0001 4.273Z"></path><path d="M16.1124 7.88724C15.4002 7.88724 14.8305 8.45697 14.8305 9.16915V14.8131C14.8305 15.5253 15.4002 16.095 16.1124 16.095C16.8245 16.095 17.3943 15.5253 17.3943 14.8131V9.18694C17.3943 8.47478 16.8245 7.88724 16.1124 7.88724Z"></path><path d="M7.90508 7.90498C7.19291 7.90498 6.62318 8.47471 6.62318 9.18688V14.8308C6.62318 15.543 7.19291 16.1127 7.90508 16.1127C8.61724 16.1127 9.18698 15.543 9.18698 14.8308V9.18688C9.18698 8.47471 8.61724 7.90498 7.90508 7.90498Z"></path></svg>`;
const ctb = `<svg class="icon catch" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M0 12C0 18.6231 5.37686 24 12 24C18.6232 24 24 18.6231 24 12C24 5.37688 18.6232 4.4322e-05 12 4.4322e-05C5.37686 -0.017807 0 5.35903 0 12ZM21.9526 12C21.9526 17.4837 17.4837 21.9525 12 21.9525C6.51633 21.9525 2.04748 17.4837 2.04748 12C2.04748 6.51635 6.51633 2.04747 12 2.04747C17.5015 2.04747 21.9526 6.4985 21.9526 12Z"></path><path d="M13.0681 12.0178C13.0681 13.0861 13.9405 13.9585 15.0087 13.9585C16.077 13.9585 16.9494 13.0861 16.9494 12.0178C16.9494 10.9496 16.077 10.0772 15.0087 10.0772C13.9227 10.095 13.0681 10.9496 13.0681 12.0178Z"></path><path d="M12.4808 15.8991C12.4808 14.8273 11.6119 13.9584 10.5401 13.9584C9.46831 13.9584 8.59945 14.8273 8.59945 15.8991C8.59945 16.9709 9.46831 17.8397 10.5401 17.8397C11.6119 17.8397 12.4808 16.9709 12.4808 15.8991Z"></path><path d="M12.4808 8.10091C12.4808 7.02911 11.6119 6.16026 10.5401 6.16026C9.4683 6.16026 8.59944 7.02911 8.59944 8.10091C8.59944 9.1727 9.4683 10.0416 10.5401 10.0416C11.6119 10.0416 12.4808 9.1727 12.4808 8.10091Z"></path></svg>`;
const standard = `<svg class="icon standard" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M4.32678 12.0178C4.32678 7.78046 7.76299 4.34425 12.0003 4.34425C16.2378 4.34425 19.674 7.76266 19.674 12.0178C19.674 16.2553 16.2378 19.6915 12.0003 19.6915C7.76299 19.6915 4.32678 16.2553 4.32678 12.0178Z"></path><path fill-rule="evenodd" clip-rule="evenodd" d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM0 12C0 5.37258 5.37258 0 12 0C18.6274 0 24 5.37258 24 12C24 18.6274 18.6274 24 12 24C5.37258 24 0 18.6274 0 12Z"></path></svg>`;
const documentSolid = `<svg class="icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z" clip-rule="evenodd"></path></svg>`;
const image = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><rect x="32" y="48" width="192" height="160" rx="8" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></rect><path d="M32,168l50.3-50.3a8,8,0,0,1,11.4,0l44.6,44.6a8,8,0,0,0,11.4,0l20.6-20.6a8,8,0,0,1,11.4,0L224,184" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path><circle cx="156" cy="100" r="12"></circle></svg>`;
const arrowUp = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><polyline points="94.1 121.9 128 88 161.9 121.9" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polyline><line x1="128" y1="168" x2="128" y2="88" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const arrowDown = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><polyline points="94.1 134.1 128 168 161.9 134.1" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></polyline><line x1="128" y1="88" x2="128" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const trash = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><line x1="216" y1="56" x2="40" y2="56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="104" y1="104" x2="104" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="152" y1="104" x2="152" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><path d="M200,56V208a8,8,0,0,1-8,8H64a8,8,0,0,1-8-8V56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path><path d="M168,56V40a16,16,0,0,0-16-16H104A16,16,0,0,0,88,40V56" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></path></svg>`;
const add = `<svg class="icon" xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="currentColor" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><circle cx="128" cy="128" r="96" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="16"></circle><line x1="88" y1="128" x2="168" y2="128" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line><line x1="128" y1="88" x2="128" y2="168" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="16"></line></svg>`;
const playSolid = `<svg fill="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path clip-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm14.024-.983a1.125 1.125 0 010 1.966l-5.603 3.113A1.125 1.125 0 019 15.113V8.887c0-.857.921-1.4 1.671-.983l5.603 3.113z" fill-rule="evenodd"></path></svg>`;

var icons = /*#__PURE__*/Object.freeze({
    __proto__: null,
    chevronLeft: chevronLeft,
    chevronRight: chevronRight,
    cross: cross,
    fullscreen: fullscreen,
    taiko: taiko,
    mania: mania,
    ctb: ctb,
    standard: standard,
    documentSolid: documentSolid,
    image: image,
    arrowUp: arrowUp,
    arrowDown: arrowDown,
    trash: trash,
    add: add,
    playSolid: playSolid
});

function createElement(tagName, options = {}) {
    const { attributes = {}, style = {}, className = "", children = [] } = options;
    const element = document.createElement(tagName);
    Object.assign(element, Object.assign(Object.assign({}, attributes), { className: className }));
    Object.assign(element.style, style);
    element.append(...children);
    return element;
}
function ensuredSelector(query) {
    const element = document.querySelector(query);
    if (!element) {
        throw new MissingElementException(`No element found for query: "${query}"`);
    }
    return element;
}
class MissingElementException extends Error {
    constructor(message) {
        super(message);
        this.name = "MissingElementException";
    }
}
function createIcon(name) {
    return createElementFromHTML(icons[name]);
}
function createElementFromHTML(html) {
    return createElement("div", {
        attributes: {
            innerHTML: html,
        },
    }).firstChild;
}

function imgurImageToThumbnail(url, size) {
    if (url.match(/https?:\/\/(i.)?imgur.com\/[a-zA-Z0-9]*.(png|jpg|jpeg|webp)/gm) != null) {
        return url.substring(0, url.lastIndexOf(".")) + size + url.substring(url.lastIndexOf(".", url.length));
    }
    else {
        return url;
    }
}

class CarousselItem extends HTMLElement {
    constructor(contentData, controller, type) {
        super();
        this.mainElement = createElement(type == "img" ? "img" : "embed", {
            className: "main-gallery__main-preview",
            attributes: {
                src: type == "img" ? imgurImageToThumbnail(contentData, "h") : `https://www.youtube.com/embed/${contentData}?rel=0`,
            },
        });
        this.fullscreenElement = createElement("div", {
            className: `fullscreen-gallery__main-preview-container fullscreen-gallery__main-preview-container--${type}`,
            children: [
                createElement(type == "img" ? "img" : "embed", {
                    className: "fullscreen-gallery__main-preview",
                    attributes: {
                        src: type == "img" ? contentData : `https://www.youtube.com/embed/${contentData}?rel=0`,
                    },
                }),
            ],
        });
        const baseClassName = "caroussel__item";
        this.classList.add(baseClassName);
        const videoClassName = `${baseClassName}--video`;
        if (type == "yt-author") {
            this.classList.add(`${videoClassName}-author`, videoClassName);
        }
        else if (type == "yt-skinship") {
            this.classList.add(`${videoClassName}-skinship`, videoClassName);
        }
        else {
            this.classList.add(`${baseClassName}--image`);
        }
        if (type == "yt-author" || type == "yt-skinship") {
            const playIcon = createIcon("playSolid");
            playIcon.classList.add("play-button");
            if (type == "yt-skinship") {
                playIcon.classList.add("play-button--skinship");
            }
            this.append(playIcon);
        }
        this.addEventListener("click", () => {
            controller.setActiveItem(controller.getAllItems().indexOf(this));
        });
        this.append(createElement("img", {
            attributes: {
                src: type == "img" ? imgurImageToThumbnail(contentData, "t") : `https://img.youtube.com/vi/${contentData}/mqdefault.jpg`,
            },
            className: "caroussel__image",
        }));
    }
    getMainPreviewItem() {
        return this.mainElement;
    }
    getFullscreenElement() {
        return this.fullscreenElement;
    }
}
customElements.define("caroussel-item", CarousselItem);

class GalleryController {
    constructor(data, variantIndex) {
        this.listeners = [];
        this.carousselItems = [];
        this.activeItem = this.carousselItems[0];
        data.variants[variantIndex].screenshots.forEach((url, i) => {
            this.carousselItems.push(new CarousselItem(url, this, "img"));
        });
        data.videos.skinship.forEach((videoID) => {
            this.carousselItems.push(new CarousselItem(videoID, this, "yt-skinship"));
        });
        data.videos.author.forEach((videoID) => {
            this.carousselItems.push(new CarousselItem(videoID, this, "yt-author"));
        });
        this.carousselItems[0].classList.add("active");
        this.activeItem = this.carousselItems[0];
        window.addEventListener("keydown", (e) => {
            if (e.key == "ArrowLeft") {
                this.prevImage();
            }
            if (e.key == "ArrowRight") {
                this.nextImage();
            }
        });
    }
    getActiveIndex() {
        return this.carousselItems.indexOf(this.activeItem);
    }
    nextImage() {
        const index = this.getActiveIndex();
        if (index < this.carousselItems.length - 1) {
            this.setActiveItem(index + 1);
        }
    }
    prevImage() {
        const index = this.getActiveIndex();
        if (index != 0) {
            this.setActiveItem(index - 1);
        }
    }
    activeIsFirst() {
        if (this.getActiveIndex() == 0) {
            return true;
        }
        else {
            return false;
        }
    }
    activeIsLast() {
        if (this.getActiveIndex() == this.carousselItems.length - 1) {
            return true;
        }
        else {
            return false;
        }
    }
    setActiveItem(index) {
        if (index >= this.carousselItems.length) {
            return;
        }
        this.activeItem = this.carousselItems[index];
        this.carousselItems.forEach((e) => e.classList.remove("active"));
        this.activeItem.classList.add("active");
        this.triggerListeners();
    }
    getActiveItem() {
        return this.activeItem;
    }
    getAllItems() {
        return this.carousselItems;
    }
    addListener(listener) {
        this.listeners.push(listener);
    }
    triggerListeners() {
        this.listeners.forEach((func) => func());
    }
}

class MenuBar extends HTMLElement {
    constructor(fullscreenGallery, skinName) {
        super();
        this.classList.add("menubar");
        this.append(createElement("span", {
            className: "menubar__title",
            attributes: {
                innerText: skinName,
            },
        }), createElement("div", {
            className: "menubar__buttons-container",
            children: [
                createElement("div", {
                    className: "menubar__button menubar__button--fullscreen",
                    children: [createIcon("fullscreen")],
                    attributes: {
                        onclick: () => fullscreenGallery.requestFullscreen(),
                    },
                }),
                createElement("div", {
                    className: "menubar__button menubar__button--close",
                    children: [createIcon("cross")],
                    attributes: {
                        onclick: () => fullscreenGallery.exitFullscreenGallery(),
                    },
                }),
            ],
        }));
    }
}
customElements.define("menu-bar", MenuBar);

class FullscreenGallery extends HTMLElement {
    constructor(controller, skinName) {
        super();
        this.controller = controller;
        this.nextNavigationElement = this.createNavigationNode("next");
        this.prevNavigationElement = this.createNavigationNode("prev");
        this.classList.add("fullscreen-gallery", "hidden");
        this.handleNavigationVisibility();
        this.mainPreview = this.controller.getActiveItem().getFullscreenElement();
        this.append(new MenuBar(this, skinName), this.mainPreview, this.prevNavigationElement, this.nextNavigationElement);
        window.addEventListener("keydown", (e) => {
            if (e.key == "Escape") {
                this.exitFullscreenGallery();
            }
        });
        this.addEventListener("click", (e) => {
            if (e.target != null) {
                if (e.target == this || this.mainPreview.contains(e.target)) {
                    this.exitFullscreenGallery();
                }
            }
        });
        controller.addListener(() => {
            this.syncState();
        });
    }
    createNavigationNode(type) {
        return createElement("div", {
            className: `fullscreen-gallery__navigation fullscreen-gallery__navigation--${type} gallery__navigation gallery__navigation--${type}`,
            attributes: {
                onclick: () => {
                    if (type == "prev") {
                        this.controller.prevImage();
                    }
                    else {
                        this.controller.nextImage();
                    }
                    this.syncState();
                },
            },
            children: [createIcon(type == "prev" ? "chevronLeft" : "chevronRight")],
        });
    }
    syncState() {
        const newMain = this.controller.getActiveItem().getFullscreenElement();
        this.mainPreview.replaceWith(newMain);
        this.mainPreview = newMain;
        this.handleNavigationVisibility();
    }
    exitFullscreenGallery() {
        if (document.fullscreenElement) {
            document.exitFullscreen();
        }
        this.classList.add("hidden");
        document.body.classList.remove("freeze");
        const newMain = createElement("div");
        this.mainPreview.replaceWith(newMain);
        this.mainPreview = newMain;
    }
    enterFullscreenGallery() {
        this.syncState();
        this.classList.remove("hidden");
        this.handleNavigationVisibility();
        document.body.classList.add("freeze");
    }
    handleNavigationVisibility() {
        this.nextNavigationElement.classList.remove("hidden");
        this.prevNavigationElement.classList.remove("hidden");
        if (this.controller.activeIsFirst()) {
            this.prevNavigationElement.classList.add("hidden");
        }
        if (this.controller.activeIsLast()) {
            this.nextNavigationElement.classList.add("hidden");
        }
    }
}
customElements.define("fullscreen-gallery", FullscreenGallery);

class Gallery extends HTMLElement {
    constructor(skinInformation) {
        super();
        this.caroussel = createElement("div", {
            className: "caroussel",
        });
        this.mainPreview = createElement("img", {
            className: "main-gallery__main-preview",
        });
        this.activeVariantIndex = 0;
        this.nextNavigationElement = createElement("div", {
            className: "gallery__navigation gallery__navigation--next main-gallery__navigation main-gallery__navigation--next",
            attributes: {
                onclick: (e) => {
                    e.stopPropagation();
                    this.getController().nextImage();
                },
            },
            children: [createIcon("chevronRight")],
        });
        this.prevNavigationElement = createElement("div", {
            className: "gallery__navigation gallery__navigation--prev main-gallery__navigation main-gallery__navigation--prev",
            attributes: {
                onclick: (e) => {
                    e.stopPropagation();
                    this.getController().prevImage();
                },
            },
            children: [createIcon("chevronLeft")],
        });
        this.skinInformation = skinInformation;
        this.classList.add("gallery", "main-gallery");
        this.append(createElement("div", {
            className: "main-gallery__main-preview-container",
            children: [this.mainPreview, this.nextNavigationElement, this.prevNavigationElement],
            attributes: {
                onclick: () => {
                    this.fullscreenGallery.enterFullscreenGallery();
                },
            },
        }));
        this.setDisplayedVariant();
        this.append(this.caroussel);
    }
    getController() {
        return this.controller;
    }
    getGalleryTitle() {
        let galleryTitle = this.skinInformation.name;
        const variantName = this.skinInformation.variants[this.activeVariantIndex].name;
        if (variantName != galleryTitle) {
            galleryTitle = galleryTitle + ` - ${variantName}`;
        }
        return galleryTitle;
    }
    setDisplayedVariant() {
        this.controller = new GalleryController(this.skinInformation, this.activeVariantIndex);
        this.controller.addListener(() => {
            this.syncState();
        });
        this.fullscreenGallery = new FullscreenGallery(this.controller, this.getGalleryTitle());
        document.body.append(this.fullscreenGallery);
        this.caroussel.innerHTML = "";
        this.caroussel.append(...this.controller.getAllItems());
        this.syncState();
    }
    syncState() {
        this.prevNavigationElement.classList.remove("hidden");
        this.nextNavigationElement.classList.remove("hidden");
        if (this.controller.activeIsFirst()) {
            this.prevNavigationElement.classList.add("hidden");
        }
        if (this.controller.activeIsLast()) {
            this.nextNavigationElement.classList.add("hidden");
        }
        const newMain = this.controller.getActiveItem().getMainPreviewItem();
        this.mainPreview.replaceWith(newMain);
        this.mainPreview = newMain;
    }
    switchVariant(index) {
        if (this.skinInformation.variants[index] == undefined) {
            return;
        }
        this.activeVariantIndex = index;
        this.setDisplayedVariant();
    }
}
customElements.define("image-gallery", Gallery);

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", main);
}
else {
    main();
}
function main() {
    const gallery = new Gallery(getSkinData());
    ensuredSelector(".gallery").replaceWith(gallery);
    const skinSelector = document.querySelector(".skin-selector");
    if (skinSelector !== null) {
        skinSelector.addEventListener("change", () => {
            gallery.switchVariant(parseInt(skinSelector.value));
            if (document.activeElement instanceof HTMLElement) {
                document.activeElement.blur();
            }
        });
    }
}
function getSkinData() {
    return JSON.parse(ensuredSelector("#json-collection").innerText, function (key, value) {
        if (key == "skinship" || key == "author") {
            if (value == null) {
                return [];
            }
        }
        return value;
    });
}
