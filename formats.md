# contest

filename: `contest_id.html`
location: `_contest/`
```yaml
---
layout: contest
contest_id: integer 
submissions_active: boolean # submission list disabled if false
results_out: boolean # podium disabled if false
name: string # title of the contest
---
```

# user

filename: `user_id.html`

```yaml
---
layout: user
user_name:
user_id:
roles:
    - id: # string, object key of the role in _data/roles.yml
      modifier: # optional, string. This is used to e.g. insert the number of a contest iteration into the generic role placeholder 
---
```

# skin

filename: `yyyy-mm-dd-forum_thread_id.md`

the timestamp in the filename is the release date of the skin

```yaml
---
layout: skin

# Required
skin_name: skin name
forum_thread_id: forum thread id # also doubles as cover file name and general skin id
date_added: iso local date # date the skin was added to the compendium
game_modes:
    - mode_1
    - mode_2
    - ...
author:
    - author_1_id
    -...
resolutions: # list the supported resolutions (HD|SD, not aspect ratios!)
    - resolution_1
    - resolution_2
ratios: # list of supported aspect ratios
    - ratio 1
    - ratio 2
tags:
    - tag_1
    - tag 2
categories: # there is a predefined list of categories, see # Categories. A skin can be in more than one category
    - cat_1
    - cat_2
skin_collection: # even if the skin only has a single version it needs to be filed as a length = 1 collection
    - name: collection_skin_1
      screenshots:
          - link_screenshots_1
          - link_screenshots_2
          - ...
    - ...
videos:
    skinship: # videos released by skinship only
        - youtube_id_1
        - ...
    author: # videos from the skin thread itself
        - youtube_id_1
        - youtube_id_2
        - ...
---
```

# Categories
This is the current pre-defined list of categories, though I am open for suggestions.

- anime
- minimalistic
- eyecandy
- game
- joke
- other (fallback for when none of the others fit)

# Aspect Ratios
This is the current pre-defined list of aspect ratios. These must be quoted, or else yaml will interpret these as expressions:

- "4:3"
- "16:9"
- "16:10"
- "21:9"
- "all"